let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

var jsOutputPath = 'public/js/marketing';
var jsAdminOutputPath = 'public/js/marketing/admin';
var cssOutputPath = 'public/css/marketing';

mix
  .js('resources/assets/marketing/js/app.js', jsOutputPath)
  .js('resources/assets/marketing/js/navbar.js', jsOutputPath)
  .js('resources/assets/marketing/js/home.js', jsOutputPath)
  .js('resources/assets/marketing/js/contact-us.js', jsOutputPath)
  .js(
    'resources/assets/marketing/js/mailing-list-subscription.js',
    jsOutputPath
  )
  .js('resources/assets/marketing/js/admin/faq.js', jsAdminOutputPath)
  .js('resources/assets/marketing/js/admin/article.js', jsAdminOutputPath)
  .js(
    'resources/assets/marketing/js/admin/legal-document.js',
    jsAdminOutputPath
  )
  .sass('resources/assets/marketing/sass/app.sass', cssOutputPath)
  .sass('resources/assets/marketing/sass/admin.sass', cssOutputPath);
