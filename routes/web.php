<?php

use Illuminate\Support\Facades\Redirect;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

//Auth::routes();
Route::middleware(['auth'])->namespace('Backend')->prefix('admin')->group(function () {
    Route::get('dashboard', 'DashboardController@index');
    Route::get('users/{id}/suspend', 'UsersController@suspend')->name('users.suspend');
    Route::get('users/{id}/resume', 'UsersController@resume')->name('users.resume');
    Route::resource('users', 'UsersController');
    Route::get('companies/{id}/users', 'CompaniesController@users')->name('companies.users');
    Route::get('companies/{company_id}/invoices', 'InvoicesController@index')->name('companies.invoices');
    Route::get('companies/{company_id}/subscription', 'CompaniesController@editSubscription')->name('companies.subscription');
    Route::post('companies/{company_id}/subscription', 'CompaniesController@updateSubscription')->name('companies.subscription');
    Route::get('companies/{company_id}/invoices/{id}', 'InvoicesController@show')->name('companies.invoices.show');
    Route::get('companies/{company_id}/invoices/{id}/download', 'InvoicesController@download')->name('companies.invoices.download');
    Route::resource('companies', 'CompaniesController');
    Route::resource('plans', 'PlansController');
    Route::resource('coupons', 'CouponsController');
    Route::resource('standard/forms', 'StandardFormsController');
    Route::resource('standard/scopes', 'StandardScopesController');
    Route::get('standard/moisture', 'StandardMoistureMapController@index')->name('moisture-page');
    Route::get('standard/area', 'StandardAffectedAreasController@areaPage')->name('areas-page');
    Route::get('standard/team', 'StandardTeamsController@teamsPage')->name('teams-page');
    Route::resource('units-of-measure', 'UnitsOfMeasureController');
    Route::get('notifications/auto-responders/{id}/preview', 'AutoRespondersController@preview')->name('auto-responders.preview');
    Route::resource('notifications/auto-responders', 'AutoRespondersController');

// ------------------API-----------------//
    Route::resource('standard/areas', 'StandardAffectedAreasController');
    Route::resource('standard/teams', 'StandardTeamsController');
    Route::resource('standard/structure', 'StardardMoistureMapStructureController');
    Route::resource('standard/material', 'StardardMoistureMapMaterialController');
    Route::get('uoms/jsonResult', 'UnitsOfMeasureController@jsonResult');
    Route::get('standard-scopes/form', 'StandardScopesController@form');
    Route::post('standard-scopes/form-update', 'StandardScopesController@formUpdate');
    Route::get('standard-moisture/form', 'StandardMoistureMapController@form');
    Route::post('standard-moisture/form-update', 'StandardMoistureMapController@formUpdate');
    Route::get('standard-areas/form', 'StandardAffectedAreasController@form');
    Route::post('standard-areas/form-update', 'StandardAffectedAreasController@formUpdate');

    Route::resource('training/categories', 'TrainingController');
    Route::resource('training/videos', 'TrainingVideosController');

    Route::get('ticket/{ticket_id}', 'TicketsController@show');
    Route::post('comment_ticket', 'TicketCommentsController@store');
    Route::get('tickets/list', 'TicketsController@index');
    Route::post('close_ticket/{ticket_id}', 'TicketsController@close');
    Route::resource('ticket_categories', 'TicketCategoriesController');
});

Route::get('/project/print/{id}', 'Api\ProjectFormsController@print');
Route::get('/form/preview/{token}', 'Api\StandardsController@preview');
Route::get('/dropbox-auth', 'Api\CompaniesController@dropboxAuth');
Route::post('/dropbox-auth', 'Api\CompaniesController@storeDropboxAuth');

Route::middleware([])->namespace('Frontend')->group(function () {
    Route::get('/panel', ['uses' => 'HomeController@index', 'as' => 'main.index']);
});


Route::namespace('Marketing')->group(function () {
    Route::get('/', 'HomeController@index');
    Route::get('/about', 'AboutController@index');
    Route::get('/features', 'FeatureController@index');
    Route::get('/terms-and-conditions', 'LegalDocumentController@showTermsAndConditions');
    Route::get('/privacy-policy', 'LegalDocumentController@showPrivacyPolicy');

    Route::group(['prefix' => 'contact-us'], function () {
        Route::get('/', 'ContactUsController@index');
        Route::post('/', 'ContactUsController@store');
    });

    Route::group(['prefix' => MAILING_LIST_PATH], function () {
        Route::get('/', 'MailingListController@index');
        Route::post('/', 'MailingListController@store');
        Route::post('/verify', 'MailingListController@verify');
        Route::get('/confirm-cancellation/{cancellationKey}', 'MailingListController@confirmRemoval');
        Route::get('/confirm/{confirmationKey}', 'MailingListController@confirm');
    });

    Route::get(MAILING_LIST_CANCEL_PATH, ['uses' => 'MailingListController@cancel', 'as' => 'mailing-list.cancel']);

    Route::group(['prefix' => 'blog'], function () {
        Route::get('/', 'BlogController@index');
        Route::get('/article/{id}/{slug}', ['uses' => 'ArticleController@index', 'as' => ARTICLE_ROUTE_NAME]);
    });

    Route::get('/faq', 'FAQController@index');
});

Route::middleware(['auth'])->namespace('Marketing\CMS')->group(function () {
    Route::group(['prefix' => 'cms'], function () {
        Route::get('/', function () {
            return redirect(route('cms.contact-people'));
        });
        Route::group(['prefix' => 'contact-people'], function () {
            Route::get('/', ['uses' => 'ContactPersonController@index', 'as' => 'cms.contact-people']);
            Route::delete('/delete/{id}', ['uses' => 'ContactPersonController@destroy', 'as' => 'cms.faq.destroy']);
        });

        Route::group(['prefix' => 'mailing-list'], function () {
            Route::get('/', 'MailingListController@index');
            Route::get('/download-csv', 'MailingListController@downloadCSV');
            Route::delete('/delete/{id}', 'MailingListController@destroy');
        });

        Route::group(['prefix' => 'other-settings'], function () {
            Route::get('/', ['uses' => 'MarketingSettingController@index', 'as' => 'cms.other-settings']);
            Route::get('/edit', 'MarketingSettingController@edit');
            Route::post('/edit', 'MarketingSettingController@update');
        });

        Route::group(['prefix' => 'faq'], function () {
            Route::get('/', 'FAQController@index');
            Route::get('/create', ['uses' => 'FAQController@create', 'as' => 'cms.faq.create']);
            Route::post('/', ['uses' => 'FAQController@store', 'as' => 'cms.faq.store']);
            Route::get('/edit/{id}', ['uses' => 'FAQController@edit', 'as' => 'cms.faq.edit']);
            Route::put('/{id}', ['uses' => 'FAQController@update', 'as' => 'cms.faq.update']);
            Route::delete('/delete/{id}', ['uses' => 'FAQController@destroy', 'as' => 'cms.faq.destroy']);
        });

        Route::group(['prefix' => 'country'], function () {
            Route::get('/', 'SupportedCountryController@index');
            Route::get('/create', ['uses' => 'SupportedCountryController@create', 'as' => 'cms.country.create']);
            Route::post('/', ['uses' => 'SupportedCountryController@store', 'as' => 'cms.country.store']);
            Route::get('/edit/{id}', ['uses' => 'SupportedCountryController@edit', 'as' => 'cms.country.edit']);
            Route::put('/{id}', ['uses' => 'SupportedCountryController@update', 'as' => 'cms.country.update']);
            Route::delete('/delete/{id}', ['uses' => 'SupportedCountryController@destroy', 'as' => 'cms.country.destroy']);
        });

        Route::group(['prefix' => 'blog-article'], function () {
            Route::get('/', 'ArticleController@index');
            Route::get('/edit/{id}', ['uses' => 'ArticleController@edit', 'as' => 'cms.blog-article.edit']);
            Route::put('/{id}', ['uses' => 'ArticleController@update', 'as' => 'cms.blog-article.update']);
            Route::get('/{id}/{slug}', ['uses' => 'ArticleController@show', 'as' => 'cms.blog-article.show']);
            Route::get('/create', ['uses' => 'ArticleController@create', 'as' => 'cms.blog-article.create']);
            Route::post('/', ['uses' => 'ArticleController@store', 'as' => 'cms.blog-article.store']);
            Route::delete('/delete/{id}', ['uses' => 'ArticleController@destroy', 'as' => 'cms.blog-article.destroy']);
        });

        Route::group(['prefix' => 'legal-documents'], function () {
            Route::get('/', 'LegalDocumentController@index');
            Route::get('/{type}', 'LegalDocumentController@show');
            Route::get('/edit/{type}', ['uses' => 'LegalDocumentController@edit', 'as' => 'cms.legal-document.edit']);
            Route::put('/', ['uses' => 'LegalDocumentController@update', 'as' => 'cms.legal-document.update']);
        });
    });
});
