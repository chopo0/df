@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
        {{ trans('backpack::base.dashboard') }}
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">{{ trans('backpack::base.dashboard') }}</li>
      </ol>
    </section>
    <style>
        .fixed-width {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-title">Information</div>
                </div>
                <div class="box-body">
                    <div class="col-xs-3">
                        <div class="panel panel-primary fixed-width">
                            <div class="panel-heading">
                                <h3 class="panel-title">Total Companies</h3>
                            </div>
                            <div class="panel-body">
                                <strong>{{ $companies }}</strong>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <div class="panel panel-primary fixed-width">
                            <div class="panel-heading">
                                <h3 class="panel-title">Total Users</h3>
                            </div>
                            <div class="panel-body">
                                <strong>{{ $users }}</strong>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <div class="panel panel-primary fixed-width">
                            <div class="panel-heading">
                                <h3 class="panel-title">Total Projects</h3>
                            </div>
                            <div class="panel-body">
                                <strong>{{ $projects }}</strong>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <div class="panel panel-primary fixed-width">
                            <div class="panel-heading">
                                <h3 class="panel-title">Total Mailing List Subscribers</h3>
                            </div>
                            <div class="panel-body">
                                <strong>{{ $subscriptions }}</strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
