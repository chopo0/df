@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            User
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a></li>
            <li><a href="{{ route('companies.index') }}">Companies</a></li>
            <li class="active">Edit Company</li>
        </ol>
    </section>
@endsection


@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <a href="{{ route('companies.index') }}"><i class="fa fa-angle-double-left"></i> Back to companies</a>
            <br>
            <br>
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-title">Edit Company</div>
                </div>

                <div class="box-body">
                    <form method="post"
                          action="{{ route('companies.update', $company->id) }}" enctype="multipart/form-data">
                        <input type="hidden" name="_method" value="patch">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" name="name" value="{{ $company->name }}">
                            @if($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Logo</label>
                            <input type="file" class="form-control" name="logo">
                            @if($errors->has('logo'))
                                <span class="text-danger">{{ $errors->first('logo') }}</span>
                            @endif
                        </div>
                        @if($company->public_logo_path)
                        <div class="form-group">
                            <img src="{{ asset($company->public_logo_path) }}">
                        </div>
                        @endif
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-sm btn-primary">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
