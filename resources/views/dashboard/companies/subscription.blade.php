@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            Company Subscription
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a></li>
            <li class="active">Edit {{ $company->name }} Subscription</li>
        </ol>
    </section>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <a href="{{ route('companies.index') }}"><i class="fa fa-angle-double-left"></i> Back to companies</a>
            <br>
            <br>
            @if(isset($company->user) && $company->user->stripe_id)
                <div class="box box-default">
                    <div class="box-header with-border">
                        <div class="box-title">{{ $company->name }} Subscription Details</div>
                    </div>

                    <div class="box-body">
                        <form method="post" action="{{ route('companies.subscription', $company->id) }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Action</label>
                                <select class="form-control" name="action">
                                    <option value="">-- Please select --</option>
                                    <option value="terminate_at_the_end">Terminate subscription at period end</option>
                                    <option value="terminate_now">Terminate subscription now</option>
                                </select>
                                @if($errors->has('action'))
                                    <span class="text-danger">{{ $errors->first('action') }}</span>
                                @endif
                            </div>
                            <!-- //TODO future improvement -->
                            {{--<div class="form-group">--}}
                                {{--<label>Days</label>--}}
                                {{--<input type="number" min="1" class="form-control" name="days" value="1">--}}
                                {{--@if($errors->has('days'))--}}
                                    {{--<span class="text-danger">{{ $errors->first('days') }}</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-sm btn-danger">Apply</button>
                            </div>
                        </form>
                    </div>
                </div>
            @endif

        </div>
    </div>
@endsection
