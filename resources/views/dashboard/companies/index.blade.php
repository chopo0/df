@extends('backpack::layout')

@section('header')
    <style>
        th > a {
            cursor: pointer;
        }
    </style>
    <section class="content-header">
        <h1>
            Companies
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a></li>
            <li class="active">Companies</li>
        </ol>
    </section>
@endsection


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-title">Companies ({{ $companies->total() }})</div>
                </div>

                <div class="box-header">
                    <form method="get" id="filters" class="form-inline" action="{{ route('companies.index') }}">
                        <div class="form-group form-group-sm">
                            <label>Subscription status: </label>
                            <select class="form-control form-control-sm" name="status">
                                <option value="all" @if(request('status') === 'all') selected @endif>All</option>
                                <option value="active" @if(request('status') === 'active') selected @endif>Active</option>
                                <option value="canceled" @if(request('status') === 'canceled') selected @endif>Cancelled</option>
                                <option value="past_due" @if(request('status') === 'past_due') selected @endif>Past Due</option>
                                <option value="trialing" @if(request('status') === 'trialing') selected @endif>Trialing</option>
                                <option value="unpaid" @if(request('status') === 'unpaid') selected @endif>Unpaid</option>
                            </select>
                        </div>
                        <div class="form-group form-group-sm">
                            <label>Name: </label>
                            <input type="text" class="form-control form-control-sm" name="name" value="{{ request('name') }}">
                        </div>
                        <div class="form-group form-group-sm">
                            <label>Email: </label>
                            <input type="text" class="form-control form-control-sm" name="email" value="{{ request('email') }}">
                        </div>
                        <div class="form-group form-group-sm">
                            <label>Coupon: </label>
                            <input type="text" class="form-control form-control-sm" name="coupon" value="{{ request('coupon') }}">
                        </div>

                        <input type="hidden" name="sort" value="{{ request('sort') }}">

                        <div class="form-group form-group-sm pull-right" style="margin-left: 15px;">
                            <button class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Search</button>
                        </div>
                        <div class="form-group form-group-sm pull-right">
                            <a class="btn btn-sm btn-warning" href="{{ route('companies.index') }}"><i class="fa fa-filter"></i> Clear</a>
                        </div>
                    </form>
                </div>

                <div class="box-body">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th><a id="sort"><strong>Company</strong></a></th>
                            <th>Address</th>
                            <th>Coupon</th>
                            <th>Company Email</th>
                            <th>Main Registered Email</th>
                            <th>Date Account Created</th>
                            <th>Date Next Payment Due</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($companies as $company)
                            <tr>
                                <td>{{ $company->name }}</td>
                                <td>{{ $company->full_address }}</td>
                                <td>{{ optional($company->user)->coupon ?? '-' }}</td>
                                <td>{{ $company->email }}</td>
                                <td>{{ optional($company->user)->email }}</td>
                                <td>{{ optional($company->user)->created_at }}</td>
                                <td>{{ optional($company->user)->subscription ? \Carbon\Carbon::createFromTimestamp($company->user->subscription->current_period_end)->format('m/d/Y H:m') : 'n/a' }}</td>
                                <td>{{ optional($company->user)->subscription ? ucfirst($company->user->subscription->status) : 'n/a' }}</td>
                                <td class="text-center">
                                    <a class="btn btn-xs btn-primary" href="{{ route('companies.users', $company->id) }}">
                                        <i class="fa fa-users"></i> Users
                                    </a>
                                    <a class="btn btn-xs btn-primary" href="{{ route('companies.invoices', $company->id) }}">
                                        <i class="fa fa-archive"></i> Invoices
                                    </a>
                                    @if(isset($company->user) && $company->user->stripe_id && optional($company->user->subscription)->status !== 'canceled')
                                        <a class="btn btn-xs btn-warning" href="{{ route('companies.subscription', $company->id) }}">
                                            <i class="fa fa-credit-card"></i> Subscription
                                        </a>
                                    @endif
                                    <a class="btn btn-xs btn-default" href="{{ route('companies.edit', $company->id) }}">
                                        <i class="fa fa-edit"></i> Edit
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $companies->links() }}
            </div>
        </div>
    </div>
@endsection

@section('after_scripts')
    <script>
        $(document).ready(function() {
            $('#sort').on('click', function() {
                let currentSort = $('[name="sort"]').val();
                console.log(currentSort)
                if (currentSort === '' || currentSort === 'desc') {
                    $('[name="sort"]').attr('value', 'asc')
                } else {
                    $('[name="sort"]').attr('value', 'desc')
                }

                $('#filters').submit()
            })
        })
    </script>
@endsection
