@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            Invoices
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a></li>
            <li class="active">Invoices</li>
        </ol>
    </section>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-title">Invoices ({{ count($invoices->data) }})</div>
                </div>

                <div class="box-body">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Number</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Discount</th>
                            <th>Subtotal</th>
                            <th>Total</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($invoices->data as $invoice)
                            <tr>
                                <td>{{ $invoice->number }}</td>
                                <td>{{ \Carbon\Carbon::createFromTimestamp($invoice->date)->format('m/d/Y H:m') }}</td>
                                <td>{{ ucfirst($invoice->status) }}</td>
                                <td>{{ $invoice->discount && $invoice->discount->coupon ? $invoice->discount->coupon->id : 'n/a' }}</td>
                                <td>$ {{ number_format($invoice->subtotal / 100, 2) }}</td>
                                <td>$ {{ number_format($invoice->total / 100, 2) }}</td>
                                <td>
                                    <a class="btn btn-xs btn-default" href="{{ route('companies.invoices.show', ['company_id' => $companyId, 'id' => $invoice->id]) }}" target="_blank">
                                        <i class="fa fa-eye"></i> Show
                                    </a>
                                    <a class="btn btn-xs btn-default" href="{{ route('companies.invoices.download', ['company_id' => $companyId, 'id' => $invoice->id]) }}" target="_blank">
                                        <i class="fa fa-download"></i> Download
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
