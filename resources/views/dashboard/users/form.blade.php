@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            User
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a></li>
            <li><a href="{{ route('companies.users', $companyId) }}">Company Users</a></li>
            <li class="active">{{ isset($user) ? 'Edit' : 'Create' }} User</li>
        </ol>
    </section>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <a href="{{ route('companies.users', $companyId) }}"><i class="fa fa-angle-double-left"></i> Back to company users</a>
            <br>
            <br>
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-title">{{ isset($user) ? 'Edit' : 'Create' }} User</div>
                </div>

                <div class="box-body">
                    <form method="post"
                          action="@if(isset($user)) {{ route('users.update', $user->id) }} @else {{ route('users.store') }} @endif">
                        @isset($user)
                            <input type="hidden" name="_method" value="patch">
                        @endisset
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control" name="first_name" @isset($user) value="{{ $user->first_name }}" @endisset>
                            @if($errors->has('first_name'))
                                <span class="text-danger">{{ $errors->first('first_name') }}</span>
                            @endif
                        </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" class="form-control" name="last_name" @isset($user) value="{{ $user->last_name }}" @endisset>
                                @if($errors->has('last_name'))
                                    <span class="text-danger">{{ $errors->first('last_name') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" name="email" @isset($user) value="{{ $user->email }}" @endisset>
                                @if($errors->has('email'))
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Role</label>
                                <select class="form-control" name="role_id" @if($user->id === $user->company->user_id) disabled @endif>
                                    <option value="">-- Please select --</option>
                                    @foreach($roles as $role)
                                        <option value="{{ $role->id }}" @if(isset($user) && $user->role_id === $role->id) selected @endif>{{ $role->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('role_id'))
                                    <span class="text-danger">{{ $errors->first('role_id') }}</span>
                                @endif
                            </div>

                            <hr>
                            <h4>Password change</h4>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" name="new_password">
                                @if($errors->has('new_password'))
                                    <span class="text-danger">{{ $errors->first('new_password') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Confirm Password</label>
                                <input type="password" class="form-control" name="new_password_confirmation">
                                @if($errors->has('new_password_confirmation'))
                                    <span class="text-danger">{{ $errors->first('new_password_confirmation') }}</span>
                                @endif
                            </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-sm btn-primary">{{ isset($user) ? 'Update' : 'Create' }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after_scripts')
    <script>
        $(document).ready(function() {
            $('[name="days"]').parent().hide();
            // $('[name="action"]').on('change', function() {
            //     let value = $('[name="action"]').val()
            //     if (value === 'extend') {
            //         $('[name="days"]').parent().show();
            //     } else {
            //         $('[name="days"]').parent().hide();
            //     }
            // })
        })
    </script>
@endsection
