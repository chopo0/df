@include('pdf.partials._styles')

<style>
    table {
        page-break-after: avoid;
    }
    .deleted {
        background-color: rgb(244, 115, 115);
        border-radius: 4px;
        padding: 2px 6px 2px 6px;
    }
</style>

<div class="container">
    @foreach($logs as $log)
        <table width="100%">
            <tr>
                <td>
                    <strong>{{ $log->notes ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $log->updated_at)->format('m/d/Y H:i') : '' }} by {{ $log->user ? $log->user->full_name : 'n/a' }} {{ $log->standard_form->name }}</strong>
                    @if(!$log->notes)
                        <label class="deleted">DELETED</label>
                    @endif
                </td>
            </tr>
            <tr>
                <td>{{ $log->notes }}</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    @endforeach
</div>
