@include('pdf.partials._styles')

<div class="container">
    <table width="100%">
        @foreach($form->statements as $statement)
            @isset($statement->title)<tr><td> @if($statement->checked) <i class="fa fa-check-square"></i> @else <i class="fa fa-square-o"></i> @endif {{ $statement->title }}</td></tr>@endisset
            <tr>
                <td>{!! \App\Services\Statements\StatementTransformer::transformForPdf($callReport, $statement->statement) !!}</td>
            </tr>
        @endforeach
    </table>

    @if($form->project->project_equipment)
        <table width="100%">
            <tr>
                <td class="text-center border bg-grey"><strong>Category</strong></td>
                <td class="text-center border bg-grey"><strong>Make/Model</strong></td>
                <td class="text-center border bg-grey"><strong>Equipment #</strong></td>
                <td class="text-center border bg-grey"><strong>Crew/Team</strong></td>
                <td class="text-center border bg-grey"><strong>Set Date</strong></td>
                <td class="text-center border bg-grey"><strong>Pick Up Date</strong></td>
            </tr>
            @foreach($form->project->project_equipment as $equipment)
                <tr>
                    <td class="text-center border">{{ $equipment->model && $equipment->model->category ? $equipment->model->category->name : 'n/a' }}</td>
                    <td class="text-center border">{{ $equipment->model ? $equipment->model->name : 'n/a' }}</td>
                    <td class="text-center border">{{ $equipment->serial ?? 'n/a' }}</td>
                    <td class="text-center border">{{ $equipment->team ? $equipment->team->name : 'n/a' }}</td>
                    <td class="text-center border">{{ $equipment->pivot->set_up_date ? \Carbon\Carbon::createFromFormat('Y-m-d', $equipment->pivot->set_up_date)->format('m/d/Y') : 'n/a' }}</td>
                    @if($equipment->status_id !== 5)
                        <td class="text-center border">{{ $equipment->pivot->pick_up_date ? \Carbon\Carbon::createFromFormat('Y-m-d', $equipment->pivot->pick_up_date)->format('m/d/Y') : 'n/a' }}</td>
                    @else
                        <td class="text-center border">MISSING</td>
                    @endif
                </tr>
            @endforeach
            <tr><td>&nbsp;</td></tr>
        </table>
    @endif

    @if($form->notes)
        <table width="100%">
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td><strong>Additional Notes:</strong></td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>
                    <strong>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $form->notes->updated_at)->format('m/d/Y H:i') }}
                        by {{ $form->notes->user ? $form->notes->user->full_name : 'n/a' }}</strong></td>
            </tr>
            <tr>
                <td>{{ $form->notes->notes }}</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    @endif

    @if($form && $form->footer_text_show)
        <table width="100%" class="footer-text">
            <tr>
                <td>{!! $form->footer_text !!}</td>
            </tr>
        </table>
    @endif
</div>
