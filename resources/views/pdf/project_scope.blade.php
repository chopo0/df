@include('pdf.partials._styles')

<style>
    .border {
        border: 1px solid black !important;
        border-collapse: collapse;
        padding: 3px;
    }
    .row-border {
        border: 1px solid black;
    }
    .bg-grey {
        background-color: #c3c3c3;
    }

    table {
        page-break-inside: avoid;
    }
    .page-break {
        page-break-after: always;
    }
    .scopes * {
        font-size: 12px;
        height: 12px;
    }
    .text-right {
        text-align: right;
    }
</style>

<div class="container">
    @foreach($form->project->areas as $area)
        <table width="100%">
            <tr class="border">
                <td colspan="8" class="text-center bg-grey border"><strong>{{ $area->standard_area->title }}</strong></td>
            </tr>
            <tr>
                <td colspan="4" class="text-center bg-grey border" width="50%"><strong>Overall Square Feet</strong></td>
                <td colspan="4" class="text-center border" width="50%">{{ $area->overal_square_feet }}</td>
            </tr>
            <tr><td colspan="8">&nbsp;</td></tr>
        </table>
        @foreach($area->scopes as $page => $areaScopes)
            <table class="scopes" width="100%">
                <tr>
                    <td colspan="4">RH = Regular Hours; AH = After Hours</td>
                    <td>&nbsp;</td>
                    <td colspan="4" class="text-right">Page Number: <strong>{{ $page }}</strong></td>
                </tr>
            @foreach($areaScopes as $scope)
                <tr>
                    @if($scope->header)
                        <td width="5%" class="text-center border bg-grey">
                            <i class="fa fa-times"></i>
                        </td>
                        <td width="32%" class="text-center border bg-grey">{{ $scope->service }}</td>
                        <td width="5%" class="text-center border bg-grey">UOM</td>
                        <td width="5%" class="text-center border bg-grey">QTY</td>
                    @else
                        <td width="5%" class="text-center border">
                            {{ $scope->selected }}
                        </td>
                        <td width="32%" class="text-center border">{{ $scope->service }}</td>
                        <td width="5%" class="text-center border">{{ $scope->uom_info ? $scope->uom_info->title : '&nbsp;' }}</td>
                        <td width="5%" class="text-center border">{{ $scope->quantity }}</td>
                    @endif
                    <td width="4%">&nbsp;</td>
                    @if($areaScopes[$loop->index + ($loop->count / 2)]->header)
                        <td width="5%" class="text-center border bg-grey">
                            <i class="fa fa-times"></i>
                        </td>
                        <td width="32%" class="text-center border bg-grey">{{ $areaScopes[$loop->index + ($loop->count / 2)]->service }}</td>
                        <td width="5%" class="text-center border bg-grey">UOM</td>
                        <td width="5%" class="text-center border bg-grey">QTY</td>
                    @else
                        <td width="5%" class="text-center border">
                            {{ $areaScopes[$loop->index + ($loop->count / 2)]->selected }}
                        </td>
                        <td width="32%" class="text-center border">{{ $areaScopes[$loop->index + ($loop->count / 2)]->service }}</td>
                        <td width="5%" class="text-center border">{{ $areaScopes[$loop->index + ($loop->count / 2)]->uom_info ? $areaScopes[$loop->index + ($loop->count / 2)]->uom_info->title : '&nbsp;' }}</td>
                        <td width="5%" class="text-center border">{{ $areaScopes[$loop->index + ($loop->count / 2)]->quantity }}</td>
                    @endif
                </tr>
                @break($loop->count / 2 === ($loop->index + 1))
            @endforeach
            </table>
        @endforeach
        <div class="page-break"></div>
    @endforeach

    <table width="100%">
        <tr class="border">
            <td colspan="8" class="text-center bg-grey border"><strong>Miscellaneous</strong></td>
        </tr>
        <tr><td colspan="8">&nbsp;</td></tr>
    </table>
    <table class="scopes" width="100%">
        <tr>
            <td colspan="4">RH = Regular Hours; AH = After Hours</td>
            <td>&nbsp;</td>
            <td colspan="4" class="text-right">Misc Page</td>
        </tr>
        @foreach($miscScopes as $scope)
            <tr>
                @if($scope->header)
                    <td width="5%" class="text-center border bg-grey">
                        <i class="fa fa-times"></i>
                    </td>
                    <td width="32%" class="text-center border bg-grey">{{ $scope->service }}</td>
                    <td width="5%" class="text-center border bg-grey">UOM</td>
                    <td width="5%" class="text-center border bg-grey">QTY</td>
                @else
                    <td width="5%" class="text-center border">
                        {{ $scope->selected }}
                    </td>
                    <td width="32%" class="text-center border">{{ $scope->service }}</td>
                    <td width="5%" class="text-center border">{{ $scope->uom_info ? $scope->uom_info->title : '&nbsp;' }}</td>
                    <td width="5%" class="text-center border">{{ $scope->quantity }}</td>
                @endif
                <td width="4%">&nbsp;</td>
                @if($miscScopes[$loop->index + ($loop->count / 2)] && $miscScopes[$loop->index + ($loop->count / 2)]->header)
                    <td width="5%" class="text-center border bg-grey">
                        <i class="fa fa-times"></i>
                    </td>
                    <td width="32%" class="text-center border bg-grey">{{ $miscScopes[$loop->index + ($loop->count / 2)]->service }}</td>
                    <td width="5%" class="text-center border bg-grey">UOM</td>
                    <td width="5%" class="text-center border bg-grey">QTY</td>
                @else
                    <td width="5%" class="text-center border">
                        {{ $miscScopes[$loop->index + ($loop->count / 2)]->selected }}
                    </td>
                    <td width="32%" class="text-center border">{{ $miscScopes[$loop->index + ($loop->count / 2)]->service }}</td>
                    <td width="5%" class="text-center border">{{ $miscScopes[$loop->index + ($loop->count / 2)]->uom_info ? $miscScopes[$loop->index + ($loop->count / 2)]->uom_info->title : '&nbsp;' }}</td>
                    <td width="5%" class="text-center border">{{ $miscScopes[$loop->index + ($loop->count / 2)]->quantity }}</td>
                @endif
            </tr>
            @break($loop->count / 2 === ($loop->index + 1))
        @endforeach
        <tr>
            <td colspan="4">&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="4">&nbsp;</td>
        </tr>
    </table>
    <div class="page-break"></div>
        @if($form->notes)
            <table width="100%">
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td><strong>Additional Notes:</strong></td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>
                        <strong>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $form->notes->updated_at)->format('m/d/Y H:i') }}
                            by {{ $form->notes->user ? $form->notes->user->full_name : 'n/a' }}</strong></td>
                </tr>
                <tr>
                    <td>{{ $form->notes->notes }}</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
        @endif
</div>
