@component('mail::layout')
{{-- Header --}}
@slot('header')
    @component('mail::header', ['url' => config('app.url')])
        {{ $company->name }}
    @endcomponent
@endslot

{{-- Body --}}

<div style="text-align: center;">
    <img src="{{ asset($company->public_logo_path) }}" height="100">
</div>

<br>
<h3 style="text-align: center;">{{ $company->name }} – {{ $callReport->insured_name }} - {{ $callReport->short_job_address }} – Claim #{{ $callReport->insurance_claim_no }}</h3>

<strong>Insured/Owner:</strong><br/>
{{ $callReport->insured_name }}<br/>
{{ $callReport->short_job_address }}<br/>
{{ $callReport->city }}, {{ $callReport->state }} {{ $callReport->zip_code }}<br/>
Email: {{ $callReport->insured_email }}<br/>
Phone: {{ $callReport->insured_cell_phone }}<br/>

<strong>Company:</strong><br/>
{{ $company->name }}<br/>
{{ $company->street }}<br/>
{{ $company->city }}, {{ $company->state }} {{ $company->zip }}<br/>
Email: {{ $company->email }}<br/>
Phone: {{ $company->phone }}<br/>

@if($attachDropboxLink)
<strong>Please  view  images  and/or  videos  for  this  project  here: <a href="{{ $url }}">{{ $url }}</a> </strong>
@endif

Sincerely,<br>
{{ $company->name }}
    {{-- Subcopy --}}
@isset($subcopy)
    @slot('subcopy')
        @component('mail::subcopy')
            {{ $subcopy }}
        @endcomponent
    @endslot
@endisset

{{-- Footer --}}
@slot('footer')
    @component('mail::footer')
        © {{ date('Y') }} {{ $company->name }}. All rights reserved.
    @endcomponent
@endslot

@endcomponent
