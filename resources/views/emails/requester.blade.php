@component('mail::layout')
{{-- Header --}}
@slot('header')
    @component('mail::header', ['url' => config('app.url')])
        {{ $company->name }}
    @endcomponent
@endslot

{{-- Body --}}
Hi,

We appreciate your business and hope we did a good job.<br>
If you don't mind, could you please leave us a review at 1 or all of the sites below as we're trying to build our online reputation

@foreach($links as $link)
<a href="{{ $link->url }}">{{ $link->url }}</a>
<br>
@endforeach

Thanks,<br>
{{ $company->name }}

{{-- Subcopy --}}
@isset($subcopy)
    @slot('subcopy')
        @component('mail::subcopy')
            {{ $subcopy }}
        @endcomponent
    @endslot
@endisset

{{-- Footer --}}
@slot('footer')
    @component('mail::footer')
        © {{ date('Y') }} {{ $company->name }}. All rights reserved.
    @endcomponent
@endslot

@endcomponent


