<nav class="navbar" id="mktg-navbar" v-cloak>
  <a class="navbar-brand" href="{{ url('/') }}">
    <img class="logo" src="/favicon.png" alt="{{ MKTG_SITE_NAME }} Logo">
  </a>
  <ul class="nav toggler">
    <li class="nav-item">
      <i class="fa fa-bars fa-2x" @click="isOpen = !isOpen"></i>
    </li>
  </ul>
  <ul class="nav links" :class="{'is-open': isOpen}">
    <li class="nav-item inner-toggler text-right">
      <a class="nav-link"><i class="fa fa-times" @click="isOpen = !isOpen"></i></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ url('/') }}">Home</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ url('/about') }}">About</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ url('/features') }}">Features</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ url('/blog') }}">Blog</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ url('/faq') }}">FAQ</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ url('/contact-us') }}">Contact Us</a>
    </li>
    <li class="nav-item login">
      <a class="nav-link call-to-act" href="{{ env('APP_URL') }}/panel#/login">Login</a>
    </li>
  </ul>
  <div class="backdrop" :class="{'is-open': isOpen}" @click="isOpen = !isOpen"></div>
  <cookie-law theme="dark-lime" button-text="Accept">
    <div slot="message">
      This website stores cookies on your computer.
      These cookies are used to improve your website experience and provide more personalized services to you,
      both on this website and through other media.
      To find out more about the cookies we use, see our <a rel="noopener" target="_blank" href="{{ url('/privacy-policy') }}">Privacy Policy</a>.
    </div>
  </cookie-law>
</nav>
<script src="/js/marketing/navbar.js"></script>
