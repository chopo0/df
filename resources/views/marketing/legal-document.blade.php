@extends('marketing.layout.marketing')

@section('body')
  <div class="bannered-page" id="mktg-legal-document">
    @include('marketing.includes.banner', $banner)
    <div class="main container">
      <article>
        {!! $document->content !!}
      </article>
    </div>
  </div>
@endsection