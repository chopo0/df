@extends('marketing.layout.marketing')

<script src='https://www.google.com/recaptcha/api.js'></script>

@section('body')
  <div id="mktg-mailing-list-subscription">
    @include('marketing.includes.banner', $banner)
    <section class="main container">

      @include('marketing.includes.alert')

      
      @unless (session()->has('message'))
        <h4 class="suez-1 mt-4 mb-4">
          Enter the email you want to be removed and follow the confirmation link we'll send you to completely unsubscribe from our mailing list.
        </h4>

        {{ Form::open(['url' => 'mailing-list-subscription/verify', 'method' => 'POST', 'novalidate' => 'novalidate']) }}
          {{ csrf_field() }}
            
          <div class="form-group">
            {{ Form::label('email', 'E-Mail Address', ['class' => 'col-md-6 control-label']) }}
            <div class="col-md-6">
              {{ Form::email('email', old('email'), ['id' => 'email', 'class' => 'form-control']) }}
              @include('marketing.includes.error-message', ['name' => 'email'])
            </div>
          </div>
            
          <div class="form-group">
            <div class="col-md-6">
              <div id="g-recaptcha" class="g-recaptcha" data-sitekey="{{ env('G_RECAPTCHA_SITE_KEY') }}"></div>
              @include('marketing.includes.error-message', ['name' => 'g-recaptcha-response'])
            </div>
          </div>

          <div class="form-group">
            <div class="col-md-6">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
        {{ Form::close() }}
      @endunless
    </section>
  </div>
  <script src="/js/marketing/mailing-list-subscription.js"></script>
@endsection