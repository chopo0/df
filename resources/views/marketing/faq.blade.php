@extends('marketing.layout.marketing')

@section('body')
  <div id="mktg-faq">
    @include('marketing.includes.banner', $banner)
    <div class="main container">
      @if(!count($faqs))
        @include('marketing.includes.empty-list')
      @endif
      @foreach ($faqs as $faq)
        <div class="row q-and-a">
          <h4 class="question suez-1">{{ $faq['question'] }}</h4><br>
          <p class="answer">{{ $faq['answer'] }}</p>
        </div>
      @endforeach
    </div>
  </div>
@endsection