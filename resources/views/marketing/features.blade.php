@extends('marketing.layout.marketing')

@section('body')
  <div id="mktg-features">
    @include('marketing.includes.banner', $banner)
    @for ($i = 0; $i < 9; $i++)
      @include('marketing.features.feature-'.($i + 1), ['feature' => $features[$i]])
    @endfor
  </div>
@endsection