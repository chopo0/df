<section id="mktg-hero">
  <div class="foreground">
    <div class="container">
      <div class="content">
        <figure>
          <img src="/assets/logo.png" alt="{{ MKTG_SITE_NAME }} Logo">
        </figure>
        <h2 class="tagline">{{ MKTG_SITE_TAGLINE }}</h2>
        <button class="btn btn-secondary action-button mb-2 call-to-act" @click="showVideo(true)">
          <i class="fa fa-play-circle mr-2"></i>Watch Video
        </button>
        <a href="{{ SIGNUP_URL }}" class="btn btn-tertiary action-button mb-2 call-to-act">
          <b></i>Get Started</b>
        </a>
      </div>
      <div class="mouse-scroll"></div>
    </div>
  </div>
  <div class="background"></div>
</section>