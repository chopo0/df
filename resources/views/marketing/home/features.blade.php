<div id="mktg-home-features">
  <div class="foreground">
    <div class="container">
      <div class="content">
        <h3>Reasons to love {{ MKTG_SITE_NAME }}</h3>
        <hr class="section-divider tertiary">
        <div class="row">
          @foreach ($features as $feature)
            <a href="{{ url('/features#' . $feature['slug']) }}" class="col-sm-4 feature">
              <span class="icon"><i class="fa {{ $feature['icon'] }} fa-2x"></i></span>
              <span class="caption">{{ $feature['caption'] }}</span><br>
            </a>
          @endforeach
        </div>
      </div>
    </div>
  </div>
  <div class="background"></div>
</div>