<div id="mktg-home-logos">
    <div class="container">
      <div class="caption text-center m-4">
        <h3 class="suez-1">{{ MKTG_SITE_NAME }} is proud to do business with the following companies:</h3>
        <hr class="section-divider tertiary">
      </div>
      <div id="company-logos" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          @foreach ($logos as $i => $logosChunk)
            @if(count($logosChunk) === 4)
              <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                <div class="row">
                  @foreach ($logosChunk as $logo)
                    <div class="column col-md-3">
                      <div class="logo">
                        <img class="d-block mx-auto" src="{{ APP_URL }}/{{ $logo->public_logo_path }}" alt="{{ $logo->name }}">
                      </div>
                    </div>
                  @endforeach
                </div>
              </div> 
            @endif
          @endforeach
        </div>
        @include('marketing.includes.carousel-control', ['type' => 'prev', 'id' => 'company-logos' ])
        @include('marketing.includes.carousel-control', ['type' => 'next', 'id' => 'company-logos' ])
      </div>
    </div>
  </div>