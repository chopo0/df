<div id="mktg-home-pricing">
  <div class="foreground">
    <div class="content">
      <h3 class="suez-1 caller">Unlimited Projects.<br> Unlimited Users.</h3>
      <h5 class="message">Transform the way you do business today and<br>get access to all
          <b>{{ MKTG_SITE_NAME }}</b> features for only:
      </h5>
      <h1 class="price suez-1">${{$setting->package_price}} / month</h1>
      <br>
      <a href="{{ SIGNUP_URL }}" class="btn btn-tertiary get-started call-to-act">
        <b>Get Started</b>
      </a>
    </div>
  </div>
</div>