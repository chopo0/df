<section id="mktg-mailing-list-banner" class="{{ !empty($clipped) ? 'clipped' : ''}}">
  <div class="container">
    <div class="content">
      <h3 class="suez-1 main heading">Get the latest {{ MKTG_SITE_NAME }} related news right to your inbox.</h3>
      <hr class="section-divider tertiary">
      <h5 class="sub heading">We'll send you relevant emails only. Absolutely no spamming.</h5>
      <br>
      <a href="{{ url('/mailing-list-subscription') }}">
        <button class="btn btn-tertiary call-to-act">
          <b>Subscribe to our Mailing List</b>
        </button>
      </a>
    </div>
  </div>
</section>