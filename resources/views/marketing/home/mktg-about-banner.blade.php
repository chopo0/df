<div id="mktg-about-banner">
  <div class="container">
    <div class="row">
      <div class="col-md-6 left">
        <div class="content">
          <h3>Dry Forms Plus is the solution for you.</h3>
          <p>Dry Forms Plus is the perfect document and equipment management solution for any serious restoration contractor. </p>
          <b>Transform the way you do business today!</b>
        </div>
      </div>
      <div class="col-md-6 right">
        <figure>
          <img src="/assets/images/mktg-about-banner.png" alt="">
        </figure>
      </div>
      <div class="col-12 bottom">
        <a href="{{ url('/about') }}">
          <button class="btn btn-secondary call-to-act">More about {{ MKTG_SITE_NAME }}</button>
        </a>
      </div>
    </div>
  </div>
</div>