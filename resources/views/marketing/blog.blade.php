@extends('marketing.layout.marketing')

@section('body')
  <div id="mktg-blog">
    @include('marketing.includes.banner', $banner)
    <div class="container">
      @if(!count($articles))
        @include('marketing.includes.empty-list')
      @endif
      <div class="row">
        @foreach ($articles as $article)
          @if ($loop->first)
            <div class="col-12 top">
              @include('marketing.blog.article-card', ['article' => $article, 'first' => 1])
            </div>
          @endif
          @if($loop->index > 0)
            <div class="col-12 col-sm-6 col-md-4">
              @include('marketing.blog.article-card', ['article' => $article, 'first' => 0])
            </div>
          @endif
        @endforeach
      </div>
    </div>
    <br>
    {{ $articles->links('pagination::bootstrap-4') }}
  </div>
@endsection