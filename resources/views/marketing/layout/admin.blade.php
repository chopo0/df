<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="/favicon.png">
    <title>{{ MKTG_SITE_NAME }}</title>
    <link rel="stylesheet" href="/css/marketing/admin.css">
    <script src="/js/marketing/app.js"></script>
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
</head>
<body>
    @include('marketing.admin.navbar')
    <div id="mktg-admin">
      @yield('body')
    </div>
</body>
</html>