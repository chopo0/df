<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="/favicon.png">
    
    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}
    
    <link rel="stylesheet" href="/css/marketing/app.css">
</head>
<body>
    @include('marketing.navbar')

    @yield('body')

    @includeWhen(request()->path() !== MAILING_LIST_PATH
        && request()->path() !== MAILING_LIST_CANCEL_PATH,
        'marketing.home.mailing-list-banner')

    @if (!empty($recentArticles) && count($recentArticles))
        @if (request()->route()->getName() === ARTICLE_ROUTE_NAME)
            @include('marketing.includes.recent-articles')
        @else
            @includeWhen(count($recentArticles) == 3, 'marketing.includes.recent-articles')
        @endif
    @endif
    
    @includeWhen(!empty($countries), 'marketing.includes.contact-us-link')

    @include('marketing.footer')

    <script src="/js/marketing/app.js"></script>
</body>
</html>