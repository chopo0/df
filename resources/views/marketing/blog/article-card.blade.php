<div class="card article-card">
  <div class="article-thumbnail" style="background-image: url('{{ $first ? $article->cover : $article->thumbnail }}')"></div>
  <div class="card-body">
    <small class="card-subtitle mt-1 mb-2 text-muted d-block">
      {{ $article->published_on }} • <a href="{{ url('/') }}">{{ MKTG_SITE_NAME }} Team</a>
    </small>
    <h5 class="card-title suez-1 piped">
      {{ strlen($article->title) > 80 ? substr($article->title, 0, 80).'...' : $article->title }}
    </h5>
    <div class="read-more-btn {{ $first ? 'first' : '' }}">
      <a href="{{ url('/blog/article/'.$article->id.'/'.$article->slug) }}" class="btn btn-primary call-to-act">Read More</i></a>
    </div>
  </div>
</div>