@extends('marketing.layout.marketing')

@section('body')
  <div id="mktg-about">
    @include('marketing.includes.banner', $banner)
    <section class="container">
      @foreach ($data as $datum)
        <article>
          <header>
            <h4 class="suez-1">{{ $datum['header'] }}</h4>
          </header>
          <p>{{ $datum['body'] }}</p>
        </article><br>
      @endforeach
    </section>
  </div>
@endsection