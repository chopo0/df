@extends('marketing.layout.marketing')

<script src='https://www.google.com/recaptcha/api.js'></script>

@section('body')
  <div id="mktg-contact-us">
    @include('marketing.includes.banner', $banner)
    <section class="main container">
      
      @include('marketing.includes.alert')

      @unless (session()->has('message'))
        <h4 class="mb-4 suez-1 side-piped">
          Submit your contact details and we'll get back to you as soon as possible!
        </h4>
        {{ Form::open(['url' => 'contact-us', 'method' => 'POST', 'novalidate' => 'novalidate']) }}
          {{ csrf_field() }}
            
          <div class="form-group">
            {{ Form::label('full-name', 'Full Name', ['class' => 'col-md-6 control-label']) }}
            <div class="col-md-6">
              {{ Form::text('full_name', old('full_name'), ['id' => 'full-name', 'class' => 'form-control']) }}
              @include('marketing.includes.error-message', ['name' => 'full_name'])
            </div>
          </div>
            
          <div class="form-group">
            {{ Form::label('email', 'E-Mail Address', ['class' => 'col-md-6 control-label']) }}
            <div class="col-md-6">
              {{ Form::email('email', old('email'), ['id' => 'email', 'class' => 'form-control']) }}
              @include('marketing.includes.error-message', ['name' => 'email'])
            </div>
          </div>
            
          <div class="form-group">
            {{ Form::label('contact-number', 'Contact Number', ['class' => 'col-md-6 control-label']) }}
            <div class="col-md-6">
              {{ Form::number('contact_number', old('contact_number'), ['id' => 'contact-number', 'class' => 'form-control']) }}
              @include('marketing.includes.error-message', ['name' => 'contact_number'])
            </div>
          </div>

          <div class="form-group">
            {{ Form::label('description', 'Company Name', ['class' => 'col-md-6 control-label']) }}
            <div class="col-md-6">
              {{ Form::text('company_name', old('company_name'), ['id' => 'company_name', 'class' => 'form-control']) }}
            </div>
          </div>

          <div class="form-group">
            <label for="message" class="col-md-6 control-label">Message
              <span class="text-success pull-right" :class="{'text-danger': msgLength > 1000 || !msgLength}" v-cloak>@{{ msgLength }}/1000</span>
            </label>
            <div class="col-md-6">
              {{ Form::textarea('message', old('message'), ['id' => 'message', 'class' => 'form-control', 'ref' => 'message', 'rows' => '5', '@input' => 'onInput($event)']) }}
              @include('marketing.includes.error-message', ['name' => 'message'])
            </div>
          </div>
            
          <div class="form-group">
            <div class="col-md-6">
              <div id="g-recaptcha" class="g-recaptcha" data-sitekey="{{ env('G_RECAPTCHA_SITE_KEY') }}"></div>
              @include('marketing.includes.error-message', ['name' => 'g-recaptcha-response'])
            </div>
          </div>

          <div class="form-group">
            <div class="col-md-6">
              <button type="submit" class="btn btn-primary call-to-act">
                Submit Contact Info
              </button>
            </div>
          </div>
        {{ Form::close() }}
      @endunless
    </section>
  </div>
  <script src="/js/marketing/contact-us.js"></script>
@endsection