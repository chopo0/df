@include('marketing.features.carousel', ['id' => 'project-scope-left', 'folder' => 'project-scope', 'class' => 'col-md-6 left'])
<div class="col-md-6 feature description">
  <section>
    <header><h5 class="suez-1 side-piped">Create Unlimited Custom Forms</h5></header>
    @include('marketing.features.carousel', ['id' => 'project-scope-right', 'folder' => 'project-scope', 'class' => 'right'])
    <p class="text">We understand that there are an enormous number of forms in use throughout the restoration industry. This is why we place no limit on the number of contract forms you can create.</p>
  </section>
</div>