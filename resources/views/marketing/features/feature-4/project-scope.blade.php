@include('marketing.features.carousel', ['id' => 'project-scope-left', 'folder' => 'project-scope', 'class' => 'col-md-6 left'])
<div class="col-md-6 feature description">
  <section>
    <header><h5 class="suez-1 side-piped">Project Scope Standard Forms</h5></header>
    @include('marketing.features.carousel', ['id' => 'project-scope-right', 'folder' => 'project-scope', 'class' => 'right'])
    <p class="text">Our Project Scope Form functions like a checklist and saves lots of time at the job site. Once setup, your technician will simply go down the list for each affected area and check the items they have used at the job site, making it easy for your billing department to build your estimates. Either use our standard form or generate your own to enter in the affected area’s line items. On the standard, drag and drop all group headers and item descriptions however you want, and if you have any extra information, just add a note at the end of the form. After you create a new project and add in all its affected areas, the system will automatically generate scope forms using your standard.</p>
  </section>
</div>