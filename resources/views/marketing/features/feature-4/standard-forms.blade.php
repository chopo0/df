<div class="col-md-6 feature description">
  <section>
    <header><h5 class="suez-1 side-piped">Legal Standard Forms</h5></header>
    @include('marketing.features.carousel', ['id' => 'project-scope-right', 'folder' => 'project-scope', 'class' => 'right'])
    <p class="text">Customizability is key to building forms that properly fit your company. That’s why we give you the ability to use, edit or delete and replace any of our provided text. We have standard templates for work authorization forms, anti-microbial authorization forms, customer responsibility forms, release from liability forms, work stoppage forms, and certificate of completion forms. You can obtain electronic signatures for each of these on-site.</p>
  </section>
</div>
@include('marketing.features.carousel', ['id' => 'project-scope-left', 'folder' => 'project-scope', 'class' => 'col-md-6 left'])