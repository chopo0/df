<section class="main container four" id="{{ $feature['slug'] }}">
  <header>
    <h4 class="suez-1 side-piped">{{ $feature['caption'] }}</h4>
  </header>
  <p class="text">{{ $feature['content'] }}</p>
  <p class="text">{{ $feature['note'] }}</p>
  <div class="row inner-feature">
    @include('marketing.features.feature-4.project-scope')
    @include('marketing.features.feature-4.standard-forms')
    @include('marketing.features.feature-4.custom-forms')
  </div>
</section>