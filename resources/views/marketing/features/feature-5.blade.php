<section class="main container" id="{{ $feature['slug'] }}">
  <header>
    <h4 class="suez-1 side-piped">{{ $feature['caption'] }}</h4>
  </header>
  <p class="text">{{ $feature['texts'][0] }}</p>
  <div class="row">
    @foreach ( $feature['items'] as $item )
      <div class="col-12 col-md-6 feature description inner-feature">
        <section>
          <header><h5 class="suez-1 side-piped">{{ $item['caption'] }}</h5></header>
          <figure class="inner">
            <img class="feature-5" src="{{ $item['imgSrc'] }}" alt="{{ $item['imgAlt'] }}">
          </figure>
          <p class="text">{{ $item['text'] }}</p>
        </section>
      </div>
    @endforeach
  </div>
</section>