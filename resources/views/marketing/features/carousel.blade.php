<div id="{{ $id }}" class="carousel slide {{ !empty($class) ? $class : '' }}" data-ride="carousel">
  <div class="carousel-inner">
    @for ($i = 0; $i < 4; $i++)
      <div class="carousel-item {{ !$i ? 'active' : '' }}">
        <div class="row">
          <div class="col-12 text-center">
            <img src="/assets/images/{{ $folder }}/image-{{ $i }}.png" alt="">
          </div>
        </div>
      </div>
    @endfor
  </div>
  @include('marketing.includes.carousel-control', ['type' => 'prev', 'id' => $id ])
  @include('marketing.includes.carousel-control', ['type' => 'next', 'id' => $id ])
</div>