<section class="main container feature-7" id="{{ $feature['slug'] }}">
  <header>
    <h4 class="suez-1 side-piped">{{ $feature['caption'] }}</h4>
  </header>
  <p class="text">{{ $feature['content'] }}</p>
  <div class="row">
    <div class="col-12 col-md-6 feature description">
      <section>
        <figure>
          <img src="{{ $feature['imgSrc'] }}" alt="{{ $feature['imgAlt'] }}">
        </figure>
        <ul class="mktg-list">
          @foreach ($feature['items'] as $item)
            <li class="text">
              <i class="fa fa-check-circle"></i>{{ $item }}
            </li>
          @endforeach
        </ul>
      </section>
    </div>
    @include('marketing.features.image', ['imgSrc' => $feature['imgSrc'], 'imgAlt' => $feature['imgAlt']])
  </div>
</section>