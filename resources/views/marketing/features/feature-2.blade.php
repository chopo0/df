<section class="main container" id="{{ $feature['slug'] }}">
  <div class="row">
    @include('marketing.features.image', ['imgSrc' => $feature['imgSrc'], 'imgAlt' => $feature['imgAlt']])
    <div class="col-12 col-md-6 feature description">
      <section>
        <header>
          <h4 class="suez-1 side-piped">{{ $feature['caption'] }}</h4>
        </header>
        <figure>
          <img src="{{ $feature['imgSrc'] }}" alt="{{ $feature['imgAlt'] }}">
        </figure>
        <p class="text">{{ $feature['content'] }}</p>
      </section>
    </div>
  </div>
</section>