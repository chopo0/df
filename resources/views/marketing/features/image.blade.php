<div class="feature image {{ !empty($class) ? $class : 'col-md-6' }}">
  <figure>
    <img src="{{ $imgSrc }}" alt="{{ $imgAlt }}">
  </figure>
</div>