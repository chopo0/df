<div class="filter">
  <span class="text-secondary">Filter: &nbsp;</span>
  @switch(app('request')->input('filter'))
    @case(MESSAGE_READ)
      <a href="{{ route('cms.contact-people', ['filter' => MESSAGE_UNREAD]) }}">Show Unread Only</a>&nbsp;|&nbsp;
      <a href="{{ route('cms.contact-people') }}">Show All</a>
      @break

    @case(MESSAGE_UNREAD)
      <a href="{{ route('cms.contact-people', ['filter' => MESSAGE_READ]) }}">Show Read Only</a>&nbsp;|&nbsp;
      <a href="{{ route('cms.contact-people') }}">Show All</a>
      @break
    @default
      <a href="{{ route('cms.contact-people', ['filter' => MESSAGE_UNREAD]) }}">Show Unread Only</a>&nbsp;|&nbsp;
      <a href="{{ route('cms.contact-people', ['filter' => MESSAGE_READ]) }}">Show Read Only</a>
  @endswitch
</div>