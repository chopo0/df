<div class="modal fade" id="delete-mdl{{$id}}" tabindex="-1" role="dialog" aria-labelledby="delete-mdl-lbl" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="delete-mdl-lbl">Delete Record</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Are you sure you want to delete this record?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-dark" data-dismiss="modal">Cancel</button>
        {{ Form::open(['url' => $url.$id, 'method' => 'DELETE']) }}
            {{ Form::submit('Yes, proceed', ['class' => 'btn btn-danger']) }}
        {{ Form::close() }}
      </div>
    </div>
  </div>
</div>