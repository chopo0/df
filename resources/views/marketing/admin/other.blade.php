@extends('marketing.layout.admin')

@section('body')
  <div class="container" id="mktg-admin-other-settings">
    <h4 class="suez-1 side-piped">Other Settings</h4>

    @include('marketing.includes.alert', ['class' => 'col-md-6 mb-0'])

    {{ Form::open(['url' => '/cms/other-settings/edit', 'method' => 'POST', 'class' => 'form-horizontal', 'novalidate' => 'novalidate']) }}
      {{csrf_field()}}

      <h5 class="suez-1 side-piped secondary">Home</h5>
      <div class="form-group">
        {{ Form::label('intro-video', 'Introduction Video (Youtube Video ID)', ['class' => 'col-md-6 control-label']) }}
        <div class="col-md-6">
          {{ Form::text('intro_video', old('intro_video', $setting->intro_video), ['id' => 'intro-video', 'class' => 'form-control', !empty($isEditing) ? '' : 'disabled']) }}
          @include('marketing.includes.error-message', ['name' => 'intro_video'])
        </div>
      </div>
      
      <h5 class="suez-1 side-piped secondary">Pricing</h5>
      <div class="form-group">
        {{ Form::label('package-price', 'Package Price', ['class' => 'col-md-6 control-label']) }}
        <div class="col-md-6">
          {{ Form::number('package_price', old('package_price', $setting->package_price), ['id' => 'package-price', 'class' => 'form-control', !empty($isEditing) ? '' : 'disabled', 'step' => "1"]) }}
          @include('marketing.includes.error-message', ['name' => 'package_price'])
        </div>
      </div>
      
      <h5 class="suez-1 side-piped secondary">Contact Us</h5>
      <div class="form-group">
        {{ Form::label('notification-receiver', 'Notification Receiver', ['class' => 'col-md-6 control-label']) }}
        <div class="col-md-6">
          {{ Form::text('notification_receiver', old('notification_receiver', $setting->notification_receiver), ['id' => 'notification-receiver', 'class' => 'form-control', !empty($isEditing) ? '' : 'disabled']) }}
          @include('marketing.includes.error-message', ['name' => 'notification_receiver'])
        </div>
      </div>
        
      <div class="form-group">
        <div class="col-md-6">
          @if(!empty($isEditing))
            <a href="{{url("/cms/other-settings")}}" class="btn btn-dark">Cancel</a>
            {{ Form::submit('Save Changes', ['class' => 'btn btn-secondary']) }}
          @else
            <a href="{{url("/cms/other-settings/edit")}}" class="btn btn-secondary">Edit</a>
          @endif
        </div>
      </div>
    </form>
  </div>
  <script src="/js/marketing/admin/contact-people.js"></script>
@endsection