@extends('marketing.layout.admin')

@section('body')
  <div id="mktg-blog-article">
    @include('marketing.includes.article', ['content' => $article->article])
  </div>
@endsection