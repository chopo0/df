@extends('marketing.layout.admin')

@section('body')
  <div class="container mb-5" id="mktg-admin-blog">
    <h4 class="suez-1 side-piped mb-4">Blog Articles
      <a href="/cms/blog-article/create" class="btn btn-success btn-sm pull-right call-to-act">
        <i class="fa fa-plus mr-1"></i>Add New
      </a>
    </h4>

    @include('marketing.includes.alert', ['class' => 'mb-1'])
    @if(!count($articles))
      @include('marketing.includes.empty-list')
    @endif
    
    <div class="main container">
      @foreach ($articles as $article)
        <div class="row mb-3 mt-3">
          <div class="col-md-4 col-lg-3">
            <img class="mb-2" src="{{ $article->thumbnail }}" style="width: 100%">
          </div>
          <div class="col-md-8 col-lg-9">
            <span class="badge {{ $article->published ? 'badge-success' : 'badge-dark' }}">Published</span>
            <h4 class="suez-1">{{ $article->title }}</h4>
            <p class="m-0"><b class="mb-0">{{ $article->sub_title }}</b></p>
            <a href="/cms/blog-article/{{$article->id}}/{{$article->slug}}" class="btn btn-success btn-sm">
              <i class="fa fa-eye mr-1"></i>View
            </a>
            <a href="/cms/blog-article/edit/{{$article->id}}" class="btn btn-primary btn-sm">
              <i class="fa fa-edit mr-1"></i>Edit
            </a>
            <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="{{'#delete-mdl'.$article->id}}"
              @click="instanceId = {!! $article->id !!}">
              <i class="fa fa-trash mr-1"></i>Delete
            </button>
          </div>
        </div>
        @include('marketing.admin.delete-mdl', ['id' => $article->id, 'url' => '/cms/blog-article/delete/'])
        <br>
      @endforeach
    </div>
    {{ $articles->links('pagination::bootstrap-4') }}
  </div>
@endsection