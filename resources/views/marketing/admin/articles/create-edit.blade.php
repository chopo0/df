@extends('marketing.layout.admin')

@section('body')
  <script src="{{ url('/vendor/backpack/tinymce/tinymce.min.js') }}"></script>
  <div class="container" id="mktg-admin-articles">
    <h4 class="suez-1 side-piped">{{ empty($article) ? 'Add New Article' : 'Edit Article' }}</h4>
    
    @if(empty($article))
      {{ Form::open(['url' => 'cms/blog-article', 'method' => 'POST', 'class' => 'form-horizontal', 'novalidate' => 'novalidate']) }}
    @else
      {{ Form::open(['url' => 'cms/blog-article/'.$article->id, 'method' => 'PUT', 'class' => 'form-horizontal', 'novalidate' => 'novalidate']) }}
    @endif
    
    {{csrf_field()}}
    
    <div class="form-group">
      <label for="title" class="control-label">Title</label>
      {{ Form::text('title', old('title', !empty($article) ? $article->title : ''), ['id' => 'title', 'class' => 'form-control']) }}
      @include('marketing.includes.error-message', ['name' => 'title'])
    </div>

    <div class="form-group">
      <label for="sub-title" class="control-label">Subtitle</label>
      {{ Form::text('sub_title', old('sub_title', !empty($article) ? $article->sub_title : ''), ['id' => 'sub-title', 'class' => 'form-control']) }}
      @include('marketing.includes.error-message', ['name' => 'sub_title'])
    </div>

    <div class="form-group">
      <label for="sub-title" class="control-label">Cover Image</label>
        <img src="{{ old('cover', !empty($article) ? $article->cover : '') }}" id="cover-preview" class="image-thumbnail" style="display: block; max-height:100px;">
      <div class="input-group mt-2">
          <button type="button" id="lfm" data-input="cover" data-preview="cover-preview" class="btn btn-primary">
            <i class="fa fa-upload mr-1"></i>Upload a cover image
          </button>
        {{ Form::text('cover', old('cover', !empty($article) ? $article->cover : ''), ['id' => 'cover', 'class' => 'form-control', 'hidden']) }}
      </div>
      @include('marketing.includes.error-message', ['name' => 'cover'])
    </div>
    
    <div class="form-group">
      <label for="message" class="control-label">Article</label>
      {{ Form::textarea('article', old('article', !empty($article) ? $article->article : ''), ['id' => 'article', 'class' => 'form-control']) }}
      @include('marketing.includes.error-message', ['name' => 'article'])
    </div>

    <div class="form-group">
      <div class="form-check">
        {{ Form::checkbox('published', true, old('published', !empty($article) ? $article->published : ''), ['id' => 'published']) }}
        <label class="form-check-label" for="published">Publish article</label>
      </div>
    </div>

    <div class="form-group">
      <a href="/cms/blog-article" class="btn btn-dark">Cancel</a>
      @if(empty($article))
        {{ Form::submit('Create', ['class' => 'btn btn-secondary']) }}
      @else
        {{ Form::submit('Save Changes', ['class' => 'btn btn-secondary']) }}
      @endif
    </div>
    </form>
  </div>
  <script src="/js/marketing/admin/article.js"></script>
@endsection