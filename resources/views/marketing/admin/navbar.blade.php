<nav class="navbar" id="mktg-navbar" v-cloak>
  <a class="navbar-brand" href="{{ url('/') }}">
    <img class="logo" src="/favicon.png" alt="{{ MKTG_SITE_NAME }} Logo">
  </a>
  <ul class="nav toggler">
    <li class="nav-item">
      <i class="fa fa-bars fa-2x" @click="isOpen = !isOpen"></i>
    </li>
  </ul>
  <ul class="nav links" :class="{'is-open': isOpen}">
    <li class="nav-item inner-toggler text-right">
      <a class="nav-link"><i class="fa fa-times" @click="isOpen = !isOpen"></i></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ url('/cms/contact-people') }}">Contacts</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ url('/cms/mailing-list') }}">Mailing List</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ url('/cms/blog-article') }}">Blog</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ url('/cms/faq') }}">FAQ</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ url('/cms/country') }}">Countries</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ url('/cms/other-settings') }}">Settings</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ url('/cms/legal-documents') }}">Legal Docs</a>
    </li>
    <li class="nav-item login">
      <a class="nav-link call-to-act" href="{{ url('/admin') }}">Dashboard</a>
    </li>
  </ul>
  <div class="backdrop" :class="{'is-open': isOpen}" @click="isOpen = !isOpen"></div>
</nav>
<script src="/js/marketing/navbar.js"></script>