@extends('marketing.layout.admin')

@section('body')
  <div class="container" id="mktg-admin-legal-documents">
    <h4 class="suez-1 side-piped">Legal Documents</h4>
    <br>
    <div class="row">
      <div class="col-md-7">
        <div class="list-group">
          <a class="list-group-item" href="{{ url('/cms/legal-documents/' . TERMS_AND_CONDITIONS_SLUG)}}">Terms and Conditions</a>
          <a class="list-group-item" href="{{ url('/cms/legal-documents/' . PRIVACY_POLICY_SLUG)}}">Privacy Policy</a>
        </div>
      </div>
    </div>
  </div>
@endsection