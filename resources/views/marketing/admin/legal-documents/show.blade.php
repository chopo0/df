@extends('marketing.layout.admin')

@section('body')
  <div class="container padded-bottom" id="mktg-admin-legal-documents">
    <h4 class="suez-1 side-piped">{{ $document->title }}
      <a href="/cms/legal-documents/edit/{{ $document->type }}" class="btn btn-success btn-sm pull-right call-to-act">
        <i class="fa fa-edit mr-1"></i>Edit
      </a>
    </h4>
    <br>
    <article>
      {!! $document->content !!}
    </article>

  </div>
@endsection