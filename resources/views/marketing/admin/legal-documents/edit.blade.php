@extends('marketing.layout.admin')

@section('body')
  <script src="{{ url('/vendor/backpack/tinymce/tinymce.min.js') }}"></script>
  <div class="container padded-bottom" id="mktg-admin-legal-documents">
    <h4 class="suez-1 side-piped">Edit {{ $document->title }}</h4>
    <br>

    {{ Form::open(['url' => 'cms/legal-documents', 'method' => 'PUT', 'class' => 'form-horizontal', 'novalidate' => 'novalidate']) }}
      <div class="form-group">
        <label for="message" class="control-label">Content</label>
        {{ Form::textarea('content', old('content', $document->content), ['id' => 'content', 'class' => 'form-control']) }}
        @include('marketing.includes.error-message', ['name' => 'content'])
      </div>

      {{ Form::hidden('type', $document->type) }}
      @include('marketing.includes.error-message', ['name' => 'type'])

      <div class="form-group">
        <a href="/cms/legal-documents/{{ str_slug($document->title) }}" class="btn btn-dark">Cancel</a>
        {{ Form::submit('Save Changes', ['class' => 'btn btn-secondary']) }}
      </div>
    {{ Form::close() }}
  </div>
  <script src="/js/marketing/admin/legal-document.js"></script>
@endsection