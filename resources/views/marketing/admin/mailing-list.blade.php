@extends('marketing.layout.admin')

@section('body')
  <div class="container" id="mktg-admin-mailing-list">
    <h4 class="suez-1 side-piped">Mailing List
      <a href="/cms/mailing-list/download-csv" class="btn btn-success btn-sm pull-right call-to-act">
        <i class="fa fa-download mr-1"></i>Download CSV
      </a>
    </h4><br>
    
    @include('marketing.includes.alert')
    
    <div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
              <th scope="col">Subscription Date</th>
            <th scope="col">E-Mail Address</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @if(!count($mailingListItems))
            <tr>
              <td colspan="100%">
                @include('marketing.includes.empty-list')
              </td>
            </tr>
          @endif
          @foreach ($mailingListItems as $item)
            <tr>
                <td>{{$item->created_at->format('M. d, Y h:ia')}}</td>
              <td scope="row">{{$item->email}}</td>
              <td>
                <button class="btn badge badge-danger" type="button" data-toggle="modal" data-target="{{'#delete-mdl'.$item->id}}" title="Delete">
                  <i class="fa fa-trash"></i>
                </button>
              </td>
            </tr>
            @include('marketing.admin.delete-mdl', ['id' => $item->id, 'url' => '/cms/mailing-list/delete/'])
          @endforeach
        </tbody>
      </table>
      {{$mailingListItems->appends(['filter' => request('filter')])->links('pagination::bootstrap-4')}}
    </div>
  </div>
@endsection