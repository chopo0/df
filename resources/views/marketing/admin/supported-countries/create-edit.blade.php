@extends('marketing.layout.admin')

@section('body')
  <div class="container">
    <h4 class="suez-1 side-piped">{{empty($country) ? 'Add New Country' : 'Edit Country'}}</h4>
    @if(empty($country))
      {{ Form::open(['url' => 'cms/country', 'method' => 'POST', 'class' => 'form-horizontal', 'novalidate' => 'novalidate']) }}
    @else
      {{ Form::open(['url' => 'cms/country/'.$country->id, 'method' => 'PUT', 'class' => 'form-horizontal', 'novalidate' => 'novalidate']) }}
    @endif
      {{csrf_field()}}
      <div class="form-group">
        <label for="name" class="control-label">Country Name</label>
        {{ Form::text('name', old('name', !empty($country) ? $country->name : ''), ['id' => 'name', 'class' => 'form-control']) }}
        @include('marketing.includes.error-message', ['name' => 'name'])
      </div>
        
      <div class="form-group">
        <div>
          <a href="/cms/country" class="btn btn-dark">Cancel</a>
          @if(empty($country))
            {{ Form::submit('Create', ['class' => 'btn btn-secondary']) }}
          @else
            {{ Form::submit('Save Changes', ['class' => 'btn btn-secondary']) }}
          @endif
        </div>
      </div>
    </form>
  </div>
@endsection