@extends('marketing.layout.admin')

@section('body')
  <div class="container" id="mktg-admin-supported-countries">
    <h4 class="suez-1 side-piped mb-4">Supported Countries
      <a href="/cms/country/create" class="btn btn-success btn-sm pull-right call-to-act">
        <i class="fa fa-plus mr-1"></i>Add New
      </a>
    </h4>

    @include('marketing.includes.alert', ['class' => 'mb-1'])
    @if(!count($supportedCountries))
      @include('marketing.includes.empty-list')
    @endif
    
    <div class="main container">
      @foreach ($supportedCountries as $country)
        <div class="row">
          <div class="col-12">
            <h4 class="suez-1 side-piped secondary">{{ $country->name }}</h4>
            <a href="/cms/country/edit/{{$country->id}}" class="btn btn-primary btn-sm">
              <i class="fa fa-edit mr-1"></i>Edit
            </a>
            @if (count($supportedCountries) > 1)
              <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="{{'#delete-mdl'.$country->id}}">
                <i class="fa fa-trash mr-1"></i>Delete
              </button>
            @endif
          </div>
        </div>
        @include('marketing.admin.delete-mdl', ['id' => $country->id, 'url' => '/cms/country/delete/'])
        <br>
      @endforeach
    </div>
  </div>
@endsection