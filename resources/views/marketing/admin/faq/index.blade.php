@extends('marketing.layout.admin')

@section('body')
  <div class="container" id="mktg-admin-faq">
    <h4 class="suez-1 side-piped mb-4">FAQs
      <a href="/cms/faq/create" class="btn btn-success btn-sm pull-right call-to-act">
        <i class="fa fa-plus mr-1"></i>Add New
      </a>
    </h4>

    @include('marketing.includes.alert', ['class' => 'mb-1'])
    @if(!count($faqs))
      @include('marketing.includes.empty-list')
    @endif
    
    <div class="main container">
      @foreach ($faqs as $faq)
        <div class="row">
          <div class="col-12">
            <h4 class="suez-1 side-piped secondary">{{ $faq['question'] }}</h4>
            <p class="mb-0">{{ $faq['answer'] }}</p>
            <a href="/cms/faq/edit/{{$faq->id}}" class="btn btn-primary btn-sm">
              <i class="fa fa-edit mr-1"></i>Edit
            </a>
            <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="{{'#delete-mdl'.$faq->id}}">
              <i class="fa fa-trash mr-1"></i>Delete
            </button>
          </div>
        </div>
        @include('marketing.admin.delete-mdl', ['id' => $faq->id, 'url' => '/cms/faq/delete/'])
        <br>
      @endforeach
    </div>
  </div>
@endsection