@extends('marketing.layout.admin')

@section('body')
  <div class="container" id="mktg-admin-faq">
    <h4 class="suez-1 side-piped">{{empty($faq) ? 'Add New FAQ' : 'Edit FAQ'}}</h4>
    @if(empty($faq))
      {{ Form::open(['url' => 'cms/faq', 'method' => 'POST', 'class' => 'form-horizontal', 'novalidate' => 'novalidate']) }}
    @else
      {{ Form::open(['url' => 'cms/faq/'.$faq->id, 'method' => 'PUT', 'class' => 'form-horizontal', 'novalidate' => 'novalidate']) }}
    @endif
      {{csrf_field()}}
      <div class="form-group">
        <label for="question" class="control-label" style="display: block">Question
          <span class="text-success pull-right" :class="{'text-danger': questionLength > 1000 || !questionLength}" v-cloak>@{{ questionLength }}/1000</span>
        </label>
        {{ Form::text('question', old('question', !empty($faq) ? $faq->question : ''), ['id' => 'question', 'class' => 'form-control',  'ref' => 'question', '@input' => 'onQuestionInput($event)']) }}
        @include('marketing.includes.error-message', ['name' => 'question'])
      </div>
      <div class="form-group">
        <label for="answer" class="control-label" style="display: block">Answer
          <span class="text-success pull-right" :class="{'text-danger': answerLength > 1000 || !answerLength}" v-cloak>@{{ answerLength }}/1000</span>
        </label>
        {{ Form::text('answer', old('answer', !empty($faq) ? $faq->answer : ''), ['id' => 'answer', 'class' => 'form-control',  'ref' => 'answer', '@input' => 'onAnswerInput($event)']) }}
        @include('marketing.includes.error-message', ['name' => 'answer'])
      </div>
        
      <div class="form-group">
        <div>
          <a href="/cms/faq" class="btn btn-dark">Cancel</a>
          @if(empty($faq))
            {{ Form::submit('Create', ['class' => 'btn btn-secondary']) }}
          @else
            {{ Form::submit('Save Changes', ['class' => 'btn btn-secondary']) }}
          @endif
        </div>
      </div>
    </form>
  </div>
  <script src="/js/marketing/admin/faq.js"></script>
@endsection