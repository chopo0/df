@extends('marketing.layout.admin')

@section('body')
  <div class="container" id="mktg-admin-contact-people">
    <h4 class="suez-1 side-piped">Contact People</h4><br>
    
    @include('marketing.includes.alert')
    @include('marketing.admin.contact-people.filter')
    
    <div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">Full Name</th>
            <th scope="col">Contact Number</th>
            <th scope="col">E-Mail Address</th>
            <th scope="col">Company Name</th>
            <th scope="col">Date and Time Sent</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @if(!count($contactPeople))
            <tr>
              <td colspan="100%">
                @include('marketing.includes.empty-list')
              </td>
            </tr>
          @endif
          @foreach ($contactPeople as $cp)
            <tr class="{{!$cp->is_read ? 'bg-primary-light' : ''}} {{isset($instance) && $instance->id == $cp->id ? 'bg-secondary-light' : ''}}">
              <td scope="row">{{$cp->full_name}}</td>
              <td>{{$cp->contact_number}}</td>
              <td>{{$cp->email}}</td>
              <td>{{$cp->company_name}}</td>
              <td>{{$cp->created_at->format('M. d, Y h:ia')}}</td>
              <td>
                <a href="{{route('cms.contact-people', ['message-id' => $cp->id] + request()->all())}}"
                  class="btn badge badge-primary" title="View Message">
                  <i class="fa fa-envelope-open"></i>
                </a>
                <button class="btn badge badge-danger" type="button" data-toggle="modal" data-target="{{'#delete-mdl'.$cp->id}}" title="Delete">
                  <i class="fa fa-trash"></i>
                </button>
              </td>
            </tr>
            @if (isset($instance) && $instance->id == $cp->id)
              <tr class="bg-secondary-light">
                <td colspan="100%" class="message">
                  <h5 class="suez-1 side-piped secondary">Message from {{$instance->full_name}}</h5>
                  <p> {{$instance->message}}</p>
                </td>
              </tr>
            @endif
            @include('marketing.admin.delete-mdl', ['id' => $cp->id, 'url' => '/cms/contact-people/delete/'])
          @endforeach
        </tbody>
      </table>
      {{$contactPeople->appends(['filter' => request('filter')])->links('pagination::bootstrap-4')}}
    </div>
  </div>
@endsection