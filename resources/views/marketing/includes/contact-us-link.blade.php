<section class="contact-us-link {{ !empty($clipped) ? 'clipped' : '' }}">
  <div class="container">
    <div class="content">
      <h3>Dry Forms Plus is the solution for you.</h3>
      <h5>Transform the way you do business today.</h5>
      <p>Currently available in the following countries:
        <span>
          <br>
          @foreach ($countries as $country)
            <b>{{ $country->name }}</b>
            @if (!$loop->last)
            •
            @endif
          @endforeach
        </span>
      </p>
      <a href="{{ url('/contact-us') }}">
        <button class="btn btn-default call-to-act">
          <b>Contact Us Now</b>
        </button>
      </a>
    </div>
  </div>
</section>