@if ($errors->has($name))
  <span class="help-block text-danger">
    <strong>{{ $errors->first($name) }}</strong>
  </span>
@endif