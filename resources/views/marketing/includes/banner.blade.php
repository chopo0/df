<section class="mktg-page-banner">
  <div class="container">
    <div class="row content">
      <div class="col-12">
        <h1 class="main title">{{ $mainTitle }}</h1>
        
        @if(isset($subTitle))
          <h4 class="sub title">{{ $subTitle }}</h4>
        @endif

        @if(isset($bannerSrc))
          <img src="{{ $bannerSrc }}" alt="{{ MKTG_SITE_NAME }} Banner">
        @endif
      </div>
    </div>
  </div>
  <div class="background"></div>
</section>