<a class="carousel-control-{{ $type }}" href="#{{ $id }}" role="button" data-slide="{{ $type }}">
  <span class="carousel-control-{{ $type }}-icon" aria-hidden="true"></span>
  <span class="sr-only">{{ $type }}</span>
</a>