<div class="container back-link">
  <a href="{{ url('/blog') }}" class="btn btn-default btn-sm call-to-act">
    <i class="fa fa-chevron-left text-dark"></i>
    Back to articles list
  </a>
</div>