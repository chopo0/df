<div class="mktg-article-cover container">
  <p class="info">
    Last updated <b>{{ $article->published_on }}</b><br>
    by the<a class="suez-1 text-dark" href="{{ url('/') }}"> Dry Forms Plus Team</a>
  </p>
  <h1 class="main title suez-1">{{ $article->title }}</h1>
  <h5 class="sub title">{{ $article->sub_title }}</h5>
  <img src="{{ $article->cover }}" alt="{{ MKTG_SITE_NAME }} Banner">
</div>