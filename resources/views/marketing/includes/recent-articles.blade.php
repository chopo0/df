<section id="recent-articles">
  <div class="container">
    <h4 class="suez-1 text-center">Latest Stories from {{ MKTG_SITE_NAME }}</h4>
    <hr class="section-divider">
    <div class="row">
      @foreach ($recentArticles as $article)
        <div class="col-12 col-lg-4 pt-2 pb-2">
          @include('marketing.blog.article-card', ['article' => $article, 'first' => 0])
        </div>
      @endforeach
    </div>
  </div>
</section>