@if (session()->has('message'))
  <div class="alert alert-success {{ !empty($class) ? $class : '' }}" role="alert">
    {{ session()->get('message') }}
  </div>
@endif

@if (session()->has('errorMessage'))
  <div class="alert alert-danger {{ !empty($class) ? $class : '' }}" role="alert">
    {{ session()->get('errorMessage') }}
  </div>
@endif

@if (session()->has('warningMessage'))
  <div class="alert alert-warning {{ !empty($class) ? $class : '' }}" role="alert">
    {{ session()->get('warningMessage') }}
  </div>
@endif