<div class="alert alert-secondary mb-0" role="alert">
  <h4 class="alert-heading">
    The list is empty.
  </h4>
  <span>No items were found.</span>
</div>