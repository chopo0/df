<div id="mktg-intro-video" v-if="videoShown" @click="showVideo(false)" v-cloak>
  <div class="intro-vid video-container">
    <iframe src="https://www.youtube.com/embed/{{ $setting->intro_video }}" frameborder="0" allowfullscreen></iframe>
  </div>
</div>