@extends('marketing.layout.marketing')

@section('body')
  <div id="mktg-blog-article">
    @include('marketing.includes.blog-back-btn')
    @include('marketing.includes.article', ['content' => $article->article])
  </div>
@endsection