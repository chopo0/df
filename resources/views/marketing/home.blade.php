@extends('marketing.layout.marketing')

@section('body')
  <div id="mktg-home">
    @include('marketing.home.hero')
    @include('marketing.home.mktg-about-banner')
    @include('marketing.home.features')
    @include('marketing.home.pricing')
    @include('marketing.home.logos')
    @include('marketing.includes.intro-video')
  </div>
  <script src="/js/marketing/home.js"></script>
@endsection