<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Dry Forms Plus Mailing List: Confirm Subscription</title>
</head>
<body>
  <h1>Confirm Subscription</h1>

  <h4>Hello! Good day!</h4>
  <p>Thank you for subscribing to the Dry Forms Plus mailing list.</p>
  <p>
    <b>To complete your subscription, confirming by clicking the link below:</b>
  </p>
  <a href="{{ url('/mailing-list-subscription/confirm/' . $mailingListItem->subs_confirmation_key) }}">
    {{ url('/mailing-list-subscription/confirm/' . $mailingListItem->subs_confirmation_key) }}
  </a>
  <p>If this is a mistake and you're not the one who requested this, no action is needed.</p>
  <p>Thanks! Have a great day!</p>
</body>
</html>