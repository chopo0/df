<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>New Dry Forms Plus Contact Person</title>
</head>
<body>
  <h1>New Contact Person!</h1>

  <h4>Hello Admin!</h4>
  <p>Someone has submitted his/her contact details and is waiting for a response:</p>
  <p><b>Contact Information:</b></p>
  <p>
    <p>
      <b>Full Name: </b><br>{{ $contactPerson->full_name }}
    </p>
    <p>
      <b>E-Mail: </b><br>{{ $contactPerson->email }}
    </p>
    <p>
      <b>Contact Number: </b><br>{{ $contactPerson->contact_number }}
    </p>
    <p>
      <b>Company Name: </b><br>{{ $contactPerson->company_name }}
    </p>
    <p>
      <b>Message: </b><br>{{ $contactPerson->message }}
    </p>
  </p>
  <p>Thanks!</p>
</body>
</html>