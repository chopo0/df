<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Dry Forms Plus Mailing List: Confirm Subscription Cancellation</title>
</head>
<body>
  <h1>Confirm Subscription Cancellation</h1>

  <h4>Hello! Good day!</h4>
  <p>You requested for the cancellation of your subscription to our mailing list.</p>
  <p>
    <b>By clicking the link below, you agree that you'll no longer receive relevant Dry Forms Plus emails.</b>
  </p>
  <a href="{{ url('/mailing-list-subscription/confirm-cancellation/' . $mailingListItem->cancellationKey) }}">
    {{ url('/mailing-list-subscription/confirm-cancellation/' . $mailingListItem->cancellationKey) }}
  </a>
  <p>If this is a mistake and you're not the one who requested this, no action is needed.</p>
  <p>Thanks! Have a great day!</p>
</body>
</html>