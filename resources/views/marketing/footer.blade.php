<footer id="mktg-footer">
  <div class="top">
    <div class="container">
      <figure>
        <a href="{{ url('/') }}">
          <img src="/favicon.png" alt="">
        </a>
      </figure>
      <div class="links">
        <a class="item" href="{{ url('/') }}">Home</a>
        <a class="item" href="{{ url('/about') }}">About</a>
        <a class="item" href="{{ url('/features') }}">Features</a>
        <a class="item" href="{{ url('/blog') }}">Blog</a>
        <a class="item" href="{{ url('/faq') }}">FAQ</a>
        <a class="item" href="{{ url('/mailing-list-subscription') }}">Mailing List</a>
        <a class="item" href="{{ url('/contact-us') }}">Contact Us</a>
      </div>
      <div class="social">
        <a rel="noopener" target="_blank" href="{{ FACEBOOK_LINK }}">
          <i class="fa fa-facebook"></i>
        </a>
        <a rel="noopener" target="_blank" href="{{ LINKEDIN_LINK }}">
          <i class="fa fa-linkedin"></i>
        </a>
      </div>
    </div>
  </div>
  <div class="bottom">
    <a class="item" href="{{ url('/') }}">&copy; Dry Forms Plus 2018</a>
    <span class="middot">&middot;</span>
    <a class="item" rel="noopener" target="_blank" href="{{ url('/terms-and-conditions') }}">Terms and Conditions</a>
    <span class="middot">&middot;</span>
    <a class="item" rel="noopener" target="_blank" href="{{ url('/privacy-policy') }}">Privacy Policy</a>
  </div>
</footer>