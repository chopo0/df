import Vue from 'vue';

new Vue({
  el: '#mktg-mailing-list-subscription',
  mounted() {
    this.setReCAPTCHAWidth();
  },
  methods: {
    setReCAPTCHAWidth() {
      let gReCAPTCHA = document.getElementById('g-recaptcha');
      const width = gReCAPTCHA.clientWidth;
      if (width < 302) {
        var scale = width / 302;
        gReCAPTCHA.style.transform = `scale(${scale})`;
        gReCAPTCHA.style.webkitTransform = `scale(${scale})`;
        gReCAPTCHA.style.transformOrigin = '0 0';
        gReCAPTCHA.style.webkitTransformOrigin = '0 0';
      }
    }
  }
});
