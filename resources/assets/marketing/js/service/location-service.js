class LocationService {
  static getLocation() {
    return location.protocol + '//' + location.host + location.pathname;
  }
}

export default LocationService;