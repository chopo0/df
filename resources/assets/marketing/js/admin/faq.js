import Vue from 'vue';

new Vue({
  el: '#mktg-admin-faq',
  data() {
    return {
      questionLength: 0,
      answerLength: 0
    }
  },
  mounted() {
    this.questionLength = this.$refs.question.value.length
    this.answerLength = this.$refs.answer.value.length
  },
  methods: {
    onQuestionInput(e) {
      this.questionLength = e.target.value.trim().length
    },
    onAnswerInput(e) {
      this.answerLength = e.target.value.trim().length
    }
  }
});