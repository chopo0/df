import Vue from 'vue';

new Vue({
  el: '#mktg-home',
  data() {
    return {
      videoShown: false
    };
  },
  methods: {
    showVideo(bool) {
      this.videoShown = bool;
    }
  }
});
