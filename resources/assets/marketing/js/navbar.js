import Vue from 'vue';
import CookieLaw from 'vue-cookie-law';

new Vue({
  el: '#mktg-navbar',
  components: { CookieLaw },
  data() {
    return {
      isOpen: false
    };
  }
});
