import axios from 'axios'
import apiAuth from '../api/auth'
import errorHandler from './error-handler'
import {createToken} from 'vue-stripe-elements-plus'
import DataProcessMixin from './data-process'

export default {
    mixins: [errorHandler, DataProcessMixin],
    data() {
        return {}
    },
    methods: {
        logout() {
            apiAuth.logout().then(res => {
                axios.defaults.headers.common['Authorization'] = null
                this.$session.destroy()
                this.$localStorage.remove('apiToken')
                window.location.replace('/')
            })
        },
        login() {
            this.run()
            let self = this
            apiAuth.login(this.user)
                .then(response => {
                    this._setToken(response)
                    self.$emit('data-ready')
                    location.reload()
                })
                .catch(response => {
                    self.$emit('data-failed')
                    this.handleErrorResponse(response)
                })
        },
        register() {
            this.$validator.validateAll()
            this.firstTouchState = false
            if (this.errors.any() || !this.complete || !this.agreement || !this.user.state) {
                if (this.user.cardnumber === null) this.user.cardnumber = false
                if (this.user.cardexpiry === null) this.user.cardexpiry = false
                if (this.user.cardcvc === null) this.user.cardcvc = false
                if (this.user.postalcode === null) this.user.postalcode = false
                return
            }
            this.run()
            let self = this
            createToken().then(data => {
                this.$session.start()
                this.user.stripeToken = data.token
                apiAuth.register(this.user)
                    .then(response => {
                        let self = this
                        self.$localStorage.set('apiToken', response.data.token)
                        axios.defaults.headers.common['Authorization'] = 'Bearer ' + response.data.token
                        if (self.$route.query.redirect) {
                            self.$router.push(self.$route.query.redirect)
                        } else {
                            self.$router.push('/')
                        }
                        self.$emit('data-ready')
                    })
                    .catch(response => {
                        this.$session.destroy()
                        self.$emit('data-failed')
                        this.handleErrorResponse(response)
                    })
            }).catch(response => {
                this.$session.destroy()
                this.isLoaded = true
                this.handleErrorResponse(response.error)
            })
        },
        _setToken(response) {
            this.$session.start()
            // this.$session.set('apiToken', response.data.token)
            this.$localStorage.set('apiToken', response.data.token)
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + response.data.token
            if (this.$route.query.redirect) {
                this.$router.push(this.$route.query.redirect)
            } else {
                this.$router.push('/')
            }
        }
    }
}
