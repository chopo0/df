import axios from 'axios'

const standardFormPreview = 'api/standard/form/preview'

export default {
    preview(id) {
        return axios.get(standardFormPreview + '/' + id)
    }
}
