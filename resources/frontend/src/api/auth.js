import axios from 'axios'

const loginRoute = '/api/login'
const registerRoute = '/api/register'
const logoutRoute = '/api/logout'
const setPasswordRoute = '/api/set-password'
const resetPasswordRoute = '/api/reset-password'

export default {
    login (data) {
        return axios.post(loginRoute, data)
    },
    register(data) {
        return axios.post(registerRoute, data)
    },
    logout() {
        return axios.post(logoutRoute)
    },
    setPassword(data) {
        return axios.post(setPasswordRoute, data)
    },
    resetPassword(data) {
        return axios.post(resetPasswordRoute, data)
    }
}
