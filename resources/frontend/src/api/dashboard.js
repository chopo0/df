import axios from 'axios'

const dashboardResource = '/api/dashboard'

export default {
    projects(data) {
        return axios.get(dashboardResource + '/projects', {params: data})
    },
    equipment(data) {
        return axios.get(dashboardResource + '/equipment', {params: data})
    }
}
