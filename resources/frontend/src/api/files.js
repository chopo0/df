import axios from 'axios'

const resource = '/api/project/files'

export default {
    show(id) {
        return axios.get(resource + '/' + id)
    },
    update(id, data) {
        return axios.patch(resource + '/' + id, data)
    }
}
