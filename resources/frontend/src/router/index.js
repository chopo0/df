import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '@/components/main/Dashboard'
import Login from '@/components/auth/Login'
import Register from '@/components/auth/Register'
import CreditCard from '@/components/auth/CreditCard'
import PasswordSet from '@/components/auth/PasswordSet'
import PasswordReset from '@/components/auth/PasswordReset'

import settings from './settings'
import equipment from './equipment'
import projects from './projects'
import standards from './standards'
import forms from './forms'
import training from './training'

Vue.use(VueRouter)

const router = new VueRouter({
    routes: [
        {
            path: '/',
            name: 'Dashboard',
            component: Dashboard
        },
        {
            path: '/login',
            name: 'Login',
            component: Login
        },
        {
            path: '/register',
            name: 'Register',
            component: Register
        },
        {
            path: '/set-password',
            name: 'Set Password',
            component: PasswordSet
        },
        {
            path: '/reset-password',
            name: 'Reset Password',
            component: PasswordReset
        },
        {
            path: '/company/credit-card',
            name: 'CreditCard Register',
            component: CreditCard
        },
        {
            path: '/calendar',
            name: 'Calendar',
            props: {title: 'Calendar'},
            component: resolve => {
                require(['../components/standards/right-link/Calendar.vue'], resolve)
            },
            meta: {
                leftLinks: [
                    {
                        path: '/calendar',
                        name: 'Go Back'
                    }
                ]
            }
        },
        ...projects(),
        ...settings(),
        ...equipment(),
        ...standards(),
        ...forms(),
        ...training()
    ]
})

export default router
