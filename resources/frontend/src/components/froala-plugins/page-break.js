/* eslint-disable */
$.FroalaEditor.DefineIcon('page-break', {NAME: 'chain-broken'});
$.FroalaEditor.RegisterCommand('page-break', {
    title: 'Page break',
    focus: true,
    undo: true,
    refreshAfterCallback: true,
    callback: function () {
        this.html.insert('<div id="page-break" style="width: 100%; height: 2px; border: 1px dotted black;"></div><br>');
    }
});
