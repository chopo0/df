/* eslint-disable */
$.FroalaEditor.RegisterCommand('Statements', {
    title: 'Statements',
    type: 'dropdown',
    focus: false,
    undo: false,
    refreshAfterCallback: true,
    options: {
        "<div><h5><i class='fa fa-square-o' data-id='accept'></i> Accept</h5><p>Statement</p></div>": "Accept",
        "<div><h5><i class='fa fa-square-o' data-id='decline'></i> Decline</h5><p>Statement</p></div>": 'Decline',
        "<div><h5><span data-id='initials'>_________</span> Initial</h5><p>Statement</p></div>": 'Initial',
        "<div><h5><i class='fa fa-square-o' data-id='yes'></i> Yes <i class='fa fa-square-o' data-id='no'></i> No</h5><p>Statement</p></div>": 'Yes/No',
    },
    callback: function (cmd, val) {
        this.html.insert(val)
    }
})
