/* eslint-disable */
$.FroalaEditor.RegisterCommand('Blocks', {
    title: 'Blocks',
    type: 'dropdown',
    focus: false,
    undo: false,
    refreshAfterCallback: true,
    options: {
        "<div id='header-placeholder' style='text-align: center; border: 1px dotted black; height: 120px;'><br><strong>Header content will be placed here automatically</strong></div><p></p>": "Header",
        "<div id='signatures-placeholder' style='text-align: center; border: 1px dotted black; height: 120px;'><br><strong>Signatures will be placed here automatically</strong></div><p></p>": 'Signatures',
    },
    callback: function (cmd, val) {
        this.html.insert(val)
    }
})
