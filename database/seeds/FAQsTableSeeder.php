<?php

use Illuminate\Database\Seeder;
use App\Providers\Marketing\FAQProvider;

class FAQsTableSeeder extends Seeder
{

    /**
     * @var FAQProvider
     */
    private $faqProvider;

    /**
     * FAQsTableSeeder constructor
     * 
     * @param FAQProvider $faqProvider
     */
    public function __construct(FAQProvider $faqProvider)
    {
        $this->faqProvider = $faqProvider;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('faqs')->truncate();

        $data = $this->faqProvider->list();

        \DB::table('faqs')->insert($data);
    }
}
