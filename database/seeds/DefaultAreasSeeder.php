<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DefaultAreasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $areas = json_decode(file_get_contents('database/seeds/support/areas.json'), true);

        Schema::disableForeignKeyConstraints();

        DB::table('default_areas')->truncate();
        DB::table('default_areas')->insert($areas);

        Schema::enableForeignKeyConstraints();
    }
}
