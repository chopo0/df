<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class StructuresAndMaterialsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $structures = json_decode(file_get_contents('database/seeds/support/structures.json'), true);
        $materials = json_decode(file_get_contents('database/seeds/support/materials.json'), true);

        Schema::disableForeignKeyConstraints();
        DB::table('default_materials')->truncate();
        DB::table('default_materials')->insert($materials);

        DB::table('default_structures')->truncate();
        DB::table('default_structures')->insert($structures);
        Schema::enableForeignKeyConstraints();
    }
}
