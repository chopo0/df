<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class DefaultScopeSeeder extends Seeder
{
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('default_scopes')->truncate();

        $dataToInsert = $this->getInsertData();

        DB::table('default_scopes')->insert($dataToInsert);
        Schema::enableForeignKeyConstraints();
    }

    /**
     * @return array
     */
    private function getInsertData(): array
    {
        $json = file_get_contents('database/seeds/support/scopes.json');

        return json_decode($json, true);
    }
}
