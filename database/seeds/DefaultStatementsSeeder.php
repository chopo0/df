<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DefaultStatementsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statements = json_decode(file_get_contents('database/seeds/support/statements.json'), true);

        Schema::disableForeignKeyConstraints();
        DB::table('default_statements')->truncate();
        DB::table('default_statements')->insert($statements);
        Schema::enableForeignKeyConstraints();
    }
}
