<?php

use Illuminate\Database\Seeder;
use App\Providers\Marketing\LegalDocumentProvider;

class LegalDocumentsTableSeeder extends Seeder
{
    /**
     * @var LegalDocumentProvider
     */
    private $provider;

    /**
     * FAQsTableSeeder constructor
     * 
     * @param LegalDocumentProvider $provider
     */
    public function __construct(LegalDocumentProvider $provider)
    {
        $this->provider = $provider;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'legal_documents';

        \DB::table($table)->truncate();

        $data = $this->getList();

        \DB::table($table)->insert($data);
    }

    public function getList()
    {
        $abbrevs = [
            PRIVACY_POLICY_ABBREV,
            TERMS_AND_CONDITIONS_ABBREV
        ];

        $list = [];

        foreach ($abbrevs as $abbrev) {
            $list[] = [
                'type' => $abbrev,
                'content' => $this->provider->getDefaultContent($abbrev)
            ];
        }

        return $list;
    }
}
