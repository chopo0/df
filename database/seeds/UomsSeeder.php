<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class UomsSeeder extends Seeder
{
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('units_of_measure')->truncate();

        $dataToInsert = $this->getInsertData();

        DB::table('units_of_measure')->insert($dataToInsert);
        Schema::enableForeignKeyConstraints();
    }

    /**
     * @return array
     */
    private function getInsertData(): array
    {
        $json = file_get_contents('database/seeds/support/uoms.json');

        return json_decode($json, true);
    }
}
