<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DefaultTeamsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teams = [
            [
                'name' => 'Team 1',
            ],
            [
                'name' => 'Team 4',
            ],
            [
                'name' => 'Team 2',
            ],
            [
                'name' => 'Team 3',
            ],
        ];

        Schema::disableForeignKeyConstraints();

        DB::table('default_teams')->truncate();
        DB::table('default_teams')->insert($teams);

        Schema::enableForeignKeyConstraints();
    }
}
