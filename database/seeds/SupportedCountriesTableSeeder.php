<?php

use Illuminate\Database\Seeder;
use App\Models\Marketing\SupportedCountry;

class SupportedCountriesTableSeeder extends Seeder
{

    /**
     * @var SupportedCountry
     */
    private $supportedCountry;

    /**
     * SupportedCountriesTableSeeder constructor
     * 
     * @param SupportedCountry $supportedCountry
     */
    public function __construct(SupportedCountry $supportedCountry)
    {
        $this->supportedCountry = $supportedCountry;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->supportedCountry->truncate();
        $this->supportedCountry->create(['name' => 'United States']);
    }
}
