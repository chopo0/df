<?php

use Illuminate\Database\Seeder;

class MarketingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(FAQsTableSeeder::class);
        $this->call(SupportedCountriesTableSeeder::class);
        $this->call(LegalDocumentsTableSeeder::class);
    }
}
