<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIntroVideoToMarketingSettings extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('marketing_settings', function (Blueprint $table) {
      $table->string('intro_video');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('marketing_settings', function (Blueprint $table) {
      $table->dropColumn('intro_video');
    });
  }
}
