<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterScopesTableQuantity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('default_scopes', function (Blueprint $table) {
            $table->string('quantity')->nullable()->default(null)->change();
        });

        Schema::table('standard_scopes', function (Blueprint $table) {
            $table->string('quantity')->nullable()->default(null)->change();
        });

        Schema::table('project_scopes', function (Blueprint $table) {
            $table->string('quantity')->nullable()->default(null)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
