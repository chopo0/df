<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqsTable extends Migration {
  public function up() {
    Schema::create('faqs', function (Blueprint $table) {
      $table->increments('id');
      $table->string('question', 1000);
      $table->string('answer', 1000);
      $table->timestamps();
      $table->softDeletes();
    });
  }

  public function down() {
    Schema::dropIfExists('faqs');
  }
}