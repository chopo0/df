<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketingSettingsTable extends Migration {
  public function up() {
    Schema::create('marketing_settings', function (Blueprint $table) {
      $table->increments('id');
      $table->string('package_price')->nullable();
      $table->string('notification_receiver')->nullable();
      $table->timestamps();
    });
  }

  public function down() {
    Schema::dropIfExists('marketing_settings');
  }
}
