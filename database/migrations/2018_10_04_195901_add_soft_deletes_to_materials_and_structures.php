<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDeletesToMaterialsAndStructures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('standard_materials', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('standard_structures', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('standard_materials', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('standard_structures', function (Blueprint $table) {
            $table->softDeletes();
        });
    }
}
