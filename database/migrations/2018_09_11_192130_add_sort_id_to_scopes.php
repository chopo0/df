<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSortIdToScopes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('default_scopes', function (Blueprint $table) {
            $table->integer('sort_id')->nullable()->default(null);
        });

        Schema::table('standard_scopes', function (Blueprint $table) {
            $table->integer('sort_id')->nullable()->default(null);
        });

        Schema::table('project_scopes', function (Blueprint $table) {
            $table->integer('sort_id')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
