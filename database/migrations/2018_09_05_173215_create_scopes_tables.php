<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScopesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::dropIfExists('project_scopes');
        Schema::dropIfExists('standard_scopes');
        Schema::dropIfExists('default_scopes');

        Schema::create('default_scopes', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('selected')->default(false);
            $table->boolean('header')->default(false);
            $table->string('service')->nullable();
            $table->integer('page');
            $table->integer('uom')->unsigned()->nullable();
            $table->integer('quantity')->unsigned()->nullable();

            $table->foreign('uom')
                ->references('id')
                ->on('units_of_measure')
                ->onDelete('cascade');
        });

        Schema::create('standard_scopes', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('selected')->default(false);
            $table->boolean('header')->default(false);
            $table->string('service')->nullable();
            $table->integer('page');
            $table->integer('uom')->unsigned()->nullable();
            $table->integer('quantity')->unsigned()->nullable();
            $table->integer('company_id')->unsigned();

            $table->foreign('uom')
                ->references('id')
                ->on('units_of_measure')
                ->onDelete('cascade');

            $table->foreign('company_id')
                ->references('id')
                ->on('companies')
                ->onDelete('cascade');
        });

        Schema::create('project_scopes', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('selected')->default(false);
            $table->boolean('header')->default(false);
            $table->string('service')->nullable();
            $table->integer('page');
            $table->integer('uom')->unsigned()->nullable();
            $table->integer('quantity')->unsigned()->nullable();
            $table->integer('company_id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->integer('project_area_id')->unsigned()->nullable();
            $table->boolean('uom_from_standard')->default(false);
            $table->boolean('service_from_standard')->default(false);

            $table->foreign('uom')
                ->references('id')
                ->on('units_of_measure')
                ->onDelete('cascade');

            $table->foreign('company_id')
                ->references('id')
                ->on('companies')
                ->onDelete('cascade');

            $table->foreign('project_id')
                ->references('id')
                ->on('projects')
                ->onDelete('cascade');

            $table->foreign('project_area_id')
                ->references('id')
                ->on('project_areas')
                ->onDelete('cascade');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_scopes');
        Schema::dropIfExists('standard_scopes');
        Schema::dropIfExists('default_scopes');
    }
}
