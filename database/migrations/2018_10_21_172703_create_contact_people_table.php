<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactPeopleTable extends Migration {
  public function up() {
    Schema::create('contact_people', function (Blueprint $table) {
      $table->increments('id');
      $table->string('full_name');
      $table->string('email');
      $table->string('contact_number');
      $table->string('company_name')->nullable();
      $table->string('message', 1000);
      $table->boolean('is_read')->default(false);
      $table->timestamps();
      $table->softDeletes();
    });
  }

  public function down() {
    Schema::dropIfExists('contact_people');
  }
}