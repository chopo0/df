<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToMailingListItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mailing_list_items', function (Blueprint $table) {
            $table->text('subs_cancellation_key')->nullable();
            $table->text('subs_confirmation_key')->nullable();
            $table->boolean('confirmed')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mailing_list_items', function (Blueprint $table) {
            $table->dropColumn('subs_cancellation_key');
            $table->dropColumn('subs_confirmation_key');
            $table->dropColumn('confirmed');
        });
    }
}
