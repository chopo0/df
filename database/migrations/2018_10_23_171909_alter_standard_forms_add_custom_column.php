<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterStandardFormsAddCustomColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('standard_forms', function (Blueprint $table) {
            $table->boolean('custom')->default(false);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('standard_forms', function (Blueprint $table) {
            $table->dropColumn('custom');
            $table->dropSoftDeletes();
        });
    }
}
