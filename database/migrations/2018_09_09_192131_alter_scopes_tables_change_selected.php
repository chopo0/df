<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterScopesTablesChangeSelected extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('default_scopes', function (Blueprint $table) {
            $table->string('selected')->nullable()->default(null)->change();
        });

        Schema::table('standard_scopes', function (Blueprint $table) {
            $table->string('selected')->nullable()->default(null)->change();
        });

        Schema::table('project_scopes', function (Blueprint $table) {
            $table->string('selected')->nullable()->default(null)->change();
        });

        \DB::unprepared('UPDATE default_scopes SET selected = NULL;');
        \DB::unprepared('UPDATE standard_scopes SET selected = NULL;');
        \DB::unprepared('UPDATE project_scopes SET selected = NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
