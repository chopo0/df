<?php

return [
    'meta' => [
    /*
     * The default configurations to be used by the meta generator.
     */
        'defaults' => [
            'title' => MKTG_SITE_NAME, // set false to total remove
            'description' => MKTG_SITE_TAGLINE, // set false to total remove
            'separator' => ' - ',
            'keywords' => [MKTG_SITE_NAME, MKTG_SITE_TAGLINE, MKTG_SHORT_NAME, 'Dry Logs'],
            'canonical' => false, // Set null for using Url::current(), set false to total remove
        ],

        /*
         * Webmaster tags are always added.
         */
        'webmaster_tags' => [
            'google' => null,
            'bing' => null,
            'alexa' => null,
            'pinterest' => null,
            'yandex' => null,
        ],
    ],
    'opengraph' => [
    /*
     * The default configurations to be used by the opengraph generator.
     */
        'defaults' => [
            'title' => MKTG_SITE_NAME, // set false to total remove
            'description' => MKTG_SITE_TAGLINE, // set false to total remove
            'url' => FACEBOOK_LINK, // Set null for using Url::current(), set false to total remove
            'type' => false,
            'site_name' => MKTG_SITE_NAME,
            'images' => [APP_FAVICON, APP_LOGO],
        ],
    ],
    'twitter' => [
    /*
     * The default values to be used by the twitter cards generator.
     */
        'defaults' => [
          //'card'        => 'summary',
          //'site'        => '@LuizVinicius73',
        ],
    ],
];
