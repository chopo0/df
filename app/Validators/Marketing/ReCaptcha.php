<?php

namespace App\Validators\Marketing;

use GuzzleHttp\Client;

class ReCaptcha {

  public function validate($attribute, $value, $parameters, $validator) {

    $client = new Client();
    $response = $client->post(G_RECAPTCHA_VERIFY_SITE, [
        'form_params'=> [ 'secret' => env('G_RECAPTCHA_SECRET_KEY'), 'response' => $value ]
    ]);

    return json_decode((string)$response->getBody())->success;
  }

}