<?php

const APP_URL = 'https://dryformsplus.com';
const SIGNUP_URL = 'https://dryformsplus.com/panel#/register';
const LOGIN_URL = 'https://dryformsplus.com/panel#/login';
const APP_FAVICON = 'https://dryformsplus.com/favicon.png';
const APP_LOGO = 'https://dryformsplus.com/assets/logo.png';

const ARTICLE_ROUTE_NAME = 'article';
const MAILING_LIST_PATH = 'mailing-list-subscription';
const MAILING_LIST_CANCEL_PATH = 'mailing-list-subscription/cancel';

const MKTG_SITE_NAME = 'Dry Forms Plus';
const MKTG_SHORT_NAME = 'dryformsplus';
const MKTG_SITE_TAGLINE = 'A Document and Equipment Management System for Restoration Contractors';

const MKTG_FEATURE_1 = 'Easily Manage your Projects';
const MKTG_FEATURE_2 = 'Intuitive Dashboard';
const MKTG_FEATURE_3 = 'All of your Documents in one Place';
const MKTG_FEATURE_4 = 'Simply Customize to Fit your Company';
const MKTG_FEATURE_5 = 'Easy to Use Dry Logs';
const MKTG_FEATURE_6 = 'Efficiently Inventory and Track all your Equipment';
const MKTG_FEATURE_7 = 'Easy to use iOS and Android Apps';
const MKTG_FEATURE_8 = 'Seamlessly Share Forms, Photos and Videos';
const MKTG_FEATURE_9 = 'Automatically Send out Company Review Request Emails';

const ADMIN_EMAIL = 'dryformsplus@gmail.com';
const PACKAGE_PRICE = '75';
const INTRO_VIDEO = 'qBIgoRN2GrQ';

const MESSAGE_READ = 'read';
const MESSAGE_UNREAD = 'unread';

const G_RECAPTCHA_VERIFY_SITE = 'https://www.google.com/recaptcha/api/siteverify';
const COMPANY_LOGOS_API_URL = 'https://dryformsplus.com/api/companies/logos';

const LFM_PATH = '/laravel-filemanager/public/storage/images/shares/';

const PRIVACY_POLICY_SLUG = 'privacy-policy';
const PRIVACY_POLICY_ABBREV = 'PP';
const PRIVACY_POLICY = 'Privacy Policy';

const TERMS_AND_CONDITIONS_SLUG = 'terms-and-conditions';
const TERMS_AND_CONDITIONS_ABBREV = 'TC';
const TERMS_AND_CONDITIONS = 'Terms and Conditions';

const LINKEDIN_LINK = 'https://www.linkedin.com/company/35457017';
const FACEBOOK_LINK = 'https://web.facebook.com/dryformsplus';