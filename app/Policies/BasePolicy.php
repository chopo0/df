<?php

namespace App\Policies;

use App\Models\User;

class BasePolicy
{
    public function before(User $user, $ability)
    {
        if ($user->hasRole('Admin')) {
            return true;
        }
    }
}
