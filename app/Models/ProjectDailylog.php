<?php

namespace App\Models;

use App\Traits\BelongsToCompany;
use App\Traits\Dates;
use Illuminate\Database\Eloquent\Model;

class ProjectDailylog extends Model
{
    use BelongsToCompany, Dates;

    /**
     * @var string
     */
    public $table = 'project_dailylogs';

    /**
     * @var array
     */
    public $fillable = [
        'company_id',
        'user_id',
        'form_id',
        'project_id',
        'is_copied',
        'notes'
    ];

    /**
     * @var array
     */
    public $visible = [
    	'id',
        'company_id',
        'user_id',
        'form_id',
        'project_id',
        'is_copied',
        'notes',
        'updated_at',

        'company',
        'default_form_data',
        'standard_form',
        'project',
        'user'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function default_form_data()
    {
        return $this->hasOne(DefaultFromData::class, 'form_id', 'form_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function standard_form()
    {
        return $this->hasOne(StandardForm::class, 'form_id', 'form_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
