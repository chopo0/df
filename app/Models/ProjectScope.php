<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\BelongsToCompany;

class ProjectScope extends Model
{
    use BelongsToCompany;
    /**
     * @var string
     */
    public $table = 'project_scopes';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    public $fillable = [
    	'project_id',
        'company_id',
        'selected',
        'service',
        'header',
        'quantity',
        'uom',
        'page',
        'uom_from_standard',
        'service_from_standard',
        'project_area_id',
        'sort_id'
    ];

	/**
     * @var array
     */
    public $visible = [
    	'id',
        'project_id',
        'company_id',
        'selected',
        'service',
        'header',
        'quantity',
        'uom',
        'page',
        'uom_from_standard',
        'service_from_standard',
        'project_area_id',

        'uom_info',
        'project',
        'project_area',
        'sort_id'
    ];

    /**
     * @var array
     */
    public $casts = [
        'uom_from_standard' => 'boolean',
        'service_from_standard' => 'boolean',
        'header' => 'boolean',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project_area()
    {
        return $this->belongsTo(ProjectArea::class, 'project_area_id');
    }


    /**
     * Relation with unit_of_measure.
     */
    public function uom_info()
    {
        return $this->belongsTo(UnitOfMeasure::class, 'uom');
    }
}
