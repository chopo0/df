<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property string question
 * @property string answer
 */
class FAQ extends SoftDeletingModel {

  /**
   * @var string
   */
  protected $table = 'faqs';
}
