<?php

namespace App\Models\Marketing;

class Article extends SoftDeletingModel
{
    /**
     * Formats published_on attribute from 'Y-m-d' to 'M d, Y'
     * 
     * @return string
     */
    public function getPublishedOnAttribute()
    {
        $date = date_create($this->attributes['published_on']);
        return $this->published_on = date_format($date, "M d, Y");
    }
}
