<?php

namespace App\Models\Marketing;

/**
 * @property int id
 * @property string full_name
 * @property string email
 * @property string contact_number
 * @property string company_name
 * @property string message
 * @property bool is_read
 */
class ContactPerson extends SoftDeletingModel {
}
