<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Iatstuti\Database\Support\CascadeSoftDeletes;

class SoftDeletingModel extends Model {
  use SoftDeletes, CascadeSoftDeletes;
  
  /**
   * @var array
   */
  protected $dates = ['deleted_at'];

  /**
   * @var array
   */
  protected $guarded = ['id'];
}
