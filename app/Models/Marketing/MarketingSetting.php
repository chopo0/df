<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property string package_price
 * @property string notification_receiver
 */
class MarketingSetting extends Model {

  /**
   * @var array
   */
  protected $guarded = ['id'];
}
