<?php

namespace App\Models;

use App\Traits\BelongsToCompany;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Equipment
 * @package App\Models
 *
 * @property int id
 * @property int status_id
 * @property int model_id
 * @property int category_id
 * @property int project_id
 * @property int team_id
 * @property int company_id
 * @property string serial
 * @property string location
 * @property string set_up_date
 */
class Equipment extends Model
{
    use BelongsToCompany, SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'equipments';

    /**
     * @var array
     */
    protected $fillable = [
        'model_id',
        'category_id',
        'status_id',
        'team_id',
        'serial',
        'location',
        'company_id',
        'project_id'
    ];

    protected $visible = [
        'id',
        'model_id',
        'category_id',
        'project_id',
        'status_id',
        'team_id',
        'serial',
        'location',
        'company_id',
        'set_up_date',
        'pivot',

        'model',
        'status',
        'category',
        'team',
        'company',
        'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function model()
    {
        return $this->belongsTo(EquipmentModel::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
