<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DefaultScope extends Model
{
    /**
     * @var string
     */
    public $table = 'default_scopes';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    public $fillable = [
        'header',
        'selected',
        'service',
        'quantity',
        'uom',
        'page',
        'sort_id'
    ];

    /**
     * @var array
     */
    public $visible = [
        'id',
        'selected',
        'service',
        'header',
        'quantity',
        'uom',
        'page',
        'sort_id'
    ];

    /**
     * @var array
     */
    public $casts = [
        'header' => 'boolean',
    ];

    /**
     * Relation with unit_of_measure.
     */
    public function uom_info()
    {
        return $this->belongsTo(UnitOfMeasure::class, 'uom');
    }
}
