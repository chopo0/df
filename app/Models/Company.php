<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property User user
 */
class Company extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'companies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'logo',
        'name',
        'email',
        'street',
        'city',
        'state',
        'zip',
        'phone',
        'dropbox_access_token',
        'dropbox_token',
        'skip_disclaimer',
        'review_requester_status',
        'permit_to_use_logo'
    ];

    /**
     * @var array
     */
    protected $visible = [
        'id',
        'logo',
        'public_logo_path',
        'name',
        'email',
        'street',
        'city',
        'state',
        'zip',
        'phone',
        'dropbox_access_token',
        'dropbox_token',
        'skip_disclaimer',
        'review_requester_status',
        'permit_to_use_logo',
        'full_address'
    ];

    /**
     * @var array
     */
    protected $appends = [
        'public_logo_path',
        'full_address'
    ];

    /**
     * @var array
     */
    public $casts = [
        'skip_disclaimer' => 'boolean',
        'permit_to_use_logo' => 'boolean',
    ];

    /**
     * Relation with user.
     */
    public function user()
    {
        return $this->belongsTo(User::class)->with('role');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class)->with('role');
    }

    /**
     * Relation with employees.
     */
    public function employees()
    {
        return $this->hasMany(User::class, 'company_id');
    }

    /**
     * @return string
     */
    public function getFullAddressAttribute()
    {
        $addressSegments = [
            $this->street,
            $this->city,
            $this->state,
            $this->zip
        ];

        return implode(', ', $addressSegments);
    }

    /**
     * @return null|string
     */
    public function getPublicLogoPathAttribute()
    {
        return $this->logo ? "storage/$this->logo" : null;
    }
}
