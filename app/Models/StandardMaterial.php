<?php

namespace App\Models;

use App\Traits\BelongsToCompany;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StandardMaterial extends Model
{
    use BelongsToCompany, SoftDeletes;
    /**
     * @var string
     */
    public $table = 'standard_materials';

    /**
     * @var array
     */
    public $fillable = ['title', 'type', 'company_id'];

    /**
     * @var array
     */
    public $visible = [
        'id',
        'title',
        'company_id'
    ];
}
