<?php

namespace App\Models;

use App\Traits\BelongsToCompany;
use Illuminate\Database\Eloquent\Model;

class StandardScope extends Model
{
    use BelongsToCompany;

    /**
     * @var string
     */
    public $table = 'standard_scopes';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    public $fillable = [
        'company_id',
        'selected',
        'service',
        'header',
        'quantity',
        'uom',
        'page',
        'sort_id'
    ];

	/**
     * @var array
     */
    public $visible = [
        'id',
        'company_id',
        'selected',
        'service',
        'header',
        'quantity',
        'page',
        'uom',
        'sort_id',

        'uom_info'
    ];

    /**
     * @var array
     */
    public $casts = [
        'header' => 'boolean',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Relation with unit_of_measure.
     */
    public function uom_info()
    {
        return $this->belongsTo(UnitOfMeasure::class, 'uom');
    }
}
