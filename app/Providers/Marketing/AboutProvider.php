<?php

namespace App\Providers\Marketing;

class AboutProvider {

  /**
   * @return array
   */
  public static function getAboutDetails() {
    return [
      [
        'header' => 'Our services are completely unmatched.',
        'body' => 'Between our website and mobile app, you’ll experience an infinitely faster, more efficient way to manage your company’s documents and meet your company’s needs. Track your equipment inventory in real-time. Document your project information and notes on our digital call reports and daily logs. Streamline your entire company with professional, fully-customizable standard project scopes and contract forms.'
      ], [
        'header' => 'No matter what kind of project you’re working on, our software will work for you.',
        'body' => 'There’s no limit to the amount you can achieve with Dry Forms Plus technology—and we mean that literally. The number of contract forms you can create is totally unlimited, so you can handle all the paperwork you could possibly need!'
      ], [
        'header' => 'Our software gives you the ability to track your entire equipment inventory in real time.',
        'body' => 'You can improve your productivity while keeping organized like never before. You’ll also be able to collect on-site electronic signatures, moisture maps, and psychometric report readings. And when the job is complete, our review requester will automatically send your clients a request to leave an online review.'
      ], [
        'header' => 'Dry Forms Plus is also fully integrated with all the latest online technology.',
        'body' => 'Not only will you be able to easily print and email all your documents, but you’ll also be able to instantly save them all to the Dropbox cloud. This makes it simpler than ever before to upload project documents to your estimates or invoices, while also keeping all your pictures and videos organized and easily accessible.'
      ],
    ];
  }
}