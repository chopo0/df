<?php

namespace App\Providers\Marketing;

class FeatureProvider
{

  /**
   * @return array
   */
  public static function getFeatures()
  {
    return [
      [
        'icon' => 'fa-refresh',
        'caption' => MKTG_FEATURE_1,
        'slug' => str_slug(MKTG_FEATURE_1, '-'),
        'imgSrc' => '/assets/images/easily-manage.png',
        'imgAlt' => MKTG_SITE_NAME . ' Feature: ' . MKTG_FEATURE_1,
        'items' => [
          'View all in-progress, completed and deleted projects at a glance',
          'Add unlimited teams and technicians',
          'Track equipment',
          'Schedule and add new projects ',
          'Manage each project’s forms, photos, videos and equipment using your computer or mobile device',
          'Customize and create standard forms',
          'Obtain electronic signatures on-site',
          'Automatically request reviews from your customers',
          'Toggle between measuring systems (Fahrenheit/Celsius, customary/metric)',
          'Obtain driving directions with Google Maps',
          'Back up everything with Dropbox',
        ]
      ], [
        'icon' => 'fa-pie-chart',
        'caption' => MKTG_FEATURE_2,
        'slug' => str_slug(MKTG_FEATURE_2, '-'),
        'imgSrc' => '/assets/images/mktg-about-banner.png',
        'imgAlt' => MKTG_SITE_NAME . ' Feature: ' . MKTG_FEATURE_2,
        'content' => 'Our dashboard displays the most pertinent information about your company status. Easily see the distribution of projects between teams and track your equipment inventory. We provide the tools you need to monitor performance by the month or year.'
      ], [
        'icon' => 'fa-check',
        'slug' => str_slug(MKTG_FEATURE_3, '-'),
        'imgSrc' => '/assets/images/mktg-about-banner.png',
        'imgAlt' => MKTG_SITE_NAME . ' Feature: ' . MKTG_FEATURE_3,
        'caption' => MKTG_FEATURE_3,
        'content' => 'There are so many different forms at use in the restoration industry today, it can sometimes be difficult to manage and maintain them all. That’s why our system is here to help. Gone are the days of having to carry around paper documents. Our easy-to-use system and mobile app allows you to store all forms, photos and videos for each of your projects in one place:',
        'items' => [
          'Call report, Project scope and Daily log forms',
          'Work authorization forms',
          'Anti-microbial authorization forms',
          'Customer responsibility forms',
          'Moisture maps and Psychometric reports',
          'Release from liability and Work stoppage forms',
          'Certificate of completion forms',
          'Custom forms and Project photos, videos',
        ]
      ], [
        'icon' => 'fa-suitcase',
        'slug' => str_slug(MKTG_FEATURE_4, '-'),
        'caption' => MKTG_FEATURE_4,
        'content' => 'All of our customers are provided with a fully-customizable set of standard forms, and you’ll be able to use, edit or delete and replace any text you want to fit your company’s needs. You can also create unlimited new standard forms, which will be used to generate all newly-created project forms.',
        'note' => '(Note that any legal documents you use should always be reviewed by an attorney.)',
      ], [
        'icon' => 'fa-edit',
        'slug' => str_slug(MKTG_FEATURE_5, '-'),
        'imgSrc' => '/assets/images/mktg-about-banner.png',
        'imgAlt' => MKTG_SITE_NAME . ' Feature: ' . MKTG_FEATURE_5,
        'caption' => MKTG_FEATURE_5,
        'texts' => ['Taking proper dry log readings is a critical part of the mitigation and drying process. We provide two different dry logs, moisture maps and psychometric reports, and our system will automatically generate dry logs for every affected area you add.'],
        'items' => [
          [
            'imgSrc' => '/assets/images/moisture-map.png',
            'imgAlt' => MKTG_SITE_NAME . ' Feature: ' . MKTG_FEATURE_5,
            'caption' => 'Moisture Maps',
            'text' => 'Our moisture maps let you easily take moisture readings for each of your project’s affected areas, and also give you the ability to choose your desired structures and materials.'
          ], [
            'imgSrc' => '/assets/images/psychometric-report.png',
            'imgAlt' => MKTG_SITE_NAME . ' Feature: ' . MKTG_FEATURE_5,
            'caption' => 'Psychometric Reports',
            'text' => 'Our psychometric reports allow you to take temperature and relative humidity readings for up to five locations per affected area. GPP and dewpoint readings are calculated automatically, and if necessary, you can also assign containment areas.'
          ],
        ]
      ], [
        'icon' => 'fa-table',
        'slug' => str_slug(MKTG_FEATURE_6, '-'),
        'imgSrc' => '/assets/images/inventory.png',
        'imgAlt' => MKTG_SITE_NAME . ' Feature: ' . MKTG_FEATURE_6,
        'caption' => MKTG_FEATURE_6,
        'content' => 'Our software allows teams to efficiently track inventory on any computer or mobile device. Effortlessly add, remove and assign numbers to any and all equipment, and keep track of where it all is by digitally assigning it to your teams. You can also change equipment status as each team sets and picks up equipment at different job sites. Mark pieces of equipment as out of commission when they’re no longer working properly. Loaning equipment to another company? You’ll still never miss a beat. Just change the equipment’s status and location, and you’ll always know exactly where it is.'
      ], [
        'icon' => 'fa-mobile',
        'slug' => str_slug(MKTG_FEATURE_7, '-'),
        'imgSrc' => '/assets/images/android-ios.png',
        'imgAlt' => MKTG_SITE_NAME . ' Feature: ' . MKTG_FEATURE_7,
        'caption' => MKTG_FEATURE_7,
        'content' => 'Our state-of-the-art mobile app allows you to access and manage your projects on the go. Schedule appointments, capture on-site e-signatures, and document everything from project scope info to notes and dry log readings. Keeping track of your business’s info has never been easier.',
        'items' => [
          'Use any mobile device in the field',
          'Enjoy 24/7 offline access to all your project information',
          'View in-progress, completed and deleted projects',
          'Create new projects',
          'Use call report forms to record all project information',
          'Record project scope information for each affected area, dry log readings on moisture maps and psychometric reports',
          'Add notes to daily logs and forms',
          'Quickly email all forms and photo/video links to anyone you want',
          'Save forms, photos and videos to the cloud',
          'Track equipment',
          'Capture electronic signatures',
        ]
      ], [
        'icon' => 'fa-file-image-o',
        'slug' => str_slug(MKTG_FEATURE_8, '-'),
        'imgSrc' => '/assets/images/sharing.png',
        'imgAlt' => MKTG_SITE_NAME . ' Feature: ' . MKTG_FEATURE_8,
        'caption' => MKTG_FEATURE_8,
        'intro' => 'Our system lets team members seamlessly share project forms, photos, videos, scope information, notes, and moisture and psychometric readings, all in real time. Get everyone on the same page in just three easy steps:',
        'outro' => 'For each project, you’ll be able to upload an unlimited number of forms, photos and videos—and you’ll be able to access it all in one place.',
        'items' => [
          'Add Dropbox cloud storage link to your settings (this automatically creates a folder when you add a new project)',
          'Save forms, photos, and videos to the folder',
          'Email link to customers and insurance company adjusters'
        ]
      ], [
        'icon' => 'fa-envelope-o',
        'slug' => str_slug(MKTG_FEATURE_9, '-'),
        'imgSrc' => '/assets/images/review-request.png',
        'imgAlt' => MKTG_SITE_NAME . ' Feature: ' . MKTG_FEATURE_9,
        'caption' => MKTG_FEATURE_9,
        'intro' => 'Reviews not only demonstrate how your company will perform to potential customers, they can dramatically improve your search engine performance. This is essential when attempting to attract more customers and increase conversion. Reviews are also a powerful way to expand your social and digital footprint.',
        'middle-content' => 'Research has found that a great number of reviews come from review request emails, which is why we’ve built an automatic review request feature into our software itself. Dry Forms Plus provides a standard review request email that you can edit or delete and replace with your own. It also lets you upload your favorite review links for your company. When a project is marked as complete, the system will instantly ask you whether you want to send a review request email.',
        'outro' => 'It couldn’t be easier.'
      ],
    ];
  }
}