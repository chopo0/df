<?php

namespace App\Providers\Marketing;

class FAQProvider {
  public static function list()
  {
    return [
      [
        'question' => 'How many users can I have with my account?',
        'answer' => 'You can add unlimited users. You can invite new users in the settings tab.'
      ], [
        'question' => 'Is there a maximum number of projects I can add per month?',
        'answer' => 'You can add unlimited projects per month.'
      ], [
        'question' => 'How many projects will the system store and how long will they be kept?',
        'answer' => 'Your company’s projects will be kept in the program until you cancel your subscription. It is always recommended that you print and save all completed projects for your records.'
      ], [
        'question' => 'Can I make changes to the Affected Areas and all Moisture Map dropdown lists?',
        'answer' => 'Yes, in the Standards Tab you can make changes to the Affected Areas and all Moisture Map dropdown lists.'
      ], [
        'question' => 'What are the Standard Forms?',
        'answer' => 'The system creates all your forms for any new project added from your Standard Forms. You can edit your Standard Project Scope Form and all your Contract Forms or use the ones we provide. Any changes made will not change any past projects, just any new projects added. (Note that any legal documents you use should always be reviewed by an attorney.)'
      ], [
        'question' => 'Can I use my own statements on the contract forms?',
        'answer' => 'Yes, standard statements are provided for each provided contract form which can be used, edited, or deleted and replaced with one of your own. (Note that any legal documents you use should always be reviewed by an attorney.)'
      ], [
        'question' => 'What if there is a contract form that is not provided that my company requires?',
        'answer' => 'In the Standards Tab, there is a Form Builder feature that allows you to build unlimited contract forms.'
      ], [
        'question' => 'Can I customize the standard Project Scope Form?',
        'answer' => 'Yes, you can use, edit, or delete and replace any line items with your own. You can also drag and drop line items to different positions on the form.'
      ], [
        'question' => 'After making changes, is there a way to revert back to the provided Standard Forms? ',
        'answer' => 'On any of your standard Forms, just click on the revert button and it will revert back to the provided statement. It will not change any past project forms, just any new projects added.'
      ], [
        'question' => 'Does the system have the capability to obtain electronic signatures?',
        'answer' => 'Yes, electronic signatures are essential so there is no need to carry paper forms to have signed at the job. '
      ], [
        'question' => 'Is there a way to mark after hours prices for line items on the Project Scope Form?',
        'answer' => 'You can mark any line item on the project scope as an after-hours price. When selecting a line item once, it marks it as a regular hours(RH) item. If you select it again, it marks it as an after-hours(AH) item. When you select it again, it will remove the line item as a selected item.'
      ], 
      [
        'question' => 'What is the Daily Log?',
        'answer' => 'The Daily Log is a form that you can enter notes of daily events that happen for that project. If elected to, in the Standards Tab, notes from all forms will transfer to this form as well. It is recommended to do this before adding any new projects. It will until you elect not to copy notes from all forms.'
      ], [
        'question' => 'Does DryFormsPlus have Dry Logs?',
        'answer' => 'Provided are two dry logs, Moisture Maps and Psychometric reports.'
      ], [
        'question' => 'Can I add containment zones to the Psychometric Reports?',
        'answer' => 'You can add unlimited containment zones to each affected area on the Psychometric Report.'
      ], [
        'question' => 'Can I save my projects?',
        'answer' => 'Yes, you can save all projects to your computer. If linked to your DropBox account, you have the ability to save your projects to the cloud. It creates a folder for each project, labeled by owner/insured name and job address.'
      ], [
        'question' => 'What is the Photos/Videos feature?',
        'answer' => 'The system allows you to take or add photos and videos for each project. If linked to your DropBox account, a photos/videos folder will automatically be added to that projects folder in the cloud. To view all photos and videos for a project, just click on the Photos/Videos button.'
      ], [
        'question' => 'What if I do not use DropBox and want to save my projects, photos and videos?',
        'answer' => 'You can save your projects to your computer. It will create a folder and save it to your desktop or downloads folder. In that folder you can add all your photos and videos for the project.'
      ], [
        'question' => 'Can I email only one form?',
        'answer' => 'Yes, you can email an individual form or all forms from a project.'
      ]
    ];
  }
}