<?php
namespace App\Notifications\Users;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class Invite extends Notification
{
    use Queueable;

    /**
     * @var User
     */
    private $user;

    /**
     * TicketCreatedForUser constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * @param $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url('/#/set-password?token=' . $this->user->password_set_token);

        return (new MailMessage)
            ->subject("Invitation from DryForms+")
            ->greeting("Dear, {$this->user->full_name}")
            ->line("We invite you to join our company.")
            ->action("Set Your Access", $url);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
