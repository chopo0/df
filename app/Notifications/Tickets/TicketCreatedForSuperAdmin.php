<?php

namespace App\Notifications\Tickets;

use App\Models\Ticket;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class TicketCreatedForSuperAdmin extends Notification
{
    use Queueable;

    /**
     * @var Ticket
     */
    private $ticket;

    /**
     * TicketCreatedForUser constructor.
     * @param Ticket $ticket
     */
    public function __construct(Ticket $ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * @param $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url('/admin/ticket/' . $this->ticket->ticket_id);

        return (new MailMessage)
            ->subject("[Ticket ID: {$this->ticket->ticket_id}] {$this->ticket->title}")
            ->greeting('Hello!')
            ->line('Support ticket was created:')
            ->line("Title: {$this->ticket->title}")
            ->line("Priority: {$this->ticket->priority}")
            ->line("Status: {$this->ticket->status}")
            ->action('Check', $url);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
