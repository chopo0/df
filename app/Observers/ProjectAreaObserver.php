<?php

namespace App\Observers;

use App\Models\ProjectArea;
use App\Models\ProjectMoistureDay;
use App\Models\ProjectMoistureDayData;
use App\Services\Projects\ProjectAreasService;
use App\Services\Scopes\ScopesService;

class ProjectAreaObserver
{
    /**
     * @var ProjectAreasService
     */
    private $projectAreasService;

    /**
     * @var ScopesService
     */
    private $scopesService;

    /**
     * ProjectAreaObserver constructor.
     * @param ProjectAreasService $projectAreasService
     * @param ScopesService $scopesService
     */
    public function __construct(ProjectAreasService $projectAreasService, ScopesService $scopesService)
    {
        $this->projectAreasService = $projectAreasService;
        $this->scopesService = $scopesService;
    }

    /**
     * @param ProjectArea $area
     */
    public function created(ProjectArea $area)
    {
        $projectMoistureDay = null;
        $this->projectAreasService->addMeasurementsForNewArea($area);
        $this->scopesService->setProjectScopes($area->project);
    }

    /**
     * @param ProjectArea $area
     */
    public function deleted(ProjectArea $area)
    {
        $this->scopesService->deleteProjectScopesForArea($area);
    }
}
