<?php

namespace App\Observers;

use App\Models\Project;
use App\Models\ProjectInstrument;
use App\Models\ProjectMoistureForm;
use App\Services\Scopes\ScopesService;

class ProjectObserver
{
    /**
     * @var ProjectMoistureForm
     */
    private $projectMoistureForm;

    /**
     * @var ProjectInstrument
     */
    private $projectInstrument;

    /**
     * @var ScopesService
     */
    private $scopesService;

    /**
     * ProjectObserver constructor.
     * @param ProjectMoistureForm $projectMoistureForm
     * @param ProjectInstrument $projectInstrument
     */
    public function __construct(ProjectMoistureForm $projectMoistureForm, ProjectInstrument $projectInstrument, ScopesService $scopesService)
    {
        $this->projectMoistureForm = $projectMoistureForm;
        $this->projectInstrument = $projectInstrument;
        $this->scopesService = $scopesService;
    }

    /**
     * @param Project $project
     */
    public function created(Project $project)
    {
        $this->scopesService->createProjectMiscPage($project->id);

        $instrument = $this->projectInstrument->where('company_id', $project->company_id)->orderBy('id', 'desc')->first();
        if ($instrument) {
            $this->projectInstrument->create([
                'make' => $instrument->make,
                'model' => $instrument->model,
                'project_id' => $project->id,
                'company_id' => $project->company_id
            ]);
        }
    }
}
