<?php

namespace App\Observers;

use App\Models\Company;
use App\Services\Forms\StructuresAndMaterialsService;
use App\Services\Projects\ProjectAreasService;
use App\Services\Projects\ProjectFormService;
use App\Services\Projects\StandardStatementsService;
use App\Services\Scopes\ScopesService;
use App\Services\Teams\TeamsService;

class CompanyObserver
{
    const TEMPLATE_COMPANY_ID = -1;

    /**
     * @var ProjectFormService
     */
    private $projectFormService;

    /**
     * @var StandardStatementsService
     */
    private $standardStatementsService;

    /**
     * @var TeamsService
     */
    private $teamsService;

    /**
     * @var ProjectAreasService
     */
    private $projectAreasService;

    /**
     * @var StructuresAndMaterialsService
     */
    private $structuresAndMaterialsService;

    /**
     * @var ScopesService
     */
    private $scopesService;

    /**
     * CompanyObserver constructor.
     * @param ProjectFormService $projectFormService
     * @param StandardStatementsService $standardStatementsService
     * @param TeamsService $teamsService
     * @param ProjectAreasService $projectAreasService
     * @param StructuresAndMaterialsService $structuresAndMaterialsService
     * @param ScopesService $scopesService
     */
    public function __construct(
        ProjectFormService $projectFormService,
        StandardStatementsService $standardStatementsService,
        TeamsService $teamsService,
        ProjectAreasService $projectAreasService,
        StructuresAndMaterialsService $structuresAndMaterialsService,
        ScopesService $scopesService
    ) {
        $this->projectFormService = $projectFormService;
        $this->standardStatementsService = $standardStatementsService;
        $this->teamsService = $teamsService;
        $this->projectAreasService = $projectAreasService;
        $this->structuresAndMaterialsService = $structuresAndMaterialsService;
        $this->scopesService = $scopesService;
    }

    /**
     * @param Company $company
     */
    public function created(Company $company)
    {
        $this->projectFormService->createStandardForms($company);
        $this->standardStatementsService->createDefaultStatements($company);
        $this->teamsService->setDefaultTeams($company->id);
        $this->projectAreasService->setDefaultAreas($company->id);
        $this->structuresAndMaterialsService->setDefaultMaterials($company->id);
        $this->structuresAndMaterialsService->setDefaultStructures($company->id);
        $this->scopesService->setStandardScopes($company->id);
    }
}
