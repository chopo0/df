<?php

namespace App\Traits;

use Carbon\Carbon;

trait Dates
{
    /**
     * @return string
     */
    public function getUpdatedAtAttribute()
    {
        $date = $this->attributes['updated_at'];
        $timezone = auth()->user()->timezone;

        return $date ? (new Carbon($date))->timezone($timezone)->toDateTimeString() : null;
    }
}
