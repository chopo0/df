<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Marketing\ContactPerson;

class NewContactPersonNotification extends Mailable
{
  use Queueable, SerializesModels;
  public $contactPerson;

  public function __construct(ContactPerson $contactPerson)
  {
    $this->contactPerson = $contactPerson;
  }

  public function build()
  {
    return $this->view('marketing.emails.contact-us-notification');
  }
}
