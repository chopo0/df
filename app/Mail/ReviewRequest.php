<?php

namespace App\Mail;

use App\Http\Controllers\Api\ProjectFilesController;
use App\Models\Project;
use App\Services\Files\DropboxService;
use App\Services\Forms\FormsPdfService;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Spatie\Dropbox\Exceptions\BadRequest as DropboxBadRequest;

class ReviewRequest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Project
     */
    private $project;

    /**
     * @var array
     */
    private $recipient;

    /**
     * @var Collection
     */
    private $links;

    /**
     * ReviewRequest constructor.
     * @param Project $project
     * @param string $recipient
     * @param Collection $links
     */
    public function __construct(Project $project, string $recipient, Collection $links)
    {
        $this->project = $project;
        $this->recipient = $recipient;
        $this->links = $links;
    }

    /**
     * @return ReviewRequest
     */
    public function build()
    {
        $email = $this->markdown('emails.requester')
            ->from($this->project->company->email, $this->project->company->name)
            ->subject('Review Request from '. $this->project->company->name)
            ->with([
                'links' => $this->links,
                'company' => $this->project->company
            ]);

        return $email->to($this->recipient);
    }
}
