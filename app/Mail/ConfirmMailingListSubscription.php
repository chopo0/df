<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmMailingListSubscription extends Mailable
{
    use Queueable, SerializesModels;
    public $mailingListItem;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailingListItem)
    {
        $this->mailingListItem = $mailingListItem;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('marketing.emails.mailing-list-subscribe');
    }
}

