<?php

namespace App\Mail;

use App\Http\Controllers\Api\ProjectFilesController;
use App\Models\Project;
use App\Services\Files\DropboxService;
use App\Services\Forms\FormsPdfService;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Spatie\Dropbox\Exceptions\BadRequest as DropboxBadRequest;

class FormsEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Project
     */
    private $project;

    /**
     * @var array
     */
    private $files;

    /**
     * @var array
     */
    private $recipients;

    /**
     * @var bool
     */
    private $attachDropboxLink;

    /**
     * FormsEmail constructor.
     * @param Project $project
     * @param array $files
     * @param array $recipients
     * @param bool $attachDropboxLink
     */
    public function __construct(Project $project, array $files, array $recipients, bool $attachDropboxLink)
    {
        $this->project = $project;
        $this->files = $files;
        $this->recipients = $recipients;
        $this->attachDropboxLink = $attachDropboxLink;
    }

    /**
     * @param DropboxService $dropboxService
     * @param FormsPdfService $formsPdfService
     * @return FormsEmail
     */
    public function build(DropboxService $dropboxService, FormsPdfService $formsPdfService)
    {
        $folderName = $formsPdfService->getCompleteDocumentTitle($this->project->call_report) . '/' . ProjectFilesController::FILES_FOLDER;
        $url = null;
        try {
            if ($this->attachDropboxLink) {
                $url = $dropboxService->getFolderSharedLink($this->project, $folderName);
            }
        } catch(DropboxBadRequest $e) {
            logger()->error($e);
            $this->attachDropboxLink = false;
        }

        $email = $this->markdown('emails.forms')->subject('Project Forms')->with([
            'attachDropboxLink' => $this->attachDropboxLink,
            'url' => $url,
            'callReport' => $this->project->call_report,
            'company' => $this->project->company
        ]);

        foreach ($this->files as $file) {
            if(!$file) continue;
            $email->attach($file);
        }

        return $email->to($this->recipients);
    }
}
