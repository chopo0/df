<?php
namespace App\Http\Requests;

class SetPasswordRequest extends BaseRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'password' => ['required', 'confirmed', 'min:8'],
            'token' => ['required', 'exists:users,password_set_token']
        ];
    }
}
