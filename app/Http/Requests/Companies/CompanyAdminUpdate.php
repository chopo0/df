<?php
namespace App\Http\Requests\Companies;

use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class CompanyAdminUpdate extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => ['exists:companies,id'],
            'name' => ['required', 'string', 'unique:companies,name,'. $this->get('company_id')],
            'logo' => ['nullable', 'file']
        ];
    }

    /**
     * @return array
     */
    public function validationData()
    {
        $this->merge(
            [
                'company_id' => $this->segment(3)
            ]
        );

        return parent::validationData();
    }
}
