<?php
namespace App\Http\Requests\Files;

use App\Http\Requests\BaseRequest;

class UpdateFileName extends BaseRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'filename' => ['required'],
            'newName' => ['required'],
            'extension' => ['required'],
            'basename' => ['required'],
        ];
    }
}
