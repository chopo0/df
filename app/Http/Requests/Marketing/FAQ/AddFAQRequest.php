<?php

namespace App\Http\Requests\Marketing\FAQ;

use Illuminate\Foundation\Http\FormRequest;

class AddFAQRequest extends FormRequest {

  /**
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * @return array
   */
  public function rules() {
    return [
      'question' => 'required|max:1000',
      'answer'=> 'required|max:1000'
    ];
  }
}