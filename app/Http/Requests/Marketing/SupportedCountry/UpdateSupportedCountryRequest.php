<?php

namespace App\Http\Requests\Marketing\SupportedCountry;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSupportedCountryRequest extends FormRequest
{

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100'
        ];
    }
}