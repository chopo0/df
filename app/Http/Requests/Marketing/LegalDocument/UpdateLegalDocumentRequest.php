<?php

namespace App\Http\Requests\Marketing\LegalDocument;

use Illuminate\Foundation\Http\FormRequest;

class UpdateLegalDocumentRequest extends FormRequest
{

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        $privacyPolicy = PRIVACY_POLICY_ABBREV;
        $termsAndConditions = TERMS_AND_CONDITIONS_ABBREV;

        return [
            'type' => "required|in:{$privacyPolicy},{$termsAndConditions}",
            'content' => 'required',
        ];
    }
}