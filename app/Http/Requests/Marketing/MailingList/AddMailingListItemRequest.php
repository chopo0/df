<?php

namespace App\Http\Requests\Marketing\MailingList;

use Illuminate\Foundation\Http\FormRequest;

class AddMailingListItemRequest extends FormRequest
{

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'g-recaptcha-response' => 'recaptcha|required'
        ];
    }
}