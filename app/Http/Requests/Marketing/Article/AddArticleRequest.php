<?php

namespace App\Http\Requests\Marketing\Article;

use Illuminate\Foundation\Http\FormRequest;

class AddArticleRequest extends FormRequest
{

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:180',
            'sub_title' => 'required|max:180',
            'cover' => 'required|max:1000',
            'article' => 'required|max:60000',
        ];
    }
}