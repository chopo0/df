<?php

namespace App\Http\Requests\Marketing\MarketingSetting;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMarketingSettingRequest extends FormRequest {
  public function authorize() {
    return true;
  }

  public function rules() {
    return [
      'notification_receiver' => 'required|email',
      'package_price' => 'required',
      'intro_video' => 'required'
    ];
  }
}