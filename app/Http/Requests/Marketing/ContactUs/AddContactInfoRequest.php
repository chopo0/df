<?php

namespace App\Http\Requests\Marketing\ContactUs;

use Illuminate\Foundation\Http\FormRequest;

class AddContactInfoRequest extends FormRequest {

  /**
   * @return bool
   */
  public function authorize() {
    return true;
  }

  /**
   * @return array
   */
  public function rules() {
    return [
      'full_name' => 'required',
      'email' => 'required|email',
      'contact_number' => 'required',
      'message' => 'required|max:1000',
      'g-recaptcha-response'=>'recaptcha|required'
    ];
  }
}