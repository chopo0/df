<?php

namespace App\Http\Requests\ProjectScopes;

use App\Http\Requests\BaseRequest;

class ProjectScopesUpdate extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_id' => ['required', 'exists:projects,id'],
            'project_area_id' => ['nullable', 'exists:project_areas,id'],
            'selected' => ['nullable', 'in:RH,AH,X'],
            'service' => ['nullable', 'string', 'max:37'],
            'header' => ['sometimes', 'required', 'boolean'],
            'quantity' => ['nullable', 'string'],
            'uom' => ['nullable', 'exists:units_of_measure,id'],
            'page' => ['sometimes', 'required', 'numeric']
        ];
    }
}
