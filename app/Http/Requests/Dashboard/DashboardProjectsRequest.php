<?php
namespace App\Http\Requests\Dashboard;

use App\Http\Requests\BaseRequest;

class DashboardProjectsRequest extends BaseRequest
{
    /**
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function authorize()
    {
        return auth()->user();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'year' => ['required', 'string'],
            'team' => ['required', 'exists:teams,id']
        ];
    }
}
