<?php

namespace App\Http\Controllers\Backend;

use App\Notifications\Tickets\TicketCommentForUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Ticket;
use App\Models\TicketComment;

class TicketCommentsController extends Controller
{
    /**
     * @var Ticket
     */
    private $ticket;

    /**
     * @var TicketComment
     */
    private $ticketComment;

    /**
     * Ticket constructor.
     *
     * @param Ticket $ticket
     * @param TicketComment $ticketComment
     */
    public function __construct(Ticket $ticket, TicketComment $ticketComment)
    {
        $this->ticket = $ticket;
        $this->ticketComment = $ticketComment;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'comment' => 'required'
        ]);

        $ticketComment = $this->ticketComment->create([
            'ticket_id' => $request->input('ticket_id'),
            'user_id' => auth()->user()->id,
            'comment' => $request->input('comment')
        ]);

        $ticketComment->ticket->user->notify(new TicketCommentForUser($ticketComment->ticket));

        return redirect()->back()->with("status", "Your comment has be submitted.");
    }
}
