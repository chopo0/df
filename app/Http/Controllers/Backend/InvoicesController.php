<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\User;
use Stripe\Invoice;
use Stripe\Stripe;

class InvoicesController extends Controller
{
    /**
     * @var Company
     */
    private $company;

    /**
     * @var Stripe
     */
    private $stripe;

    /**
     * @var Invoice
     */
    private $invoice;

    /**
     * InvoicesController constructor.
     * @param Company $company
     * @param Stripe $stripe
     * @param Invoice $invoice
     */
    public function __construct(Company $company, Stripe $stripe, Invoice $invoice)
    {
        $this->company = $company;
        $this->stripe = $stripe;
        $this->invoice = $invoice;

        $this->stripe->setApiKey(env('STRIPE_SECRET'));
    }

    /**
     * @param int $companyId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(int $companyId)
    {
        $company = $this->company->findOrFail($companyId);
        /** @var User $owner */
        $owner = $company->user;
        $invoices = $owner->customer->invoices(['limit' => 100]);

        return view('dashboard.invoices.index', compact('invoices', 'companyId'));
    }

    /**
     * @param int $companyId
     * @param string $invoiceId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show(int $companyId, string $invoiceId)
    {
        $invoice = $this->invoice->retrieve($invoiceId);

        return redirect($invoice->hosted_invoice_url);
    }

    /**
     * @param int $companyId
     * @param string $invoiceId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function download(int $companyId, string $invoiceId)
    {
        $invoice = $this->invoice->retrieve($invoiceId);

        return redirect($invoice->invoice_pdf);
    }
}
