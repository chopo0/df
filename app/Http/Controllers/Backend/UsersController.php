<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\Admin\UserUpdate;
use App\Models\Role;
use App\Models\User;
use App\Providers\StatesProvider;
use App\Services\Subscriptions\SubscriptionService;
use Illuminate\Http\Request;
use Prologue\Alerts\Facades\Alert;

class UsersController extends Controller
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var Role
     */
    private $role;

    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * UsersController constructor.
     * @param User $user
     * @param Role $role
     * @param SubscriptionService $subscriptionService
     */
    public function __construct(User $user, Role $role, SubscriptionService $subscriptionService)
    {
        $this->user = $user;
        $this->role = $role;
        $this->subscriptionService = $subscriptionService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = $this->user->orderBy('role_id')->paginate(20);

        return view('dashboard.users.index', compact('users'));
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $user = $this->user->findOrFail($id);
        $companyId = $user->company_id;
        $roles = $this->role->get();
        $states = StatesProvider::STATES;

        return view('dashboard.users.form', compact('user', 'roles', 'states', 'companyId'));
    }

    /**
     * @param UserUpdate $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserUpdate $request)
    {
        $user = $this->user->find($request->get('id'));

        $user->email = $request->get('email');
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->role_id = $request->get('role_id') ? $request->get('role_id') : $user->role_id;

        if ($request->get('new_password')) {
            $user->new_password = $request->get('new_password');
        }

        $user->save();

        Alert::success('User successfully updated')->flash();

        return redirect()->route('users.edit', $user->id)->with('alerts', Alert::all());
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function suspend(int $id)
    {
        $user = $this->user->findOrFail($id);
        $user->status = 'suspended';
        $user->save();

        Alert::success('User successfully updated')->flash();

        return redirect()->back()->with('alerts', Alert::all());
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function resume(int $id)
    {
        $user = $this->user->findOrFail($id);
        $user->status = 'active';
        $user->save();

        Alert::success('User successfully updated')->flash();

        return redirect()->back()->with('alerts', Alert::all());
    }
}
