<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Companies\CompanyAdminUpdate;
use App\Models\Company;
use App\Models\User;
use App\Services\Subscriptions\SubscriptionService;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Prologue\Alerts\Facades\Alert;
use Stripe\Stripe;
use Intervention\Image\ImageManager as Image;

class CompaniesController extends Controller
{
    /**
     * @var Stripe
     */
    private $stripe;

    /**
     * @var Company
     */
    private $company;

    /**
     * @var Image
     */
    private $image;

    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * @var User
     */
    private $user;

    /**
     * CompaniesController constructor.
     * @param Stripe $stripe
     * @param Company $company
     * @param Image $image
     * @param SubscriptionService $subscriptionService
     * @param User $user
     */
    public function __construct(
        Stripe $stripe,
        Company $company,
        Image $image,
        SubscriptionService $subscriptionService,
        User $user
    ) {
        $this->stripe = $stripe;
        $this->company = $company;
        $this->image = $image;
        $this->subscriptionService = $subscriptionService;
        $this->user = $user;

        $this->stripe->setApiKey(env('STRIPE_SECRET'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $companies = $this->company->newQuery();

        if (!empty($request->get('name'))) {
            $companies = $companies->where('name', 'like', "%{$request->get('name')}%");
        }

        if (!empty($request->get('coupon'))) {
            $companies = $companies->whereHas('user', function ($query) use ($request) {
                $query->where('coupon', 'like', "%{$request->get('coupon')}%");
            });
        }

        if (!empty($request->get('email'))) {
            $companies = $companies->where('email', 'like', "%{$request->get('email')}%");
        }

        if (!empty($request->get('status')) && $request->get('status') !== 'all') {
            $companies = $companies->with('user')->orderBy('name', $request->get('sort') ?? 'asc')->get();
            $bufferedCompanies = [];
            /** @var Company $company */
            foreach ($companies as $company) {
                if ($company->user && $company->user->subscription && $company->user->subscription && ($company->user->subscription->status === $request->get('status'))) {
                    $bufferedCompanies[] = $company;
                }
            }

            $companies = new LengthAwarePaginator(collect($bufferedCompanies), count($bufferedCompanies), 20, $request->get('page'));
        } else {
            $companies = $companies->with('user')->orderBy('name', $request->get('sort') ?? 'asc')->paginate(20);
        }

        return view('dashboard.companies.index', compact('companies'));
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function users(int $id)
    {
        $company = $this->company->findOrFail($id);
        $users = $company->users()->paginate(20);

        return view('dashboard.users.index', compact('users'));
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $company = $this->company->findOrFail($id);

        return view('dashboard.companies.form', compact('company'));
    }

    /**
     * @param int $id
     * @param CompanyAdminUpdate $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, CompanyAdminUpdate $request)
    {
        $company = $this->company->findOrFail($id);
        $company->name = $request->get('name');

        if ($request->file('logo')) {
            $logoFile = $request->file('logo');
            $filename = str_random() . '.' . $logoFile->getClientOriginalExtension();

            $img = $this->image->make($request->file('logo'));
            $img->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->save("storage/logos/$filename", 80);
            $company->logo = "logos/$filename";
        }

        $company->save();

        Alert::success('Company successfully updated')->flash();

        return redirect()->back()->with('alerts', Alert::all());
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editSubscription(int $id)
    {
        $company = $this->company->findOrFail($id);

        return view('dashboard.companies.subscription', compact('company'));
    }

    /**
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateSubscription(int $id, Request $request)
    {
        $company = $this->company->findOrFail($id);
        $user = $this->user->findOrFail($company->user_id);
        $action = $request->get('action');
        $days = $request->get('days');

        switch($action) {
            case 'terminate_now':
                $this->subscriptionService->cancelSubscription($user);
                $this->user->where('company_id', $company->id)->update(['status' => 'suspended']);
                break;
            case 'terminate_at_the_end':
                $this->subscriptionService->setCancelAtPeriodEnd($user);
                $this->subscriptionService->cancelSubscription($user);
                break;
            case 'extend':
                break;
        }

        Alert::success('Subscription successfully updated')->flash();

        return redirect()->route('companies.index')->with('alerts', Alert::all());
    }
}
