<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Marketing\MailingListItem;
use App\Models\Project;
use App\Models\User;

class DashboardController extends Controller
{
    /**
     * @var Company
     */
    private $company;

    /**
     * @var Project
     */
    private $project;

    /**
     * @var User
     */
    private $user;

    /**
     * @var MailingListItem
     */
    private $mailingListItem;

    /**
     * DashboardController constructor.
     * @param Company $company
     * @param Project $project
     * @param User $user
     * @param MailingListItem $mailingListItem
     */
    public function __construct(Company $company, Project $project, User $user, MailingListItem $mailingListItem)
    {
        $this->company = $company;
        $this->project = $project;
        $this->user = $user;
        $this->mailingListItem = $mailingListItem;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = $this->user->count();
        $companies = $this->company->count();
        $projects = $this->project->count();
        $subscriptions = $this->mailingListItem->count();

        return view('vendor.backpack.base.dashboard', compact('users', 'companies', 'projects', 'subscriptions'));
    }
}
