<?php

namespace App\Http\Controllers\Backend;

use App\Notifications\Tickets\TicketClosedForUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Ticket;
use App\Models\TicketCategory;

class TicketsController extends Controller
{
    /**
     * @var Ticket
     */
    private $ticket;

    /**
     * @var TicketCategory
     */
    private $ticketCategory;

    /**
     * Ticket constructor.
     *
     * @param Ticket $ticket
     * @param TicketCategory $ticketCategory
     */
    public function __construct(Ticket $ticket, TicketCategory $ticketCategory)
    {
        $this->ticket = $ticket;
        $this->ticketCategory = $ticketCategory;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $ticketCategories = $this->ticketCategory->get();
        $tickets = $this->ticket
            ->with(['ticketCategory', 'user', 'comments'])
            ->paginate(10);

        return view('dashboard.tickets.index', compact('tickets', 'ticketCategories'));
    }

    /**
     * @param string $ticket_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(string $ticket_id)
    {
        $ticket = $this->ticket
            ->where('ticket_id', $ticket_id)->firstOrFail();

        $comments = $ticket->comments;
        $category = $ticket->ticketCategory;

        return view('dashboard.tickets.show', compact('ticket', 'category', 'comments'));
    }

    /**
     * @param $ticket_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function close($ticket_id)
    {
        $ticket = $this->ticket
            ->where('ticket_id', $ticket_id)->firstOrFail();

        $ticket->status = 'Closed';
        $ticket->save();

        $ticket->user->notify(new TicketClosedForUser($ticket));

        return redirect()->back()->with("status", "The ticket has been closed.");
    }
}
