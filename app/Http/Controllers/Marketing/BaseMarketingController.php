<?php

namespace App\Http\Controllers\Marketing;

use App\Http\Controllers\Controller;
use App\Models\Marketing\SupportedCountry;
use App\Services\Marketing\BlogService;
use Illuminate\Foundation\Application;
use App\Services\Marketing\BannerService;
use App\Services\Marketing\SEOService;

class BaseMarketingController extends Controller
{

    /**
     * @var BlogService
     */
    protected $blogService;

    /**
     * @var SupportedCountry
     */
    protected $supportedCountry;

    /**
     * @var BannerService
     */
    protected $bannerService;

    /**
     * @var SEOService
     */
    protected $seoService;

    /**
     * HomeController constructor
     * 
     * @param SupportedCountry $supportedCountry
     * @param BlogService $blogService
     * @param SEOService $seoService
     * @param BannerService $bannerService
     */
    public function __construct(
        SupportedCountry $supportedCountry = null,
        BlogService $blogService = null,
        BannerService $bannerService = null,
        SEOService $seoService = null
    ) {
        static $params;

        // Get parameters
        if ($params === null) {
            $params = $this->getparams();
        }

        foreach ($params as $param) {
            // Process only omitted optional parameters
            if (${$param->name} === null) {
                // Assign variable
                ${$param->name} = app()->make($param->getClass()->name);
            }
        }

        $this->blogService = $blogService;
        $this->supportedCountry = $supportedCountry;
        $this->bannerService = $bannerService;
        $this->seoService = $seoService;
    }

    /**
     * @return array
     */
    private function getParams()
    {
        $reflector = new \ReflectionClass(__class__);
        $constructor = $reflector->getConstructor();
        return $constructor->getParameters();
    }
}
