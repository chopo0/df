<?php

namespace App\Http\Controllers\Marketing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Marketing\ContactPerson;
use App\Mail\NewContactPersonNotification;
use App\Services\Marketing\MarketingSettingService;
use Illuminate\Mail\Mailer as Mail;
use App\Http\Requests\Marketing\ContactUs\AddContactInfoRequest;

class ContactUsController extends BaseMarketingController
{

    /**
     * @var ContactPerson
     */
    private $contactPerson;

    /**
     * @var MarketingSettingService
     */
    private $marketingSettingService;

    /**
     * @var Mail
     */
    private $mail;


    /**
     * ContactUsController constructor
     * 
     * @param ContactPerson $contactPerson
     * @param Mail $mail
     * @param MarketingSettingService $marketingSettingService
     */
    public function __construct(
        ContactPerson $contactPerson,
        Mail $mail,
        MarketingSettingService $marketingSettingService
    ) {
        parent::__construct();
        $this->contactPerson = $contactPerson;
        $this->marketingSettingService = $marketingSettingService;
        $this->mail = $mail;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $banner = $this->bannerService->setDetails('Contact Us', 'We would love to answer your questions.', null);
        $recentArticles = $this->blogService->getRecentlyPublished();
        $this->seoService->generateRegularMetaData('Contact Us');
        return view('marketing.contact-us', compact('banner', 'recentArticles'));
    }

    /**
     * @param AddContactInfoRequest $request
     * 
     * @return \Illuminate\View\View
     */
    public function store(AddContactInfoRequest $request)
    {
        $contactPerson = $this->contactPerson->create([
            'contact_number' => $request->contact_number,
            'full_name' => $request->full_name,
            'email' => $request->email,
            'message' => $request->message,
            'company_name' => $request->company_name,
        ]);

        $this->emailAdmin($contactPerson);

        session()->flash('message', 'Thank you for reaching us! We will get back to you as soon as we can!');

        return redirect()->to('/contact-us');
    }

    /**
     * @param ContactPerson $contactPerson
     * 
     * @return void
     */
    public function emailAdmin($contactPerson)
    {
        // Just in case the email failed to send.
        try {
            $setting = $this->marketingSettingService->getSetting();
            $this->mail->to($setting->notification_receiver)->send(new NewContactPersonNotification($contactPerson));
        } catch (\Exception $e) {
        }
    }
}
