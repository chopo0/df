<?php

namespace App\Http\Controllers\Marketing\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Marketing\ContactPerson;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Redirect;

class ContactPersonController extends Controller {
  
  /**
   * @var ContactPerson
   */
  private $contactPerson;

  /**
   * ContactPersonController constructor.
   * 
   * @param ContactPerson $contactPerson
   */
  public function __construct(ContactPerson $contactPerson) {
    $this->contactPerson = $contactPerson;
  }

  /**
   * @param Request $request
   * 
   * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
   */
  public function index(Request $request) {
    $messageId = $request->input('message-id');
    
    $instance = null;
    if($messageId) {
      $instance = $this->markAsRead($messageId);
    }

    $filter = $request->input('filter');
    $contactPeople = $this->getList($filter, $messageId);

    $page = $request->input('page');
    if(!count($contactPeople) && $page != 1) {
      $lastPage = $contactPeople->lastPage();

      return redirect()->to($request->fullUrlWithQuery(['page' => $lastPage]));
    }
    
    return view('marketing.admin.contact-people', compact('instance', 'contactPeople'));
  }

  /**
   * @param string $filter
   * @param int $messageId
   * 
   * @return Collection
   */
  public function getList($filter, $messageId) {
    $query = $this->contactPerson->orderBy('created_at', 'desc');
    $query = $this->filter($query, $filter);
    
    if($messageId) {
      $query = $query->orWhere('id', $messageId);
    }

    return $query->paginate(10);
  }

  /**
   * @param Builder $query
   * @param string $filter
   * 
   * @return Builder
   */
  public function filter($query, $filter) {
    switch ($filter) {
      case MESSAGE_UNREAD:
        return $query->where('is_read', 0);
      case MESSAGE_READ:
        return $query->where('is_read', 1);
      default:
        return $query->whereIn('is_read', [0, 1]);
    }
  }

  /**
   * @param int $id
   * 
   * @return ContactPerson
   */
  public function markAsRead($id) {
    $instance = $this->contactPerson->find($id);
    
    if(!empty($instance)) {
      $instance->is_read = true;
      $instance->save();
    }

    return $instance;
  }

  /**
   * @param int $id
   * 
   * @return \Illuminate\Http\RedirectResponse
   */
  public function destroy($id) {
    $this->contactPerson->find($id)->delete();

    return redirect()->to('cms/contact-people')
      ->with(['message' => 'Contact information successfully deleted!']);
  }
}
