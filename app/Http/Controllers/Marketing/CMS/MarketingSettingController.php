<?php

namespace App\Http\Controllers\Marketing\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Collective\Html\FormFacade as Form;
use App\Services\Marketing\MarketingSettingService;
use App\Http\Requests\Marketing\MarketingSetting\UpdateMarketingSettingRequest;
use App\Models\Marketing\MarketingSetting;

class MarketingSettingController extends Controller {

  /**
   * @var MarketingSetting
   */
  private $marketingSetting;

  /**
   * @var MarketingSettingService
   */
  private $marketingSettingService;

  /**
   * MarketingSettingController constructor
   * 
   * @param MarketingSetting $marketingSetting
   * @param MarketingSettingService $marketingSettingService
   */
  public function __construct(
    MarketingSetting $marketingSetting,
    MarketingSettingService $marketingSettingService) {
      $this->marketingSetting = $marketingSetting;
      $this->marketingSettingService = $marketingSettingService;
  }

  /**
   * @return \Illuminate\View\View
   */
  public function index() {
    $setting = $this->marketingSettingService->getSetting();
    
    return view('marketing.admin.other', compact('setting'));
  }

  /**
   * @return \Illuminate\View\View
   */
  public function edit() {
    $setting = $this->marketingSettingService->getSetting();
    $isEditing = true;

    return view('marketing.admin.other', compact('setting', 'isEditing'));
  }

  /**
   * @param UpdateMarketingSettingRequest $request
   * 
   * @return \Illuminate\Http\RedirectResponse
   */
  public function update(UpdateMarketingSettingRequest $request) {
    $setting = $this->marketingSetting->first();
    if(!$setting) {
      $setting = new MarketingSetting();
    }

    $setting->notification_receiver = $request->notification_receiver;
    $setting->package_price = $request->package_price;
    $setting->intro_video = $request->intro_video;
    $setting->save();

    return redirect()->route('cms.other-settings')
      ->with(['message' => 'Settings successfully updated!']);
  }
}
