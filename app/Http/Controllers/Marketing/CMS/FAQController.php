<?php

namespace App\Http\Controllers\Marketing\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\Marketing\FAQ\AddFAQRequest;
use App\Http\Requests\Marketing\FAQ\UpdateFAQRequest;
use App\Models\Marketing\FAQ;

class FAQController extends Controller
{

    /**
     * @var FAQ
     */
    private $faq;

    /**
     * FAQController constructor
     * 
     * @param FAQ $faq
     */
    public function __construct(FAQ $faq)
    {
        $this->faq = $faq;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $faqs = $this->faq->orderBy('created_at', 'desc')->get();

        return view('marketing.admin.faq.index', compact('faqs'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('marketing.admin.faq.create-edit');
    }

    /**
     * @param AddFAQRequest $request
     * 
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddFAQRequest $request)
    {
        $this->faq->create([
            'question' => $request->question,
            'answer' => $request->answer
        ]);

        return redirect()->to('cms/faq')
            ->with(['message' => 'FAQ successfully added!']);
    }

    /**
     * @param int $id
     * 
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($id)
    {
        $faq = $this->faq->find($id);

        if (!$faq) {
            return redirect()->to('cms/faq');
        }

        return view('marketing.admin.faq.create-edit', compact('faq'));
    }

    /**
     * @param UpdateFAQRequest $request
     * @param int $id
     * 
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateFAQRequest $request, $id)
    {
        $this->faq->find($id)
            ->update([
                'question' => $request->question,
                'answer' => $request->answer
            ]);

        return redirect()->to('cms/faq')
            ->with(['message' => 'FAQ successfully edited!']);
    }

    /**
     * @param int $id
     * 
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $this->faq->find($id)->delete();

        return redirect()->to('cms/faq')
            ->with(['message' => 'FAQ successfully deleted!']);
    }
}
