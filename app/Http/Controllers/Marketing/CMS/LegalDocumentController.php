<?php

namespace App\Http\Controllers\Marketing\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Marketing\LegalDocument;
use App\Services\Marketing\LegalDocumentService;
use App\Http\Requests\Marketing\LegalDocument\UpdateLegalDocumentRequest;
use App\Providers\Marketing\LegalDocumentProvider;

class LegalDocumentController extends Controller
{

    /**
     * @var LegalDocument
     */
    private $legalDocument;

    /**
     * @var LegalDocumentService
     */
    private $service;

    /**
     * @var LegalDocumentProvider
     */
    private $provider;

    /**
     * LegalDocument constructor
     * 
     * @param LegalDocument $legalDocument
     * @param LegalDocumentService $service
     * @param LegalDocumentProvider $provider
     */
    public function __construct(
        LegalDocument $legalDocument,
        LegalDocumentService $service,
        LegalDocumentProvider $provider
    ) {
        $this->legalDocument = $legalDocument;
        $this->service = $service;
        $this->provider = $provider;
    }

    public function index()
    {
        return view('marketing.admin.legal-documents.index');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function show($type)
    {

        $type = $this->service->getType($type);

        if (empty($type)) {
            return redirect()->to('/cms/legal-documents');
        }

        $document = $this->service->getDocument($type);

        return view('marketing.admin.legal-documents.show', compact('document'));
    }

    /**
     * @param string $type
     * 
     * @return \Illuminate\View\View
     */
    public function edit($type)
    {
        if ($type !== TERMS_AND_CONDITIONS_ABBREV && $type !== PRIVACY_POLICY_ABBREV) {
            return redirect()->to('/cms/legal-documents');
        }

        $document = $this->service->getDocument($type);

        return view('marketing.admin.legal-documents.edit', compact('document'));
    }

    /**
     * @param UpdateLegalDocumentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateLegalDocumentRequest $request)
    {
        $document = $this->legalDocument
            ->where('type', $request->type)
            ->orderBy('updated_at', 'desc');

        $document->update([
            'type' => $request->type,
            'content' => $request->content,
        ]);

        $type = $this->service->toSlug($request->type);

        return redirect()->to("cms/legal-documents/{$type}")
            ->with(['message' => 'Legal Document successfully edited!']);
    }
}
