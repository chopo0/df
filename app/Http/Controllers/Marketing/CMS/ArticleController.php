<?php

namespace App\Http\Controllers\Marketing\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Marketing\Article;
use App\Services\Marketing\BannerService;
use Carbon\Carbon;
use App\Http\Requests\Marketing\Article\AddArticleRequest;
use App\Http\Requests\Marketing\Article\UpdateArticleRequest;

class ArticleController extends Controller
{

    /**
     * @var Article
     */
    private $article;

    /**
     * ArticleController constructor
     * 
     * @param Article $article
     * @param Carbon $carbon
     * @param BannerService $bannerService
     */
    public function __construct(
        Article $article,
        Carbon $carbon,
        BannerService $bannerService
    ) {
        $this->article = $article;
        $this->carbon = $carbon;
        $this->bannerService = $bannerService;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $articles = $this->article->orderBy('updated_at', 'desc')->paginate(10);
        return view('marketing.admin.articles.index', compact('articles'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function show($id, $slug)
    {
        $article = $this->article
            ->where('id', $id)
            ->where('slug', $slug)
            ->first();

        if (empty($article)) {
            return redirect()->to('/cms/blog-article');
        }

        return view('marketing.admin.articles.show', compact('article'));
    }

    /**
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('marketing.admin.articles.create-edit');
    }

    /**
     * @param AddArticleRequest $request
     * 
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AddArticleRequest $request)
    {
        $this->article->create($this->mapRequestArray($request));

        return redirect()->to('cms/blog-article')
            ->with(['message' => 'Article successfully added!']);
    }

    /**
     * @param int $id
     * 
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $article = $this->article->find($id);

        if (!$article) {
            return redirect()->to('cms/blog-article');
        }

        return view('marketing.admin.articles.create-edit', compact('article'));
    }

    /**
     * @param UpdateArticleRequest $request
     * @param int $id
     * 
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateArticleRequest $request, $id)
    {
        $this->article->find($id)
            ->update($this->mapRequestArray($request));

        return redirect()->to('cms/blog-article')
            ->with(['message' => 'Article successfully edited!']);
    }

    /**
     * @param Illuminate\Foundation\Http\FormRequest $request
     * 
     * @return array
     */
    public function mapRequestArray($request)
    {
        $published = !empty($request->published);

        return [
            'title' => $request->title,
            'sub_title' => $request->sub_title,
            'slug' => str_slug($request->title),
            'cover' => $request->cover,
            'thumbnail' => str_replace(LFM_PATH, LFM_PATH . "thumbs/", $request->cover),
            'article' => $request->article,
            'published_on' => $published ? now()->toDateString() : null,
            'published' => $published
        ];
    }

    /**
     * @param int $id
     * 
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $this->article->find($id)->delete();

        return redirect()->to('cms/blog-article')
            ->with(['message' => 'Blog article successfully deleted!']);
    }
}
