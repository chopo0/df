<?php

namespace App\Http\Controllers\Marketing\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Marketing\MailingListItem;

class MailingListController extends Controller
{

    /**
     * @var MailingListItem
     */
    private $mailingListItem;

    /**
     * @var \Laracsv\Export
     */
    private $csvExporter;

    /**
     * MailingListItemController constructor.
     * 
     * @param MailingListItem $mailingListItem
     * @param \Laracsv\Export $csvExporter
     */
    public function __construct(MailingListItem $mailingListItem, \Laracsv\Export $csvExporter)
    {
        $this->mailingListItem = $mailingListItem;
        $this->csvExporter = $csvExporter;
    }

    /**
     * @param Request $request
     * 
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $mailingListItems = $this->mailingListItem->orderBy('created_at', 'desc')
            ->where('confirmed', true)
            ->paginate(10);

        return view('marketing.admin.mailing-list', compact('mailingListItems'));
    }

    /**
     * @param int $id
     * 
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $this->mailingListItem->find($id)->delete();

        return redirect()->to('cms/mailing-list')
            ->with(['message' => 'Item successfully deleted!']);
    }

    /**
     * Generates a CSV file from the Mailing List
     * 
     * @return void
     */
    public function downloadCSV()
    {
        $mailingListItems = $this->mailingListItem->orderBy('created_at', 'desc')
            ->where('confirmed', true)
            ->get();

        $this->csvExporter->beforeEach(function ($item) {
            $item->subscriptionDate = $item->created_at->format('M. d, Y h:ia');
        })->build($mailingListItems, [
            'email' => 'E-Mail Address'
        ])->download();
    }
}
