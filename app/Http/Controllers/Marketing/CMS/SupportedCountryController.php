<?php

namespace App\Http\Controllers\Marketing\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Marketing\SupportedCountry;
use App\Http\Requests\Marketing\SupportedCountry\AddSupportedCountryRequest;
use App\Http\Requests\Marketing\SupportedCountry\UpdateSupportedCountryRequest;

class SupportedCountryController extends Controller
{
    /**
     * @var SupportedCountry
     */
    private $supportedCountry;

    /**
     * SupportedCountryController constructor
     * 
     * @param SupportedCountry $supportedCountry
     */
    public function __construct(SupportedCountry $supportedCountry)
    {
        $this->supportedCountry = $supportedCountry;
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $supportedCountries = $this->supportedCountry->orderBy('created_at', 'asc')->get();
        return view('marketing.admin.supported-countries.index', compact('supportedCountries'));
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('marketing.admin.supported-countries.create-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddSupportedCountryRequest $request)
    {
        $this->supportedCountry->create(['name' => $request->name]);

        return redirect()->to('cms/country')
            ->with(['message' => 'Country successfully added!']);
    }

    /**
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($id)
    {
        $country = $this->supportedCountry->find($id);

        if (!$country) {
            return redirect()->to('cms/country');
        }

        return view('marketing.admin.supported-countries.create-edit', compact('country'));
    }

    /**
     * @param  UpdateSupportedCountryRequest  $request
     * @param  int $id
     * 
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateSupportedCountryRequest $request, $id)
    {
        $this->supportedCountry->find($id)
            ->update(['name' => $request->name]);

        return redirect()->to('cms/country')
            ->with(['message' => 'Country successfully edited!']);
    }

    /**
     * @param  int $id
     * 
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $this->supportedCountry->find($id)->delete();

        return redirect()->to('cms/country')
            ->with(['message' => 'Country successfully deleted!']);
    }
}
