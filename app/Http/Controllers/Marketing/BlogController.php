<?php

namespace App\Http\Controllers\Marketing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Marketing\Article;
use App\Services\Marketing\SEOService;

class BlogController extends BaseMarketingController
{
    /**
     * @var Article
     */
    private $article;

    /**
     * BlogController constructor
     * 
     * @param Article $article
     */
    public function __construct(
        Article $article
    ) {
        parent::__construct();
        $this->article = $article;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $banner = $this->bannerService->setDetails('Blog', 'Read about the latest Dry Forms Plus news.');
        $articles = $this->article
            ->orderBy('published_on', 'desc')
            ->where('published', true)
            ->paginate(10);
        $countries = $this->supportedCountry->all();

        $this->seoService->generateRegularMetaData('Blog');

        return view('marketing.blog', compact('banner', 'articles', 'countries'));
    }
}
