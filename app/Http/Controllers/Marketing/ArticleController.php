<?php

namespace App\Http\Controllers\Marketing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Marketing\BannerService;
use App\Services\Marketing\BlogService;
use Carbon\Carbon;
use App\Models\Marketing\Article;
use App\Models\Marketing\SupportedCountry;

class ArticleController extends BaseMarketingController
{
    /**
     * @var Carbon
     */
    private $carbon;

    /**
     * @var Article
     */
    private $article;

    /**
     * ArticleController constructor
     * 
     * @param Carbon $carbon
     * @param Article $article
     */
    public function __construct(
        Carbon $carbon,
        Article $article
    ) {
        parent::__construct();
        $this->carbon = $carbon;
        $this->article = $article;
    }

    /**
     * @param int $id
     * @param string $slug
     * 
     * @return \Illuminate\View\View
     */
    public function index($id, $slug)
    {
        $article = $this->article
            ->where('id', $id)
            ->where('slug', $slug)
            ->first();

        if (empty($article)) {
            return redirect()->to('/blog');
        }

        $countries = $this->supportedCountry->all();
        $recentArticles = $this->blogService->getRecentlyPublished($id);

        $this->seoService->generateArticleMetaData($article);
        return view('marketing.article', compact('article', 'countries', 'recentArticles'));
    }
}
