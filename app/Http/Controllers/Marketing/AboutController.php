<?php

namespace App\Http\Controllers\Marketing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\Marketing\AboutProvider;
use Illuminate\Foundation\Application;

class AboutController extends BaseMarketingController
{
    /**
     * @var AboutProvider
     */
    private $aboutProvider;

    /**
     * AboutController constructor
     * 
     * @param AboutProvider $aboutProvider
     */
    public function __construct(
        AboutProvider $aboutProvider
    ) {
        parent::__construct();
        $this->aboutProvider = $aboutProvider;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $data = $this->aboutProvider->getAboutDetails();
        $banner = $this->bannerService->setDetails(MKTG_SITE_NAME, 'Dry Forms Plus is the perfect document and equipment management solution for any serious restoration contractor.', '/assets/images/mktg-about-banner.png');
        $countries = $this->supportedCountry->all();
        $recentArticles = $this->blogService->getRecentlyPublished();

        $this->seoService->generateRegularMetaData('About');

        return view('marketing.about', compact('data', 'banner', 'countries', 'recentArticles'));
    }
}
