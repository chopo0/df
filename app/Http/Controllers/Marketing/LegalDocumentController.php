<?php

namespace App\Http\Controllers\Marketing;

use Illuminate\Http\Request;
use App\Services\Marketing\LegalDocumentService;
use App\Services\Marketing\BannerService;

class LegalDocumentController extends BaseMarketingController
{

    /**
     * @var LegalDocumentService
     */
    private $service;

    /**
     * LegalDocumentController constructor
     */
    public function __construct(
        LegalDocumentService $service
    ) {
        parent::__construct();
        $this->service = $service;
    }

    /**
     * @return Illuminate\View\View
     */
    public function showTermsAndConditions()
    {
        $this->seoService->generateRegularMetaData(TERMS_AND_CONDITIONS);
        return $this->show(TERMS_AND_CONDITIONS_ABBREV);
    }

    /**
     * @return Illuminate\View\View
     */
    public function showPrivacyPolicy()
    {
        $this->seoService->generateRegularMetaData(PRIVACY_POLICY);
        return $this->show(PRIVACY_POLICY_ABBREV);
    }

    public function show($type)
    {
        $banner = $this->bannerService->setDetails($this->service->getTitle($type));
        $countries = $this->supportedCountry->all();
        $document = $this->service->getDocument($type);

        return view('marketing.legal-document', compact('document', 'banner', 'countries'));
    }
}
