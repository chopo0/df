<?php

namespace App\Http\Controllers\Marketing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Marketing\BannerService;
use App\Models\Marketing\MailingListItem;
use App\Models\Marketing\SupportedCountry;
use App\Http\Requests\Marketing\MailingList\AddMailingListItemRequest;
use App\Http\Requests\Marketing\MailingList\CancelSubscriptionRequest;
use App\Mail\ConfirmMailingListSubscriptionCancellation;
use App\Mail\ConfirmMailingListSubscription;
use Illuminate\Mail\Mailer as Mail;

class MailingListController extends BaseMarketingController
{
    /**
     * @var MailingListItem
     */
    private $mailingListItem;

    /**
     * @var Mail
     */
    private $mail;

    /**
     * MailingListController constructor
     * 
     * @param MailingListItem $mailingListItem
     * @param Mail $mail
     */
    public function __construct(
        Mail $mail,
        MailingListItem $mailingListItem
    ) {
        parent::__construct();
        $this->mailingListItem = $mailingListItem;
        $this->mail = $mail;
    }

    /**
     * @return Illuminate\View\View
     */
    public function index()
    {
        $siteName = MKTG_SITE_NAME;
        $banner = $this->bannerService->setDetails('Mailing List Subscription', "Get the latest {$siteName} related news right to your inbox.");
        $countries = $this->supportedCountry->all();
        $recentArticles = $this->blogService->getRecentlyPublished();
        $this->seoService->generateRegularMetaData('Mailing List');
        return view('marketing.mailing-list-subscription', compact('banner', 'countries', 'recentArticles'));
    }

    /**
     * @param AddMailingListItemRequest $request
     * 
     * @return Illuminate\Http\RedirectResponse
     */
    public function store(AddMailingListItemRequest $request)
    {
        $query = $this->mailingListItem->where('email', $request->email);
        $route = 'mailing-list-subscription';

        $confirmMsg = 'Visit the confirmation link we\'ve emailed you to completely subscribe from our mailing list. Thank you.';

        if ($query->exists()) {
            $mailingListItem = $query->first();
            if (!$mailingListItem->confirmed) {
                $mailingListItem = $this->updateSubscriptionKey($request->email);
                $this->sendSubscriptionConfirmationEmail($mailingListItem);

                session()->flash('message', $confirmMsg);
                return redirect()->to($route);
            }

            session()->flash('warningMessage', 'You already subscribed to our mailing list. No need to subscribe again. Thank you.');
            return redirect()->to($route);
        }

        $mailingListItem = $this->create($request->email);
        $this->sendSubscriptionConfirmationEmail($mailingListItem);

        session()->flash('message', $confirmMsg);
        return redirect()->to($route);
    }

    /**
     * Update subscription key of an existsting Mailing List Item
     * Generates a Mailing List Item object to be emailed
     * 
     * @param string $email
     * 
     * @return stdClass
     */
    public function updateSubscriptionKey($email)
    {
        $query = $this->mailingListItem->where('email', $email);

        $confirmationKey = hash('sha512', microtime());
        $query->update([
            'subs_confirmation_key' => $confirmationKey,
            'confirmed' => false
        ]);

        $mailingListItem = new \stdClass();
        $mailingListItem->email = $email;
        $mailingListItem->subs_confirmation_key = $confirmationKey;

        return $mailingListItem;
    }

    /**
     * Creates a new Mailing List Item
     * 
     * @param string $email
     * 
     * @return MailingListItem
     */
    public function create($email)
    {
        $mailingListItem = new MailingListItem();
        $mailingListItem->email = $email;
        $mailingListItem->subs_confirmation_key = hash('sha512', microtime());
        $mailingListItem->confirmed = false;
        $mailingListItem->save();

        return $mailingListItem;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function cancel()
    {
        $banner = $this->bannerService->setDetails('Cancel Mailing List Subscription');
        $countries = $this->supportedCountry->all();
        return view('marketing.mailing-list.remove', compact('banner', 'countries'));
    }

    /**
     * @param CancelSubscriptionRequest $request
     * 
     * @return Illuminate\Http\RedirectResponse
     */
    public function verify(CancelSubscriptionRequest $request)
    {
        $query = $this->mailingListItem->where('email', $request->email)
            ->where('confirmed', true);

        if ($query->exists()) {
            $mailingListItem = $this->generateMailingListItem($query, $request->email);
            $this->sendRemovalConfirmationEmail($mailingListItem);

            session()->flash('message', 'Visit the confirmation link we\'ve emailed you to be completely removed from our mailing list. Thank you.');
            return redirect()->to('mailing-list-subscription/cancel');
        }

        session()->flash('warningMessage', 'Can\'t find email. It seems that you\'ve already unsubcribed from our mailing list. Thank you.');
        return redirect()->to('mailing-list-subscription/cancel');
    }

    /**
     * Generates a Mailing List item to be removed
     * and setting an unsubscribe key for it
     * 
     * @param Builder $query
     * @param string $email
     * 
     * @return stdClass
     */
    public function generateMailingListItem($query, $email)
    {

        $cancellationKey = hash('sha512', microtime());
        $query->update(['subs_cancellation_key' => $cancellationKey]);

        $mailingListItem = new \stdClass();
        $mailingListItem->email = $email;
        $mailingListItem->cancellationKey = $cancellationKey;

        return $mailingListItem;
    }

    /**
     * Sends removal confirmation email to the requester
     * 
     * @param stdClass $mailingListItem
     * 
     * @return void
     */
    public function sendRemovalConfirmationEmail($mailingListItem)
    {
        // Just in case the email failed to send.
        try {
            $this->mail->to($mailingListItem->email)->send(new ConfirmMailingListSubscriptionCancellation($mailingListItem));
        } catch (\Exception $e) {
        }
    }

    /**
     * Sends subscription confirmation email to the requester
     * 
     * @param stdClass $mailingListItem
     * 
     * @return void
     */
    public function sendSubscriptionConfirmationEmail($mailingListItem)
    {
        // Just in case the email failed to send.
        try {
            $this->mail->to($mailingListItem->email)->send(new ConfirmMailingListSubscription($mailingListItem));
        } catch (\Exception $e) {
        }
    }

    /**
     * Deletes item matching the cancellation key
     * 
     * @param string $cancellationKey
     * 
     * @return Illuminate\Http\RedirectResponse
     */
    public function confirmRemoval($cancellationKey)
    {
        $query = $this->mailingListItem->where('subs_cancellation_key', $cancellationKey);

        if ($query->exists()) {
            $query->delete();
            session()->flash('message', 'You\'re now unsubscribed from our mailing list.');
            return redirect()->to('mailing-list-subscription/cancel');
        }

        session()->flash('errorMessage', 'Confirmation link expired. Please try again.');
        return redirect()->to('mailing-list-subscription/cancel');
    }

    /**
     * Confirms item matching the confirmation key
     * 
     * @param string $confirmationKey
     * 
     * @return Illuminate\Http\RedirectResponse
     */
    public function confirm($confirmationKey)
    {
        $query = $this->mailingListItem->where('subs_confirmation_key', $confirmationKey);

        if ($query->exists()) {

            $query->update([
                'subs_confirmation_key' => null,
                'confirmed' => true,
            ]);

            session()->flash('message', 'You\'re now subscribing to our mailing list. Thank you!');
            return redirect()->to('mailing-list-subscription');
        }

        session()->flash('errorMessage', 'Confirmation link expired. Please try again.');
        return redirect()->to('mailing-list-subscription');
    }
}
