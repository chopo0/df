<?php

namespace App\Http\Controllers\Marketing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\Marketing\FeatureProvider;

class FeatureController extends BaseMarketingController
{
    /**
     * @var FeatureProvider
     */
    private $featureProvider;


    /**
     * FeatureController constructor
     * 
     * @param FeatureProvider $featureProvider
     */
    public function __construct(
        FeatureProvider $featureProvider
    ) {
        parent::__construct();
        $this->featureProvider = $featureProvider;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $banner = $this->bannerService->setDetails('Features', 'There are a number of reasons to love ' . MKTG_SITE_NAME . '.', null);
        $features = $this->featureProvider->getFeatures();
        $countries = $this->supportedCountry->all();
        $recentArticles = $this->blogService->getRecentlyPublished();

        $this->seoService->generateRegularMetaData('Features');

        return view('marketing.features', compact('banner', 'features', 'countries', 'recentArticles'));
    }
}
