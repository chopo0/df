<?php

namespace App\Http\Controllers\Marketing;

use Illuminate\Http\Request;
use App\Providers\Marketing\FeatureProvider;
use App\Services\Marketing\MarketingSettingService;
use App\Services\Marketing\CompanyService;

class HomeController extends BaseMarketingController
{
    /**
     * @var FeatureProvider
     */
    private $featureProvider;

    /**
     * @var MarketingSettingService
     */
    private $marketingSettingService;

    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * HomeController constructor
     * 
     * @param FeatureProvider $featureProvider
     * @param MarketingSettingService $marketingSettingService
     */
    public function __construct(
        FeatureProvider $featureProvider,
        MarketingSettingService $marketingSettingService,
        CompanyService $companyService
    ) {
        parent::__construct();
        $this->featureProvider = $featureProvider;
        $this->marketingSettingService = $marketingSettingService;
        $this->companyService = $companyService;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $features = $this->featureProvider->getFeatures();
        $setting = $this->marketingSettingService->getSetting();
        $logos = $this->companyService->getLogosByFours();
        $recentArticles = $this->blogService->getRecentlyPublished();
        $countries = $this->supportedCountry->all();

        $this->seoService->generateRegularMetaData('Home');

        return view('marketing.home', compact('features', 'setting', 'countries', 'logos', 'recentArticles'));
    }
}
