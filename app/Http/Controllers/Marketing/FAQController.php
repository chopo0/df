<?php

namespace App\Http\Controllers\Marketing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Marketing\FAQ;

class FAQController extends BaseMarketingController
{
    /**
     * @var FAQ
     */
    private $faq;

    /**
     * FAQController constructor
     * 
     * @param FAQ $faq
     */
    public function __construct(
        FAQ $faq
    ) {
        parent::__construct();
        $this->faq = $faq;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $banner = $this->bannerService->setDetails('Frequently Asked Questions', 'Here are some of the questions we usually get. Take a look.');
        $faqs = $this->faq->all();
        $countries = $this->supportedCountry->all();
        $recentArticles = $this->blogService->getRecentlyPublished();

        $this->seoService->generateRegularMetaData('FAQs');

        return view('marketing.faq', compact('banner', 'faqs', 'countries', 'recentArticles'));
    }
}
