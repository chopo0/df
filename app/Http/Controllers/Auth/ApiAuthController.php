<?php
namespace App\Http\Controllers\Auth;

use App\Events\UserRegistered;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\SetPasswordRequest;
use App\Models\Role;
use App\Models\User;
use App\Notifications\Users\ResetPassword;
use App\Services\Subscriptions\SubscriptionService;
use Illuminate\Database\Connection;
use Illuminate\Http\Request;
use Stripe\Error\InvalidRequest;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Hashing\Hasher;

class ApiAuthController extends LoginController
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var JWTAuth
     */
    private $jwtAuth;

    /**
     * @var Connection
     */
    private $db;

    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * @var Hasher
     */
    private $hasher;

    /**
     * ApiAuthController constructor.
     * @param User $user
     * @param JWTAuth $jwtAuth
     * @param Connection $db
     * @param SubscriptionService $subscriptionService
     * @param Hasher $hasher
     */
    public function __construct(
        User $user,
        JWTAuth $jwtAuth,
        Connection $db,
        SubscriptionService $subscriptionService,
        Hasher $hasher
    ) {
        parent::__construct();
        $this->user = $user;
        $this->jwtAuth = $jwtAuth;
        $this->db = $db;
        $this->subscriptionService = $subscriptionService;
        $this->hasher = $hasher;
    }

    /**
     * @param SetPasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setPassword(SetPasswordRequest $request)
    {
        $user = $this->user->where('password_set_token', $request->get('token'))->first();
        $user->password = $this->hasher->make($request->input('password'));
        $user->password_set_token = null;
        $user->save();

        return response()->json([
            'message' => 'Your password successfully set',
        ]);
    }

    /**
     * @param ResetPasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetPasswordRequest(ResetPasswordRequest $request)
    {
        $user = $this->user->where('email', $request->get('email'))->first();
        $user->password_set_token = str_random(32);
        $user->save();
        $user->notify(new ResetPassword($user));

        return response()->json([
            'message' => 'Your request successfully submitted. Check your email to proceed.',
        ]);

    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);

        try {
            $token = $this->jwtAuth->attempt($credentials);

            if (!$token) {
                return response()->json([
                    'message' => 'Invalid credentials'
                ], 401);
            }
            $user = $this->user->where('email', $request->get('email'))->first();
            $user->timezone = $request->get('timezone');
            $user->save();

            if ($user->status === 'suspended' && ($user->role_id !== Role::SUPER_ADMIN || $user->role_id !== Role::ADMIN)) {
                throw new JWTException();
            }
        } catch (JWTException $e) {
            return response()->json([
                'message' => 'Failed to create token'
            ], 401);
        }

        return response()->json([
            'token' => $token,
        ]);
    }

    /**
     * @param RegisterRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        $tokenData = $request->get('stripeToken');

        try {
            $this->db->beginTransaction();
            $user = $this->user->create([
                'first_name' => $request->input('first_name'),
                'last_name' => $request->input('last_name'),
                'address' => $request->input('address'),
                'city' => $request->input('city'),
                'state' => $request->input('state'),
                'zip' => $request->input('zip'),
                'phone' => $request->input('phone'),
                'email' => $request->input('email'),
                'password' => $this->hasher->make($request->input('password')),
                'role_id' => Role::ADMIN,
                'verified' => 1,
                'timezone' => date_default_timezone_get(),
                'coupon' => $request->input('coupon')
            ]);

            $this->subscriptionService->subscribe($user, $tokenData, $request->input('coupon'));
            event(new UserRegistered($user));
            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollBack();

            return response()->json([
                'message' => $e->getMessage()
            ], 400);
        }
        $token = $this->jwtAuth->fromUser($user);

        $this->guard()->login($user);

        return response()->json([
            'message' => 'Welcome to DryForms!',
            'token' => $token,
            'id' => $user->id
        ]);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh(Request $request)
    {
        try {
            $token = $this->jwtAuth->parseToken()->refresh();
        } catch (JWTException $e) {
            throw new UnauthorizedHttpException('jwt-auth', $e->getMessage(), $e, $e->getCode());
        }

        return response()->json([
            'token' => $token,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     * @throws JWTException
     */
    public function logout(Request $request)
    {
        $this->jwtAuth->parseToken()->invalidate();

        return response()->json([
            'message' => 'Success',
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAuthUser(){
        $user = $this->jwtAuth->toUser();

        return response()->json(['result' => $user]);
    }
}
