<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;

class StripeController extends ApiController
{
    /**
     * @var User
     */
    private $user;

    /**
     * StripeController constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function webhook(Request $request)
    {
        if ($this->isSubscriptionDeleted($request->get('type'))) {
            $subscriptionData = $request->get('data');
            $user = $this->user->where('stripe_id', $subscriptionData['object']['customer'])->firstOrFail();

            $this->user->where('company_id', $user->company_id)->update(['status' => 'suspended']);
        }

        return $this->respond(['message' => 'success']);
    }

    /**
     * @param string $type
     * @return bool
     */
    private function isSubscriptionDeleted(string $type): bool
    {
        return $type === 'customer.subscription.deleted';
    }
}
