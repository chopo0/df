<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\StandardScopes\StandardScopesIndex;
use App\Http\Requests\StandardScopes\StandardScopesStore;
use App\Http\Requests\StandardScopes\StandardScopesUpdate;
use Illuminate\Http\Request;

use App\Models\StandardScope;
use App\Models\DefaultScope;

use Symfony\Component\HttpFoundation\JsonResponse;

class StandardScopesController extends ApiController
{
    /**
     * @var StandardScope
     */
    private $standard_scope;

    /**
     * @var DefaultScope
     */
    private $default_scope;
    /**
     * StandardScopesController constructor.
     *
     * @param StandardScope $standard_scope
     * @param DefaultScope $default_scope
     */
    public function __construct(StandardScope $standard_scope, DefaultScope $default_scope)
    {
        $this->standard_scope = $standard_scope;
        $this->default_scope = $default_scope;
    }

    /**
     * @return void
     */
    private function revertScope()
    {
        $defaultScopes = $this->default_scope->get()->toArray();
        foreach ($defaultScopes as $key => $scope) {
            $scope['company_id'] = auth()->user()->company_id;
            $this->standard_scope->create($scope);
        }
    }

    /**
     * @param StandardScopesIndex $request
     *
     * @return JsonResponse
     */
    public function index(StandardScopesIndex $request): JsonResponse
    {
        if ($request->get('curPageNum') !== null) {
            $scopes = $this->standard_scope
                ->where('page', '!=', 0)
                ->orderBy('sort_id', 'asc')
                ->get();
            $maxPage = $this->standard_scope->max('page');

            return $this->respond([
                'curPageScopes' => $scopes,
                'maxPage' => $maxPage
            ]);
        } else {
            $scopes = $this->standard_scope
                ->with('uom_info')
                ->where('page', '<>', 0)
                ->orderBy('sort_id', 'asc')
                ->get()
                ->groupBy('page');
            $miscScopes = $this->standard_scope
                ->with('uom_info')
                ->where('page', '=', 0)
                ->orderBy('sort_id', 'asc')
                ->get();

            return $this->respond([
                'scopes' => $scopes,
                'misc_scopes' => $miscScopes
            ]);
        }
    }

    /**
     * @param StandardScopesStore $request
     *
     * @return JsonResponse
     */
    public function store(StandardScopesStore $request): JsonResponse
    {
    	$scopes = $request->get('scopes');
    	foreach ($scopes as $key => $scope) {
            $scope['sort_id'] = $key + 1;
            $this->standard_scope->findOrFail($scope['id'])->update($scope);
        }

        return $this->respond(['message' => 'Standard Scopes successfully saved']);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function addPage()
    {
        $existingScope = $this->standard_scope->first();
        $page = $this->standard_scope
            ->where('page', '!=', 0)
            ->orderBy('sort_id', 'asc')
            ->max('page');
        $total = $this->standard_scope->where('page', 0)->count();
        for ($i = 0; $i < $total; $i++) {
            $this->standard_scope->create([
                'company_id' => $existingScope->company_id,
                'header' => false,
                'page' => $page + 1,
                'sort_id' => $i + 1
            ]);
        }

        return $this->respond(['message' => 'Standard Scopes successfully updated']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletePage(Request $request)
    {
        $this->standard_scope->where('page', $request->get('page'))->delete();

        return $this->respond(['message' => 'Standard Scopes successfully updated']);
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $this->standard_scope->where('page', $id)->delete();
        $maxPage = $this->standard_scope->max('page');
        for ($page = $id + 1; $page <= $maxPage; $page++) {
        	$this->standard_scope
        		->where('page', $page)
        		->update(['page' => $page - 1]);
        }
        return $this->respond(['message' => 'Scope Page successfully deleted']);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function revert(Request $request): JsonResponse
    {
        $this->standard_scope
            ->where('company_id', auth()->user()->company->id)
            ->delete();
        $this->revertScope();

        return $this->respond([
            'message' => 'Scopes successfully reverted'
        ]);
    }
}
