<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ProjectScopes\ProjectScopesIndex;
use App\Http\Requests\ProjectScopes\ProjectScopesStore;
use App\Http\Requests\ProjectScopes\ProjectScopesUpdate;

use App\Models\ProjectScope;
use App\Models\StandardScope;
use App\Models\DefaultScope;
use App\Models\ProjectArea;

use App\Services\Scopes\ScopesService;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProjectScopesController extends ApiController
{
	/**
     * @var ProjectScope
     */
    private $projectScope;

    /**
     * @var StandardScope
     */
    private $standardScope;

    /**
     * @var DefaultScope
     */
    private $defaultScope;

    /**
     * @var ProjectArea
     */
    private $projectArea;

    /**
     * @var ScopesService
     */
    private $scopesService;

    /**
     * ProjectScopesController constructor.
     * @param ProjectScope $projectScope
     * @param StandardScope $standardScope
     * @param DefaultScope $defaultScope
     * @param ProjectArea $projectArea
     * @param ScopesService $scopesService
     */
    public function __construct(
        ProjectScope $projectScope,
        StandardScope $standardScope,
        DefaultScope $defaultScope,
        ProjectArea $projectArea,
        ScopesService $scopesService
    ) {
    	$this->projectScope = $projectScope;
        $this->standardScope = $standardScope;
        $this->defaultScope = $defaultScope;
        $this->projectArea = $projectArea;
        $this->scopesService = $scopesService;
    }

	/**
     * @param ProjectScopesIndex $request
     *
     * @return JsonResponse
     */
    public function index(ProjectScopesIndex $request): JsonResponse
    {
        $scopes = [];
        $areasData = [];
        if ($request->input('areas')) {
            foreach ($request->input('areas') as $area) {
                $areaData = json_decode($area);
                $areasData[$areaData->id] = $areaData->title;
                $scopes[$areaData->id] = $this->projectScope
                    ->with(['uom_info'])
                    ->where('project_id', $request->input('project_id'))
                    ->where('page', '!=', 0)
                    ->where('project_area_id', $areaData->id)
                    ->orderBy('sort_id')
                    ->get()
                    ->groupBy('page');
            }
        }

        $miscScopes = $this->projectScope
            ->with(['uom_info'])
            ->where('project_id', $request->input('project_id'))
            ->where('page', 0)
            ->orderBy('sort_id', 'asc')
            ->get();

        return $this->respond([
            'scopes' => $scopes,
            'areas_data' => $areasData,
            'misc_page_scopes' => $miscScopes
        ]);
    }

    /**
     * @param ProjectScopesUpdate $request
     *
     * @return JsonResponse
     */
    public function update(ProjectScopesUpdate $request): JsonResponse
    {
        $projectScope = $this->projectScope
            ->findOrFail($request->input('id'));
        $projectScope->update($request->validatedOnly());

        return $this->respond(['message' => 'Scope successfully updated', 'scope' => $projectScope]);
    }
}
