<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Dashboard\DashboardEquipmentRequest;
use App\Http\Requests\Dashboard\DashboardProjectsRequest;
use App\Models\Category;
use App\Models\Project;
use App\Models\Status;
use Illuminate\Database\Connection;

class DashboardController extends ApiController
{
    /**
     * @var Project
     */
    private $project;

    /**
     * @var Connection
     */
    private $db;

    /**
     * @var Category
     */
    private $category;

    /**
     * DashboardController constructor.
     * @param Project $project
     * @param Connection $db
     * @param Category $category
     */
    public function __construct(Project $project, Connection $db, Category $category)
    {
        $this->project = $project;
        $this->db = $db;
        $this->category = $category;
    }

    /**
     * @param DashboardProjectsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function projects(DashboardProjectsRequest $request)
    {
        $year = $request->get('year');
        $team = $request->get('team');

        $projects = $this->project->where('created_at', 'like', "%{$year}%")
            ->where('assigned_to', $team)
            ->groupBy('status')
            ->get([$this->db->raw('count(id) as total'), 'status']);

        $data = ['all' => 0];
        foreach ($projects as $project) {
            $data[snake_case($project->status_info->name)] = $project->total;
            $data['all'] += $project->total;
        }

        return $this->respond(['data' => $data]);
    }

    /**
     * @param DashboardEquipmentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function equipment(DashboardEquipmentRequest $request)
    {
        $data = [];
        $team = $request->get('team');
        $categories = $this->category->with('equipments')->orderBy('name', 'asc')->get();

        foreach ($categories as $category) {
            $data[] = [
                'name' => $category->name,
                'available' => $this->fetchEquipmentQuantityByStatusAndTeam($team, Status::AVAILABLE, $category),
                'set' => $this->fetchEquipmentQuantityByStatusAndTeam($team, Status::SET, $category),
                'loan' => $this->fetchEquipmentQuantityByStatusAndTeam($team, Status::LOAN, $category),
                'missing' => $this->fetchEquipmentQuantityByStatusAndTeam($team, Status::MISSING, $category),
                'ooc' => $this->fetchEquipmentQuantityByStatusAndTeam($team, Status::OOC, $category),
                'total' => $category->equipments()->where('team_id', '=', $team)->count()
            ];
        }

        return $this->respond(['data' => $data]);
    }

    /**
     * @param int $teamId
     * @param int $statusId
     * @param Category $category
     * @return int
     */
    private function fetchEquipmentQuantityByStatusAndTeam(int $teamId, int $statusId, Category $category)
    {
        return $category->equipments()
            ->where('team_id', '=', $teamId)
            ->where('status_id', '=', $statusId)
            ->count();
    }
}
