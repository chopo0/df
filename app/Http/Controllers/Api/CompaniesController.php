<?php
namespace App\Http\Controllers\Api;

use App\Http\Requests\Companies\RemoveLogoRequest;
use App\Http\Requests\Companies\StoreLogoRequest;
use Illuminate\Filesystem\FilesystemManager;
use Illuminate\Http\Request;
use App\Http\Requests\Companies\CompanyStore;
use App\Http\Requests\Companies\CompanyUpdate;
use App\Models\Company;
use App\Models\User;
use Intervention\Image\ImageManager as Image;
use Symfony\Component\HttpFoundation\JsonResponse;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Cache\CacheManager as Cache;

class CompaniesController extends ApiController
{
    /**
     * @var Company
     */
    private $company;

    /**
     * @var User
     */
    private $user;

    /**
     * @var JWTAuth
     */
    private $jwtAuth;

    /**
     * @var Cache
     */
    private $cache;

    /**
     * @var Image
     */
    private $image;

    /**
     * CompaniesController constructor.
     * @param Company $company
     * @param User $user
     * @param JWTAuth $jwtAuth
     * @param Cache $cache
     * @param Image $image
     */
    public function __construct(Company $company, User $user, JWTAuth $jwtAuth, Cache $cache, Image $image)
    {
        $this->company = $company;
        $this->user = $user;
        $this->jwtAuth = $jwtAuth;
        $this->cache = $cache;
        $this->image = $image;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $companies = $this->company->paginate(20);

        return $this->respond($companies);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function logos()
    {
        $companies = $this->company->where('permit_to_use_logo', true)->get(['logo', 'name']);

        return $this->respond($companies);
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $company = $this->company->findOrFail($id);

        return $this->respond($company);
    }

    /**
     * @param CompanyStore $request
     *
     * @return JsonResponse
     */
    public function store(CompanyStore $request): JsonResponse
    {
        $user = auth()->user();
        $company = $this->company->create([
            'name' => $request->input('name'),
            'street' => $request->input('street'),
            'city' => $request->input('city'),
            'state' => $request->input('state'),
            'zip' => $request->input('zip'),
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'user_id' => $user->id,
            'permit_to_use_logo' => $request->input('permit_to_use_logo') ?? false,
        ]);

        $user->company_id = $company->id;
        $user->save();

        return $this->respond([
            'message' => 'Company successfully created',
            'company' => $company,
            'user' => $user
        ]);
    }

    /**
     * @param CompanyUpdate $request
     * @return JsonResponse
     */
    public function update(CompanyUpdate $request): JsonResponse
    {
        $company = $this->company->find($request->route('company'));
        $company->update([
            'name' => $request->input('name'),
            'street' => $request->input('street'),
            'city' => $request->input('city'),
            'state' => $request->input('state'),
            'zip' => $request->input('zip'),
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'review_requester_status' => $request->input('review_requester_status'),
            'skip_disclaimer' => $request->input('skip_disclaimer') ?? false,
            'permit_to_use_logo' => $request->input('permit_to_use_logo') ?? false,
        ]);

        return $this->respond([
            'message' => 'Company details successfully updated',
            'company' => $company
        ]);
    }

    /**
     * @param StoreLogoRequest $request
     * @param FilesystemManager $filesystemManager
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeLogo(StoreLogoRequest $request, FilesystemManager $filesystemManager): JsonResponse
    {
        $company = $this->company->findOrFail($request->route('company'));
        $logoFile = $request->file('logo');
        $filename = str_random() . '.' . $logoFile->getClientOriginalExtension();

        $img = $this->image->make($request->file('logo'));
        $img->resize(200, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        $img->save("storage/logos/$filename", 80);
        $company->logo = "logos/$filename";
        $company->save();

        return response()->json([
            'company' => $company
        ]);
    }

    /**
     * @param RemoveLogoRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeLogo(RemoveLogoRequest $request): JsonResponse
    {
        $company = $this->company->findOrFail($request->route('company'));
        $company->logo = null;
        $company->save();

        return response()->json([
            'message' => 'Logo successfully removed',
            'company' => $company
        ]);
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $this->company->findOrFail($id)->delete();

        return $this->respond(['message' => 'Company successfully deleted']);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function dropboxAuth(Request $request)
    {
        return view('auth.dropbox-auth');
    }

    /**
     * @param Request $request
     */
    public function setIdentifier(Request $request)
    {
        $this->cache->put('identity', $request->route('id'), 1);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function storeDropboxAuth(Request $request)
    {
        $company = $this->company->findOrFail($this->cache->getStore()->get('identity'));
        $company->dropbox_access_token = $request->get('accessToken');
        $company->dropbox_token = $request->get('_token');
        $company->save();
        $this->cache->delete('identity');

        return view('auth.dropbox-auth', compact('company'));
    }
}
