<?php

namespace App\Http\Controllers\Api;

use App\Models\ProjectCallReport;
use App\Models\Project;
use App\Models\ProjectStatus;
use App\Models\ReviewLink;
use App\Models\ReviewRequestMessage;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\ProjectCallReport\ProjectCallReportIndex;
use App\Http\Requests\ProjectCallReport\ProjectCallReportStore;
use App\Http\Requests\ProjectCallReport\ProjectCallReportUpdate;

class CallReportsController extends ApiController
{
    /**
     * @var ProjectCallReport
     */
    private $projectCallReport;

    /**
     * @var Project
     */
    private $project;

    /**
     * @var ReviewLink
     */
    private $reviewLink;

    /**
     * @var ReviewRequestMessage
     */
    private $reviewRequestMessage;

    /**
     * ProjectCallReportsController constructor.
     *
     * @param ProjectCallReport $projectCallReport
     * @param Project $project
     * @param ReviewLink $reviewLink
     * @param ReviewRequestMessage $reviewRequestMessage
     */
    public function __construct(ProjectCallReport $projectCallReport, Project $project, ReviewLink $reviewLink, ReviewRequestMessage $reviewRequestMessage)
    {
        $this->projectCallReport = $projectCallReport;
        $this->project = $project;
        $this->reviewLink = $reviewLink;
        $this->reviewRequestMessage = $reviewRequestMessage;
    }

    /**
     * @param ProjectCallReportIndex $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ProjectCallReportIndex $request): JsonResponse
    {
        $project = $this->project->findOrFail($request->route('project'));
        $projectCallReport = $this->projectCallReport
        	->where('project_id', $request->route('project'))
        	->first();

        return $this->respond([
            'call_report' => $projectCallReport ?? new ProjectCallReport() ,
            'project' => $project
        ]);
    }

     /**
     * @param ProjectCallReportStore $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ProjectCallReportStore $request)
    {
        $callReportData = $request->validatedOnly();
        $callReportData['date_contacted'] = $callReportData['date_contacted'] ? (new Carbon($callReportData['date_contacted']))->format('Y-m-d') : null;
        $projectCallReport = $this->projectCallReport->create($callReportData);

        return $this->respond([
            'message' => 'Project Call Report successfully created',
            'call_report' => $projectCallReport
        ]);
    }

    /**
     * @param ProjectCallReportUpdate $request
     * @return JsonResponse
     */
    public function update(ProjectCallReportUpdate $request)
    {
        $projectCallReport = $this->projectCallReport->findOrFail($request->input('call_report_id'));
        $callReportData = $request->validatedOnly();
        $callReportData['date_contacted'] = $callReportData['date_contacted'] ? (new Carbon($callReportData['date_contacted']))->format('Y-m-d') : null;
        $callReportData['date_loss'] = $callReportData['date_loss'] ? (new Carbon($callReportData['date_loss']))->format('Y-m-d') : null;
        $callReportData['date_completed'] = $callReportData['date_completed'] ? (new Carbon($callReportData['date_completed']))->format('Y-m-d') : null;
        $hasEquipmentSet = isset($callReportData['date_completed']) && $projectCallReport->project->hasEquipmentSet();
        if ($hasEquipmentSet) {
            $callReportData['date_completed'] = null;
        }
        $dateCompletedAdded = is_null($projectCallReport->date_completed) && isset($callReportData['date_completed']);
        $projectCallReport->update($callReportData);

        /** @var Project $project */
        $project = $this->project->findOrFail($request->get('project_id'));
        $project->update([
            'owner_name' => $request->get('insured_name'),
            'address' => $request->get('job_address'),
            'phone' => $request->get('insured_cell_phone') ? $request->get('insured_cell_phone') : ($request->get('insured_home_phone') ? $request->get('insured_home_phone') : ($request->get('insured_work_phone') ? $request->get('insured_work_phone'): '')),
            'status' => $request->get('date_completed') ? ProjectStatus::COMPLETED : ProjectStatus::IN_PROGRESS,
            'assigned_to' => $request->get('assigned_to')
        ]);

        return $this->respond([
            'message' => 'Project Call Report successfully updated',
            'call_report' => $projectCallReport,
            'date_completed_added' => $dateCompletedAdded,
            'has_set_equipment' => $hasEquipmentSet
        ]);
    }

}
