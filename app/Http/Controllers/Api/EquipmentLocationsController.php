<?php

namespace App\Http\Controllers\Api;

use App\Models\Equipment;
use App\Models\Project;
use App\Models\ProjectStatus;

class EquipmentLocationsController extends ApiController
{
    /**
     * @var Project
     */
    private $project;

    /**
     * @var Equipment
     */
    private $equipment;

    /**
     * EquipmentLocationsController constructor.
     * @param Project $project
     * @param Equipment $equipment
     */
    public function __construct(Project $project, Equipment $equipment)
    {
        $this->project = $project;
        $this->equipment = $equipment;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $locations = [
            [
                'id' => null,
                'location' => '-----'
            ]
        ];
        $projects = $this->project->with('project_call_report')->where('status', ProjectStatus::IN_PROGRESS)->get();
        foreach ($projects as $project) {
            if (!$project->project_call_report || !$project->project_call_report->full_job_address) continue;
            $locations[] = [
                'id' => $project->id,
                'location' => "{$project->project_call_report->insured_name} {$project->project_call_report->full_job_address}"
            ];
        }

        return $this->respond($locations);
    }
}
