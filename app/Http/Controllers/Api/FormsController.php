<?php
namespace App\Http\Controllers\Api;

use App\Models\DefaultFromData;
use App\Models\DefaultStatement;
use App\Models\StandardForm;
use App\Models\StandardStatement;
use App\Models\Form;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FormsController extends ApiController
{
    /**
     * @var Form
     */
    private $form;

    /**
     * @var DefaultFromData
     */
    private $standardForm;

    /**
     * @var DefaultStatement
     */
    private $standardStatement;

    private $defaultFormData;

    /**
     * FormsController constructor.
     *
     * @param Form $form
     * @param DefaultFromData $defaultFromData
     */
    public function __construct(Form $form, StandardForm $standardForm, StandardStatement $standardStatement, DefaultFromData $defaultFormData)
    {
        $this->form = $form;
        $this->standardForm = $standardForm;
        $this->standardStatement = $standardStatement;
        $this->defaultFormData = $defaultFormData;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        $forms = $this->form->get();

        return $this->respond($forms);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $form = $this->form->create([
            'company_id' => auth()->user()->company_id,
            'name' => $request->get('form_name')
        ]);
        $this->standardForm->create([
            'company_id' => auth()->user()->company_id,
            'form_id' => $form->id,
            'name' => $request->get('form_menuname'),
            'title' => $request->get('form_title'),
            'additional_notes_show' => $request->get('addNotes'),
            'footer_text_show' => $request->get('addFooter'),
            'sort_id' => $this->standardForm->max('sort_id') + 1,
            'custom' => 1
        ]);
        $statements = $request->get('statements');
        foreach($statements as $key => $statement){
            $this->standardStatement->create([
                'company_id' => auth()->user()->company_id,
                'form_id' => $form->id,
                'title' => isset($statement['title']) ? $statement['title'] : null,
                'statement' => $statement['statement']
            ]);
        }

        return $this->respond(['message' => 'Form successfully saved']);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $form = $this->form->findOrFail($id);
        $form->default_data = $this->defaultFormData->where('form_id', $form->id)->first();

        return $this->respond($form);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id)
    {
        $standardForm = $this->standardForm->findOrFail($id);
        $standardForm->delete();

        return $this->respond(['message' => 'Form successfully deleted']);
    }
}
