<?php

namespace App\Http\Controllers\Api;

use App\Models\Role;
use App\Models\User;
use App\Models\Ticket;
use App\Models\TicketComment;
use App\Notifications\Tickets\TicketCommentForAdmin;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class TicketCommentsController extends ApiController
{
    /**
     * @var Ticket
     */
    private $ticket;

    /**
     * @var TicketComment
     */
    private $ticketComment;

    /**
     * @var User
     */
    private $user;

    /**
     * TicketCommentsController constructor.
     * @param Ticket $ticket
     * @param TicketComment $ticketComment
     * @param User $user
     */
    public function __construct(Ticket $ticket, TicketComment $ticketComment, User $user)
    {
        $this->ticket = $ticket;
        $this->ticketComment = $ticketComment;
        $this->user = $user;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $this->validate($request, [
            'comment' => 'required'
        ]);

        $ticketComment = $this->ticketComment->create([
            'ticket_id' => $request->input('ticket_id'),
            'user_id' => auth()->user()->id,
            'comment' => $request->input('comment')
        ]);

        $admins = $this->user->where('role_id', Role::SUPER_ADMIN)->get();
        foreach ($admins as $admin) {
            $admin->notify(new TicketCommentForAdmin($ticketComment->ticket));
        }

        return $this->respond([
            'message' => "Your comment has be submitted.",
            'comment' => $ticketComment
        ]);
    }
}
