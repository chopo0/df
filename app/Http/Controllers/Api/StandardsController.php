<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\StandardForms\StandardFormsIndex;
use App\Http\Requests\StandardForms\StandardFormStore;
use App\Http\Requests\StandardForms\StandardFormUpdate;
use App\Http\Requests\StandardForms\StatementStore;

use App\Models\StandardForm;
use App\Models\DefaultFromData;
use App\Models\StandardStatement;
use App\Models\DefaultStatement;
use App\Models\StandardScope;
use App\Models\DefaultScope;

use App\Services\Projects\StandardStatementsService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use PDF;
use DB;

class StandardsController extends ApiController
{
    /**
     * @var StandardForm
     */
    private $standardForm;

    /**
     * @var DefaultFromData
     */
    private $defaultFormData;

    /**
     * @var StandardStatement
     */
    private $standardStatement;

    /**
     * @var DefaultStatement
     */
    private $defaultStatement;

    /**
     * @var StandardScope
     */
    private $standard_scope;

    /**
     * @var DefaultScope
     */
    private $default_scope;

    /**
     * @var JWTAuth
     */
    private $jwtAuth;

    /**
     * @var StandardStatementsService
     */
    private $standardStatementsService;

    /**
     * StandardsController constructor.
     *
     * @param StandardForm $standardForm
     * @param DefaultFromData $defaultFromData
     * @param StandardStatement $standardStatement
     * @param DefaultStatement $defaultStatement
     * @param StandardScope $standard_scope
     * @param DefaultScope $default_scope
     * @param JWTAuth $jwtAuth
     * @param StandardStatementsService $standardStatementsService
     */
    public function __construct(
        StandardForm $standardForm,
        DefaultFromData $defaultFromData,
        StandardStatement $standardStatement,
        DefaultStatement $defaultStatement,
        StandardScope $standard_scope,
        DefaultScope $default_scope,
        JWTAuth $jwtAuth,
        StandardStatementsService $standardStatementsService
    )
    {
        $this->standardForm = $standardForm;
        $this->defaultFormData = $defaultFromData;
        $this->standardStatement = $standardStatement;
        $this->defaultStatement = $defaultStatement;
        $this->jwtAuth = $jwtAuth;
        $this->standard_scope = $standard_scope;
        $this->default_scope = $default_scope;
        $this->standardStatementsService = $standardStatementsService;
    }

    /**
     * @param StandardFormsIndex $request
     *
     * @return JsonResponse
     */
    public function index(StandardFormsIndex $request): JsonResponse
    {
        $forms = $this->standardForm->all();

        return $this->respond($forms);
    }

    /**
     * @param int $formId
     * @return JsonResponse
     */
    public function show(int $formId): JsonResponse
    {
        $form = $this->standardForm->where('form_id', $formId)->first();
        /** Workaround for insane flow */
        $statements = $this->standardStatement->where('form_id', $formId)->get();
        if ($statements->isEmpty() && auth()->user()->company) {
            $this->standardStatementsService->createDefaultStatements(auth()->user()->company);
        }
        $statements = $this->standardStatement->where('form_id', $formId)->get();

        if (!count($statements)) {
            $statements = $this->defaultStatement
                ->where('form_id', $formId)
                ->get()
                ->toArray();
            foreach ($statements as $defaultStatement) {
                $existing = $this->standardStatement->where([
                    'form_id' => $defaultStatement['form_id'],
                    'company_id' => auth()->user()->company_id,
                ])->first();
                if (!$existing) {
                    $this->standardStatement->create([
                        'form_id' => $defaultStatement['form_id'],
                        'company_id' => auth()->user()->company_id,
                        'title' => $defaultStatement['title'],
                        'statement' => $defaultStatement['statement'],
                    ]);
                }
            }
        }

        return $this->respond([
            'form' => $form,
            'statements' => $statements
        ]);
    }

    /**
     * @param StandardFormStore $request
     *
     * @return JsonResponse
     */
    public function store(StandardFormStore $request): JsonResponse
    {
        $form = $this->standardForm->create($request->validatedOnly());
        if ($request->has('statements')) {
            $statements = $request->get('statements');
            foreach ($statements as $key => $statement) {
                if (array_key_exists('id', $statement)) {
                    $standardStatement = $this->standardStatement->find($statement['id']);
                    $standardStatement->update($statement);
                } else {
                    $statement['company_id'] = auth()->user()->company_id;
                    $this->standardStatement->create($statement);
                }
            }
        }
        return $this->respond(['message' => 'Form successfully created', 'form' => $form]);
    }

    /**
     * @param StatementStore $request
     *
     * @return JsonResponse
     */
    public function statementStore(StatementStore $request): JsonResponse
    {
        $statement = $this->standardStatement->create([
            'form_id' => $request->get('form_id'),
            'company_id' => auth()->user()->company_id,
            'title' => $request->get('title'),
            'statement' => ''
        ]);

        return $this->respond(['message' => 'Statement successfully created', 'statement' => $statement]);
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function statementDelete(int $id): JsonResponse
    {
        $this->standardStatement->findOrFail($id)->delete();

        return $this->respond(['message' => 'Statement successfully deleted']);
    }

    /**
     * @param StandardFormUpdate $request
     *
     * @return JsonResponse
     */
    public function update(StandardFormUpdate $request): JsonResponse
    {
        $form = $this->standardForm->find($request->input('id'));
        $form->update($request->validatedOnly());
        if ($request->has('statements')) {
            $statements = $request->get('statements');
            foreach ($statements as $statement) {
                $standardStatement = $this->standardStatement->find($statement['id']);
                $standardStatement->update($statement);
            }
        }

        return $this->respond(['message' => 'Form successfully updated', 'form' => $form]);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $this->standardForm->findOrFail($id)->delete();

        return $this->respond(['message' => 'Form successfully deleted']);
    }
}
