<?php

namespace App\Http\Controllers\Api;

use App\Models\StandardForm;
use App\Models\StandardScope;
use App\Models\StandardStatement;
use App\Services\Forms\FakerService;
use App\Services\Forms\FormsPdfService;

class StandardFormsPreviewController
{
    /**
     * @var FakerService
     */
    private $fakerService;

    /**
     * @var StandardForm
     */
    private $standardForm;

    /**
     * @var StandardStatement
     */
    private $standardStatement;

    /**
     * @var FormsPdfService
     */
    private $formPdfService;

    /**
     * @var StandardScope
     */
    private $standardScope;

    /**
     * StandardFormsPreviewController constructor.
     * @param FakerService $fakerService
     * @param StandardForm $standardForm
     * @param StandardStatement $standardStatement
     * @param FormsPdfService $formsPdfService
     * @param StandardScope $standardScope
     */
    public function __construct(
        FakerService $fakerService,
        StandardForm $standardForm,
        StandardStatement $standardStatement,
        FormsPdfService $formsPdfService,
        StandardScope $standardScope
    )
    {
        $this->fakerService = $fakerService;
        $this->standardForm = $standardForm;
        $this->standardStatement = $standardStatement;
        $this->formPdfService = $formsPdfService;
        $this->standardScope = $standardScope;
    }

    /**
     * @param int $formId
     * @return mixed
     * @throws \Exception
     */
    public function preview(int $formId)
    {
        $project = $this->fakerService->getProject(auth()->user()->company);
        $standardForm = $this->standardForm->where('form_id', $formId)->firstOrFail();
        $standardForm->statements = $this->standardStatement->where('form_id', $formId)->get();
        $form = $this->fakerService->getProjectForm($standardForm);
        $form->project = $project;
        $form->project->fake = true;
        $scopes = $this->standardScope
            ->where('page', '!=', 0)
            ->orderBy('page')
            ->orderBy('sort_id')
            ->get()
            ->groupBy('page');
        $form->project->areas = $this->fakerService->getAreas($scopes);
        $form->project->miscScopes = $this->standardScope
            ->where('page', 0)
            ->orderBy('sort_id')
            ->get();

        return explode('public', $this->formPdfService->printProjectDocuments($project, collect([$form]), false)[0])[1];
    }
}
