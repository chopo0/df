<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Files\UpdateFileName;
use App\Models\Project;
use App\Services\Files\DropboxService;
use App\Services\Forms\FormsPdfService;
use Illuminate\Http\Request;

class ProjectFilesController extends ApiController
{
    const FILES_FOLDER = 'Images and Videos';

    /**
     * @var Project
     */
    private $project;

    /**
     * @var DropboxService
     */
    private $dropboxService;

    /**
     * @var FormsPdfService
     */
    private $formPdfService;

    /**
     * ProjectFilesController constructor.
     * @param Project $project
     * @param DropboxService $dropboxService
     * @param FormsPdfService $formsPdfService
     */
    public function __construct(Project $project, DropboxService $dropboxService, FormsPdfService $formsPdfService)
    {
        $this->project = $project;
        $this->dropboxService = $dropboxService;
        $this->formPdfService = $formsPdfService;
    }

    /**
     * @param int $projectId
     * @return array
     */
    public function show(int $projectId)
    {
        $project = $this->project->findOrFail($projectId);
        $folderName = $this->formPdfService->getCompleteDocumentTitle($project->call_report) . '/' . static::FILES_FOLDER;

        return $this->dropboxService->getFiles($project, $folderName);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $project = $this->project->findOrFail($request->get('project_id'));
        if (!$project->call_report->contact_name) {
            return $this->respondWithError('Set Contact Name on Call Report before uploading any files.', 422);
        }

        $folderName = $this->formPdfService->getCompleteDocumentTitle($project->call_report) . '/' . static::FILES_FOLDER;

        $this->dropboxService->directUpload($project, $request->file('file'), $folderName);

        return $this->respond(['message' => 'File successfully uploaded']);
    }

    /**
     * @param int $projectId
     * @param UpdateFileName $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(int $projectId, UpdateFileName $request)
    {
        $project = $this->project->findOrFail($projectId);
        $folderName = $this->formPdfService->getCompleteDocumentTitle($project->call_report) . '/' . static::FILES_FOLDER;
        $oldName = $folderName . '/' . $request->get('basename');
        $newName = $folderName . '/' . $request->get('newName') . '.' . $request->get('extension');

        return response()->json([
            'success' => $this->dropboxService->updateFilename($project, $oldName, $newName)
        ]);
    }
}
