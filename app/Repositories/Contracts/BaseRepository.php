<?php
namespace App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class BaseRepository implements Repository
{
    /**
     * @var Model
     */
    private $model;

    /**
     * BaseRepository constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $params
     * @param int $page
     * @param int $perPage
     * @param array|null $with
     * @return LengthAwarePaginator
     */
    public function getList(array $params, int $page, int $perPage, ?array $with = [])
    {
        $query = $this->model;

        foreach ($params as $param => $value) {
            $query->where($param, $value);
        }

        return $query->paginate($perPage);
    }
}
