<?php
namespace App\Repositories\Contracts;

use Illuminate\Pagination\LengthAwarePaginator;

interface Repository
{
    /**
     * @param array $params
     * @param int $page
     * @param int $perPage
     * @param array|null $with
     * @return LengthAwarePaginator
     */
    public function getList(array $params, int $page, int $perPage, ?array $with = []);
}
