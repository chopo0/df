<?php

namespace App\Services\Files;

use App\Models\Project;
use Illuminate\Http\UploadedFile;
use League\Flysystem\Filesystem;
use Spatie\Dropbox\Client;
use Spatie\FlysystemDropbox\DropboxAdapter;

class DropboxService
{
    /**
     * @param Project $project
     * @param string $path
     * @param string $folderName
     */
    public function upload(Project $project, string $path, string $folderName = null)
    {
        $title = array_last(explode('/', $path));
        $folder = $folderName ? $folderName : str_replace('.pdf', '', $title);
        $documentsPath = "DryForms/projects/{$folder}/{$title}";
        $client = new Client($project->company->dropbox_access_token);
        $adapter = new DropboxAdapter($client);
        $filesystem = new Filesystem($adapter);

        $filesystem->put($documentsPath, file_get_contents($path));
    }

    /**
     * @param Project $project
     * @param UploadedFile $file
     * @param string $folderName
     */
    public function directUpload(Project $project, UploadedFile $file, string $folderName)
    {
        $documentsPath = "DryForms/projects/{$folderName}/{$file->getClientOriginalName()}";
        $client = new Client($project->company->dropbox_access_token);
        $adapter = new DropboxAdapter($client);
        $filesystem = new Filesystem($adapter);

        $filesystem->put($documentsPath, file_get_contents($file->getPathname()));
    }

    /**
     * @param Project $project
     * @param string $oldName
     * @param string $newName
     * @return bool
     */
    public function updateFilename(Project $project, string $oldName, string $newName)
    {
        $path = $documentsPath = "DryForms/projects/";
        $client = new Client($project->company->dropbox_access_token);
        $adapter = new DropboxAdapter($client);

        return $adapter->rename("{$path}{$oldName}", "{$path}{$newName}");
    }

    /**
     * @param Project $project
     * @param string $folderName
     * @return array
     */
    public function getFiles(Project $project, string $folderName)
    {
        $path = $documentsPath = "DryForms/projects/{$folderName}";
        $client = new Client($project->company->dropbox_access_token);
        $adapter = new DropboxAdapter($client);
        $filesystem = new Filesystem($adapter);

        $data = [];
        $folderFiles = $filesystem->listContents($path);
        foreach ($folderFiles as $file) {
            $file['link'] = $adapter->getTemporaryLink($file['path']);
            $data[] = $file;
        }

        return $data;
    }

    /**
     * @param Project $project
     * @param string $folderName
     * @return string
     */
    public function getFolderSharedLink(Project $project, string $folderName)
    {
        $path = $documentsPath = "DryForms/projects/{$folderName}";
        $client = new Client($project->company->dropbox_access_token);

        if (!$project->shared_folder_url) {
            $sharedFolderInfo = $client->createSharedLinkWithSettings($path, ["requested_visibility" => "public"]);
            $project->shared_folder_url = $sharedFolderInfo['url'];
            $project->shared_folder_id = $sharedFolderInfo['id'];
            $project->save();
        }

        return $project->shared_folder_url;
    }

    /**
     * @param Project $project
     * @param array $paths
     * @param string $folderName
     */
    public function uploadMultiple(Project $project, array $paths, string $folderName)
    {
        foreach ($paths as $path) {
            $this->upload($project, $path, $folderName);
        }
    }
}
