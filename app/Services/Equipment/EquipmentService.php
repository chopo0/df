<?php

namespace App\Services\Equipment;

use App\Models\Equipment;
use App\Models\Project;
use App\Models\Status;
use Illuminate\Database\Connection;

class EquipmentService
{
    /**
     * @var Project
     */
    private $project;

    /**
     * @var Equipment
     */
    private $equipment;

    /**
     * @var Connection
     */
    private $db;

    /**
     * EquipmentService constructor.
     * @param Project $project
     * @param Equipment $equipment
     * @param Connection $db
     */
    public function __construct(Project $project, Equipment $equipment, Connection $db)
    {
        $this->project = $project;
        $this->equipment = $equipment;
        $this->db = $db;
    }

    public function changeStatus(Equipment $equipment, string $date, int $statusId, ?int $projectId, ?int $unsetProject)
    {
        if ($equipment->status_id === $statusId) {
            return $equipment;
        }
        switch ($statusId) {
            case Status::AVAILABLE:
                $this->pickUp($equipment, $unsetProject ?? $projectId, $date);
                break;
            case Status::OOC:
                break;
            case Status::SET:
                $this->removeFromOtherProjects($equipment, $projectId);
                $this->set($equipment, $projectId, $date);
                break;
            case Status::LOAN:
                break;
            case Status::MISSING:
                $this->miss($equipment, $unsetProject);
                break;
        }
    }

    /**
     * @param Equipment $equipment
     * @param int|null $currentProject
     */
    private function removeFromOtherProjects(Equipment $equipment, ?int $currentProject)
    {
        $equipment->location = null;
        $equipment->save();
        $this->db->table('projects_equipment')
            ->where('project_id', '!=', $currentProject)
            ->where('equipment_id', $equipment->id)->delete();
    }

    /**
     * @param Equipment $equipment
     * @param int $projectId
     * @param string $date
     */
    private function set(Equipment $equipment, ?int $projectId, string $date)
    {
        $project = $this->project->find($projectId);
        $equipment->location = null;
        if ($project) {
            $equipment->location = "{$project->project_call_report->insured_name} {$project->project_call_report->full_job_address}";
        }
        $equipment->status_id = Status::SET;
        $equipment->save();
        if ($project) {
            $project->project_equipment()->attach($equipment, ['set_up_date' => $date]);
        }
    }

    /**
     * @param Equipment $equipment
     * @param int $projectId
     * @param string $date
     */
    private function pickUp(Equipment $equipment, ?int $projectId, string $date)
    {
        $project = $this->project->find($projectId);
        $equipment->location = null;
        $equipment->status_id = Status::AVAILABLE;
        $equipment->save();
        if ($project) {
            $project->project_equipment()->updateExistingPivot($equipment->id, ['pick_up_date' => $date]);
        }
    }

    /**
     * @param Equipment $equipment
     * @param int $projectId
     */
    private function miss(Equipment $equipment, ?int $projectId)
    {
        if ($projectId) {
            $project = $this->project->find($projectId);
            $equipment->location = "{$project->project_call_report->insured_name} {$project->project_call_report->full_job_address}";
        }
        $equipment->status_id = Status::MISSING;
        $equipment->save();
    }
}
