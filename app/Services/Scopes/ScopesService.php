<?php
namespace App\Services\Scopes;

use App\Models\DefaultScope;
use App\Models\Project;
use App\Models\ProjectArea;
use App\Models\ProjectScope;
use App\Models\StandardScope;
use Illuminate\Support\Collection;

class ScopesService
{
    /**
     * @var DefaultScope
     */
    private $defaultScope;

    /**
     * @var StandardScope
     */
    private $standardScope;

    /**
     * @var ProjectScope
     */
    private $projectScope;

    /**
     * ScopesService constructor.
     * @param DefaultScope $defaultScope
     * @param StandardScope $standardScope
     * @param ProjectScope $projectScope
     */
    public function __construct(DefaultScope $defaultScope, StandardScope $standardScope, ProjectScope $projectScope)
    {
        $this->defaultScope = $defaultScope;
        $this->standardScope = $standardScope;
        $this->projectScope = $projectScope;
    }

    /**
     * @param int $companyId
     */
    public function setStandardScopes(int $companyId)
    {
        $defaultScopes = $this->defaultScope->orderBy('sort_id')->get();
        foreach ($defaultScopes as $scope) {
            $this->standardScope->create([
                'company_id' => $companyId,
                'selected' => $scope->selected ?? false,
                'service' => $scope->service,
                'header' => $scope->header,
                'quantity' => $scope->quantity,
                'uom' => $scope->uom,
                'page' => $scope->page,
                'sort_id' => $scope->sort_id
            ]);
        }
    }

    /**
     * @param Project $project
     */
    public function setProjectScopes(Project $project)
    {
        /** @var Collection $standardScopes */
        $standardScopes = $this->standardScope->where('page', '!=', 0)->orderBy('sort_id')->get();
        $areas = $project->areas;
        foreach ($areas as $area) {
            $existingScopes = $this->projectScope->where([
                'project_id' => $project->id,
                'project_area_id' => $area->id
            ])->count();
            if (!$existingScopes) {
                $this->createProjectScopesForArea($standardScopes, $project->id, $area->id);
            }
        }
    }

    /**
     * @param ProjectArea $area
     */
    public function deleteProjectScopesForArea(ProjectArea $area)
    {
        $this->projectScope->where([
            'project_id' => $area->project->id,
            'project_area_id' => $area->id
        ])->delete();
    }

    /**
     * @param int $projectId
     */
    public function createProjectMiscPage(int $projectId)
    {
        $standardScopes = $this->standardScope->where('page', 0)->orderBy('sort_id')->get();
        foreach ($standardScopes as $standardScope) {
            $this->projectScope->create([
                'project_id' => $projectId,
                'company_id' => $standardScope->company_id,
                'selected' => $standardScope->selected,
                'service' => $standardScope->service,
                'header' => $standardScope->header,
                'quantity' => $standardScope->quantity,
                'uom' => $standardScope->uom,
                'page' => $standardScope->page,
                'sort_id' => $standardScope->sort_id,
                'uom_from_standard' => $standardScope->uom ? true : false,
                'service_from_standard' => $standardScope->service ? true : false,
            ]);
        }
    }

    /**
     * @param Collection $scopes
     * @param int $projectId
     * @param int $areaId
     */
    private function createProjectScopesForArea(Collection $scopes, int $projectId, int $areaId)
    {
        foreach ($scopes as $standardScope) {
            $this->projectScope->create([
                'project_id' => $projectId,
                'company_id' => $standardScope->company_id,
                'project_area_id' => $areaId,
                'selected' => $standardScope->selected,
                'service' => $standardScope->service,
                'header' => $standardScope->header,
                'quantity' => $standardScope->quantity,
                'uom' => $standardScope->uom,
                'page' => $standardScope->page,
                'uom_from_standard' => $standardScope->uom ? true : false,
                'service_from_standard' => $standardScope->service ? true : false,
                'sort_id' => $standardScope->sort_id
            ]);
        }
    }
}
