<?php

namespace App\Services\Forms;

use App\Models\Project;
use App\Models\ProjectCallReport;
use App\Models\ProjectForm;
use App\Models\ProjectMoistureForm;
use App\Models\ProjectPsychometricDays;
use App\Services\Psychometric\PsychometricService;
use Barryvdh\Snappy\PdfWrapper;
use Illuminate\Support\Collection;
use LynX39\LaraPdfMerger\PdfManage;
use Illuminate\Filesystem\FilesystemManager as Filesystem;

class FormsPdfService
{
    /**
     * @var PdfWrapper
     */
    private $pdf;

    /**
     * @var Project
     */
    private $project;

    /**
     * @var PsychometricService
     */
    private $psychometricService;

    /**
     * @var ScopeFormService
     */
    private $scopeFormService;

    /**
     * @var ProjectForm[]
     */
    private $forms;

    /**
     * @var string
     */
    private $baseStoragePath;

    /**
     * @var DailyLogFormService
     */
    private $dailyLogFormService;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * FormsPdfService constructor.
     * @param PdfWrapper $pdf
     * @param PsychometricService $psychometricService
     * @param ScopeFormService $scopeFormService
     * @param DailyLogFormService $dailyLogFormService
     * @param Filesystem $filesystem
     */
    public function __construct(
        PdfWrapper $pdf,
        PsychometricService $psychometricService,
        ScopeFormService $scopeFormService,
        DailyLogFormService $dailyLogFormService,
        Filesystem $filesystem
    ) {
        $this->pdf = $pdf;
        $this->psychometricService = $psychometricService;
        $this->scopeFormService = $scopeFormService;
        $this->dailyLogFormService = $dailyLogFormService;
        $this->filesystem = $filesystem;
    }

    /**
     * @param Project $project
     * @param Collection $requestedForms
     * @param bool $asFile
     * @param bool $multiple
     * @param bool $download
     * @return array|\LynX39\LaraPdfMerger\PDF
     * @throws \Exception
     */
    public function printProjectDocuments(Project $project, Collection $requestedForms, bool $asFile = true, bool $multiple = false, bool $download = false)
    {
        $this->project = $project;
        $this->forms = $requestedForms;
        $this->baseStoragePath = "documents/pdf/{$project->company_id}";
        $this->filesystem->disk('documents')->deleteDirectory("$this->baseStoragePath/$project->id");
        $pages = $this->createPdfForms();

        $pdf = new PdfManage();
        foreach ($pages as $page) {
            if ($page) {
                $pdf->addPDF($page);
            }
        }

        if (!$asFile) {
            if ($multiple) {
                return $pages;
            } else {
                $pdf->merge('file', public_path("{$this->baseStoragePath}/{$this->project->id}/{$this->getCompleteDocumentTitle($this->project->call_report)}.pdf"));

                return [public_path("{$this->baseStoragePath}/{$this->project->id}/{$this->getCompleteDocumentTitle($this->project->call_report)}.pdf")];
            }
        }

        if ($download) {

            $pdf->merge('download', "{$this->getCompleteDocumentTitle($this->project->call_report)}.pdf");
        }

        return $pdf->merge('browser', "{$this->getCompleteDocumentTitle($this->project->call_report)}.pdf");
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function createPdfForms(): array
    {
        $sections = [];

        foreach ($this->forms as $form) {
            $slug = $this->getFormSlug($form->default_form_data->title ? $form->default_form_data->title : $form->standard_form->title);
            $sections[] = $this->getFormPdf($slug, $form);
        }

        return $sections;
    }

    /**
     * @param ProjectForm|null $form
     * @param bool $withoutFooter
     * @return array
     */
    private function getOptions(?ProjectForm $form, ?bool $withoutFooter = false)
    {
        $marginBottom = $this->footerMarginShouldBeAdded($form, $withoutFooter) ? '27mm' : '23mm';
        if ($form && $form->form_id === 2) {
            $marginBottom = '10mm';
        }

        return [
            'header-html' => $this->getHeader($form),
            'footer-html' => $withoutFooter ? null : $this->getFooter($form),
            'margin-bottom' => $marginBottom,
            'margin-top' => $form ? '70mm' : '40mm',
            'margin-left' => '15mm',
            'margin-right' => '15mm',
            'enable-external-links' => true,
            'enable-javascript' => true,
        ];
    }

    /**
     * @param string $slug
     * @param ProjectForm $form
     * @return string
     * @throws \Exception
     */
    private function getFormPdf(string $slug, ProjectForm $form)
    {
        switch ($slug) {
            case 'call_report':
                return $this->generateCallReportPdf($this->project->call_report);
                break;
            case 'work_authorization':
            case 'anti_microbial':
            case 'release_from_liability':
            case 'work_stoppage':
            case 'certificate_of_completion':
                return $this->generateGenericFormPdf($form);
                break;
            case 'moisture_map':
                return $this->generateMoistureFormPdf($this->project->moisture_form, $form);
                break;
            case 'psychometric_report':
                return $this->generatePsychometricFormPdf($form);
                break;
            case 'project_scope':
                return $this->generateProjectScopePdf($form);
                break;
            case 'customer_responsibility':
                return $this->generateCustomerResponsibilityFormPdf($form);
                break;
            case 'daily_log':
                return $this->generateDailyLogPdf($form);
                break;
            default:
                return $this->generateGenericFormPdf($form);
                break;
        }
    }

    /**
     * @param ProjectCallReport $callReport
     * @return string
     */
    private function generateCallReportPdf(ProjectCallReport $callReport): string
    {
        $path = "$this->baseStoragePath/{$this->project->id}/Call Report.pdf";

        $this->pdf->loadView('pdf.call_report', [
            'title' => 'Call Report',
            'callReport' => $callReport
        ])->setOptions($this->getOptions(null, false))
            ->setPaper('letter');

        $this->pdf->save($path, true);

        return $path;
    }

    /**
     * @param ProjectForm $form
     * @return string
     * @throws \Exception
     */
    private function generateGenericFormPdf(ProjectForm $form): string
    {
        $path = "{$this->baseStoragePath}/{$this->project->id}/{$form->title}.pdf";
        $pdfWithoutFooterPath = "{$this->baseStoragePath}/{$this->project->id}/{$form->title}_no_footer.pdf";

        $this->pdf->loadView('pdf.generic_form', [
            'title' => $form->title,
            'form' => $form,
            'callReport' => $this->project->call_report
        ])->setOptions($this->getOptions($form, false))
            ->setPaper('letter');

        $this->pdf->save($path, true);

        $pagesCount = $this->getPDFPages($path);

        if ($pagesCount > 1) {
            $this->mergePdfFilesForFooterFix($form, $path, $pdfWithoutFooterPath, $pagesCount, 'pdf.generic_form');
        }

        return $path;
    }

    /**
     * @param ProjectMoistureForm $form
     * @param ProjectForm $projectForm
     * @return string
     */
    private function generateMoistureFormPdf(ProjectMoistureForm $form, ProjectForm $projectForm): string
    {
        $path = "{$this->baseStoragePath}/{$this->project->id}/{$projectForm->title}.pdf";

        $this->pdf->loadView('pdf.moisture_form', [
            'title' => $projectForm->title,
            'form' => $form,
            'callReport' => $this->project->call_report
        ])->setOptions($this->getOptions($projectForm, false))
            ->setPaper('letter');

        $this->pdf->save($path, true);

        return $path;
    }

    /**
     * @param ProjectForm $projectForm
     * @return string
     */
    private function generateProjectScopePdf(ProjectForm $projectForm)
    {
        $path = "{$this->baseStoragePath}/{$this->project->id}/{$projectForm->title}.pdf";

        foreach ($projectForm->project->areas as $area) {
            if (!$area->fake) {
                $area->scopes = $this->scopeFormService->getAreaScopes($this->project->id, $area->id);
            }
        }

        $miscScopes = $this->scopeFormService->getMiscScopes($this->project->id);

        if ($projectForm->project->fake) {
            $miscScopes = $projectForm->project->miscScopes;
        }

        $this->pdf->loadView('pdf.project_scope', [
            'title' => $projectForm->title,
            'form' => $projectForm,
            'callReport' => $this->project->call_report,
            'miscScopes' => $miscScopes
        ])->setOptions($this->getOptions($projectForm, false))
            ->setPaper('letter');

        $this->pdf->save($path, true);

        return $path;
    }

    /**
     * @param ProjectForm $form
     * @return string
     */
    private function generatePsychometricFormPdf(ProjectForm $form): string
    {
        $path = "{$this->baseStoragePath}/{$this->project->id}/{$form->title}.pdf";
        $form->measurements = $this->psychometricService->prepareMeasurements($this->project, new ProjectPsychometricDays());

        $this->pdf->loadView('pdf.psychometric_form', [
            'title' => $form->title,
            'form' => $form,
            'callReport' => $this->project->call_report
        ])->setOptions($this->getOptions($form, false))
            ->setPaper('letter');

        $this->pdf->save($path, true);

        return $path;
    }

    /**
     * @param ProjectForm $form
     * @return string
     * @throws \Exception
     */
    private function generateCustomerResponsibilityFormPdf(ProjectForm $form): string
    {
        $path = "{$this->baseStoragePath}/{$this->project->id}/{$form->title}.pdf";
        $pdfWithoutFooterPath = "{$this->baseStoragePath}/{$this->project->id}/{$form->title}_no_footer.pdf";

        $this->pdf->loadView('pdf.responsibility_form', [
            'title' => $form->title,
            'form' => $form,
            'callReport' => $this->project->call_report
        ])->setOptions($this->getOptions($form, false))
            ->setPaper('letter');

        $this->pdf->save($path, true);

        $pagesCount = $this->getPDFPages($path);

        if ($pagesCount > 1) {
            $this->mergePdfFilesForFooterFix($form, $path, $pdfWithoutFooterPath, $pagesCount, 'pdf.responsibility_form');
        }

        return $path;
    }

    /**
     * @param ProjectForm $form
     * @return string
     */
    private function generateDailyLogPdf(ProjectForm $form): string
    {
        $path = "{$this->baseStoragePath}/{$this->project->id}/{$form->title}.pdf";
        $logs = $this->dailyLogFormService->getLogs($form->project_id);

        $this->pdf->loadView('pdf.daily_log', [
            'title' => $form->title,
            'form' => $form,
            'logs' => $logs,
            'callReport' => $this->project->call_report
        ])->setOptions($this->getOptions($form, false))
            ->setPaper('letter');

        $this->pdf->save($path, true);

        return $path;
    }

    /**
     * @param ProjectForm|null $form
     * @return string
     */
    private function getHeader(?ProjectForm $form)
    {
        return view()->make('pdf.layout._header', [
            'company' => $this->project->company_details,
            'form' => $form,
            'title' => $form ? $form->title : null,
            'callReport' => $this->project->call_report
        ])->render();
    }

    /**
     * @param ProjectForm|null $form
     * @return string
     */
    private function getFooter(?ProjectForm $form)
    {
        return view()->make('pdf.layout._footer', ['form' => $form, 'callReport' => $this->project->call_report])->render();
    }

    /**
     * @param string $title
     * @return string
     */
    private function getFormSlug(string $title): string
    {
        return snake_case(str_replace('-', ' ', $title));
    }

    /**
     * @param ProjectCallReport $callReport
     * @return string
     */
    public function getCompleteDocumentTitle(ProjectCallReport $callReport): string
    {
        $name = $callReport->contact_name ? "$callReport->contact_name" : '';
        $address = $callReport->job_address? " - $callReport->job_address" : '';
        $apartment = $callReport->apartment_no ? ", Unit # $callReport->apartment_no" : '';

        $title = "{$name}{$address}{$apartment}";
        if (!$name && !$address && !$apartment) {
            $title = 'Documents';
        }

        return $title;
    }

    /**
     * @param ProjectForm|null $form
     * @param bool $withoutFooter
     * @return bool
     */
    private function footerMarginShouldBeAdded(?ProjectForm $form, ?bool $withoutFooter = false): bool
    {
        if ($withoutFooter) {
            return false;
        }

        return $form && ($form->footer_text_show || $form->insured_signature || $form->company_signature);
    }

    /**
     * This function will create additional PDF file without footer
     * then it will merge both footer and no-footer files and
     * save one merged file where footer will be only on the last page
     * @param ProjectForm $form
     * @param string $path
     * @param string $pdfWithoutFooterPath
     * @param int $pagesCount
     * @param string $view
     * @throws \Exception
     */
    private function mergePdfFilesForFooterFix(ProjectForm $form, string $path, string $pdfWithoutFooterPath, int $pagesCount, string $view)
    {
        $this->pdf->loadView($view, [
            'title' => $form->title,
            'form' => $form,
            'callReport' => $this->project->call_report
        ])->setOptions($this->getOptions($form, true))
            ->setPaper('letter');
        $this->pdf->save($pdfWithoutFooterPath, true);

        $pdf = new PdfManage();
        $rangeTo = $pagesCount - 1;
        $pdf->addPDF($pdfWithoutFooterPath, "1-{$rangeTo}");
        $pdf->addPDF($path, "{$pagesCount}");

        $pdf->merge('file', public_path($path));
    }

    /**
     * Source - https://stackoverflow.com/questions/14644353/get-the-number-of-pages-in-a-pdf-document
     * @param string $document
     * @return int
     */
    private function getPDFPages(string $document): int
    {
        $cmd = env('PDFINFO_PATH');

        // Parse entire output
        // Surround with double quotes if file name has spaces
        exec("$cmd \"$document\"", $output);

        // Iterate through lines
        $pagesCount = 0;
        foreach($output as $op) {
            // Extract the number
            if(preg_match("/Pages:\s*(\d+)/i", $op, $matches) === 1) {
                $pagesCount = intval($matches[1]);
                break;
            }
        }

        return $pagesCount;
    }
}
