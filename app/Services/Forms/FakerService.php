<?php

namespace App\Services\Forms;

use App\Models\Area;
use App\Models\Category;
use App\Models\Company;
use App\Models\Equipment;
use App\Models\EquipmentModel;
use App\Models\Project;
use App\Models\ProjectArea;
use App\Models\ProjectCallReport;
use App\Models\ProjectCompanyDetails;
use App\Models\ProjectForm;
use App\Models\ProjectStatus;
use App\Models\StandardForm;
use Faker\Generator as Faker;
use Illuminate\Support\Collection;

class FakerService
{
    /**
     * @var Faker
     */
    private $faker;

    /**
     * FakerService constructor.
     * @param Faker $faker
     */
    public function __construct(Faker $faker)
    {
        $this->faker = $faker;
    }

    /**
     * @param Company $company
     * @return Project
     */
    public function getProject(Company $company): Project
    {
        $project = new Project();

        $project->id = 1;
        $project->company_id = $company->id;
        $project->owner_name = "{$this->faker->firstName} {$this->faker->lastName}";
        $project->assigned_to = null;
        $project->address = $this->faker->address;
        $project->phone = $this->faker->phoneNumber;
        $project->status = ProjectStatus::IN_PROGRESS;
        $project->fake = true;

        $companyDetails = new ProjectCompanyDetails();
        $companyDetails->user_id = $company->user_id;
        $companyDetails->logo = $company->logo;
        $companyDetails->name = $company->name;
        $companyDetails->email = $company->email;
        $companyDetails->street = $company->street;
        $companyDetails->city = $company->city;
        $companyDetails->state = $company->state;
        $companyDetails->zip = $company->zip;
        $companyDetails->phone = $company->phone;
        $companyDetails->project_id = $project->id;

        $project->company_details = $companyDetails;
        $project->instrument = null;
        $project->equipment = $this->getEquipment();
        $project->call_report = $this->getCallReport($project);

        return $project;
    }

    /**
     * @param StandardForm $standardForm
     * @return ProjectForm
     */
    public function getProjectForm(StandardForm $standardForm): ProjectForm
    {
        $projectForm = new ProjectForm();
        $projectForm->company_id = $standardForm->company_id;
        $projectForm->form_id = $standardForm->form_id;
        $projectForm->project_id = 1;
        $projectForm->title = $standardForm->title;
        $projectForm->name = $standardForm->name;
        $projectForm->sort_id = 1;
        $projectForm->additional_notes_show = $standardForm->additional_notes_show;
        $projectForm->footer_text_show = $standardForm->footer_text_show;
        $projectForm->footer_text = $standardForm->footer_text;
        $projectForm->standard_form = $standardForm;
        $projectForm->fakeStatements = $standardForm->statements;

        return $projectForm;
    }

    /**
     * @param Collection $standardScopes
     * @return Collection
     */
    public function getAreas(Collection $standardScopes)
    {
        $area = new ProjectArea();
        $standardArea = new Area();
        $standardArea->title = 'Main Area';
        $area->standard_area = $standardArea;
        $area->overal_square_feet = 800;
        $area->id = 1;
        $area->fake = true;
        $area->scopes = $standardScopes;

        return collect([$area]);
    }

    /**
     * @param Project $project
     * @return ProjectCallReport
     */
    private function getCallReport(Project $project): ProjectCallReport
    {
        $callReport = new ProjectCallReport();
        $callReport->job_address = "{$this->faker->streetName}, {$this->faker->city} 25";
        $callReport->insured_name = "{$this->faker->firstName} {$this->faker->lastName}";
        $callReport->insurance_claim_no = $this->faker->buildingNumber;
        $callReport->project = $project;

        return $callReport;
    }

    /**
     * @return Collection
     */
    private function getEquipment(): Collection
    {
        $equipment = new Equipment();
        $equipment->model = new EquipmentModel();
        $equipment->model->category = new Category();
        $equipment->model->category->name = $this->faker->word;
        $equipment->model->name = $this->faker->word;
        $equipment->serial = $this->faker->swiftBicNumber;
        $equipment->team = null;
        $equipment->updated_at = $this->faker->dateTime;
        $equipment->status = null;

        $equipment2 = new Equipment();
        $equipment2->model = new EquipmentModel();
        $equipment2->model->category = new Category();
        $equipment2->model->category->name = $this->faker->word;
        $equipment2->model->name = $this->faker->word;
        $equipment2->serial = $this->faker->swiftBicNumber;
        $equipment2->team = null;
        $equipment2->updated_at = $this->faker->dateTime;
        $equipment2->status = null;

        $equipment3 = new Equipment();
        $equipment3->model = new EquipmentModel();
        $equipment3->model->category = new Category();
        $equipment3->model->category->name = $this->faker->word;
        $equipment3->model->name = $this->faker->word;
        $equipment3->serial = $this->faker->swiftBicNumber;
        $equipment3->team = null;
        $equipment3->updated_at = $this->faker->dateTime;
        $equipment3->status = null;

        return collect([$equipment, $equipment2, $equipment3]);
    }
}
