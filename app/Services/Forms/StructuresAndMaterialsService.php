<?php

namespace App\Services\Forms;

use App\Models\DefaultMaterial;
use App\Models\DefaultStructure;
use App\Models\StandardMaterial;
use App\Models\StandardStructure;

class StructuresAndMaterialsService
{
    /**
     * @var StandardStructure
     */
    private $standardStructure;

    /**
     * @var DefaultStructure
     */
    private $defaultStructure;

    /**
     * @var StandardMaterial
     */
    private $standardMaterial;

    /**
     * @var DefaultMaterial
     */
    private $defaultMaterial;

    /**
     * StructuresAndMaterialsService constructor.
     * @param StandardStructure $standardStructure
     * @param DefaultStructure $defaultStructure
     * @param StandardMaterial $standardMaterial
     * @param DefaultMaterial $defaultMaterial
     */
    public function __construct(
        StandardStructure $standardStructure,
        DefaultStructure $defaultStructure,
        StandardMaterial $standardMaterial,
        DefaultMaterial $defaultMaterial
    )
    {
        $this->standardStructure = $standardStructure;
        $this->defaultStructure = $defaultStructure;
        $this->standardMaterial = $standardMaterial;
        $this->defaultMaterial = $defaultMaterial;
    }

    /**
     * @param int $companyId
     */
    public function setDefaultStructures(int $companyId)
    {
        $defaultStructures = $this->defaultStructure->get();
        foreach ($defaultStructures as $defaultStructure) {
            $this->standardStructure->create([
                'title' => $defaultStructure->title,
                'company_id' => $companyId
            ]);
        }
    }

    /**
     * @param int $companyId
     */
    public function setDefaultMaterials(int $companyId)
    {
        $defaultMaterials = $this->defaultMaterial->get();
        foreach ($defaultMaterials as $defaultMaterial) {
            $this->standardMaterial->create([
                'title' => $defaultMaterial->title,
                'company_id' => $companyId
            ]);
        }
    }
}
