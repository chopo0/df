<?php
namespace App\Services\Forms;

use App\Models\ProjectScope;

class ScopeFormService
{
    /**
     * @var ProjectScope
     */
    private $scope;

    /**
     * ScopeFormService constructor.
     * @param ProjectScope $scope
     */
    public function __construct(ProjectScope $scope)
    {
        $this->scope = $scope;
    }

    /**
     * @param int $projectId
     * @param int $areaId
     * @return mixed
     */
    public function getAreaScopes(int $projectId, int $areaId)
    {
        return $this->scope->where([
            'project_id' => $projectId,
            'project_area_id' => $areaId
        ])->orderBy('page')->orderBy('sort_id')->get()->groupBy('page');
    }

    /**
     * @param int $projectId
     * @return mixed
     */
    public function getMiscScopes(int $projectId)
    {
        return $this->scope->where([
            'project_id' => $projectId,
            'project_area_id' => null
        ])->orderBy('sort_id')->get();
    }
}
