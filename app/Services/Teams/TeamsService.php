<?php
namespace App\Services\Teams;

use App\Models\DefaultTeam;
use App\Models\Team;

class TeamsService
{
    /**
     * @var Team
     */
    private $team;

    /**
     * @var DefaultTeam
     */
    private $defaultTeam;

    /**
     * TeamsService constructor.
     * @param Team $team
     * @param DefaultTeam $defaultTeam
     */
    public function __construct(Team $team, DefaultTeam $defaultTeam)
    {
        $this->team = $team;
        $this->defaultTeam = $defaultTeam;
    }

    /**
     * @param int $companyId
     */
    public function setDefaultTeams(int $companyId)
    {
        $teams = $this->defaultTeam->get();
        foreach ($teams as $team) {
            $this->team->create([
                'name' => $team->name,
                'company_id' => $companyId
            ]);
        }
    }
}
