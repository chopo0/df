<?php

namespace App\Services\QueryBuilders;

use App\Models\ProjectStatus;
use App\Services\QueryBuilder;
use Illuminate\Database\Eloquent\Builder;

class ProjectModelQueryBuilder extends QueryBuilder
{
    /**
     * @param array $params
     *
     * @return Builder
     */
    public function setQueryParams(array $params): Builder
    {
        $this->query->byUser();
        if (isset($params['status'])) {
            $this->query->where('status', $params['status']);
        } else {
            $this->query->where('status', '<>', ProjectStatus::DELETED);
        }

        if (isset($params['assigned_to'])) {
            $this->query->where('assigned_to', $params['assigned_to']);
        }

        if (isset($params['filter'])) {
            $filter = $params['filter'];
            $this->query->where(function($query) use ($filter) {
                $query
                    ->where('owner_name', 'like', "%$filter%%")
                    ->orWhereHas('assignee', function($query) use ($filter) {
                        $query->where('name', 'like', "%$filter%");
                    })
                    ->orWhere('address', 'like', "%$filter%%")
                    ->orWhere('phone', 'like', "%$filter%%");
                });
        }
        if (isset($params['year'])) {
             $this->query->where('created_at', 'like', "%{$params['year']}%");
        }
        $this->query->orderBy('id', 'desc');

        return $this->query;
    }
}
