<?php

namespace App\Services\Marketing;

class BannerService
{

    /**
     * @param string $mainTitle
     * @param string $subTitle
     * @param string $bannerSrc
     * 
     * @return array
     */
    public static function setDetails($mainTitle, $subTitle = null, $bannerSrc = null)
    {
        return compact('mainTitle', 'subTitle', 'bannerSrc');
    }
}