<?php

namespace App\Services\Marketing;

use Faker\Generator as Faker;

class FakerService
{
    /**
     * @var Faker
     */
    private $faker;

    /**
     * FakerService constructor.
     * @param Faker $faker
     */
    public function __construct(Faker $faker)
    {
        $this->faker = $faker;
    }

    /**
     * @return array
     */
    public function getArticles()
    {

        $list = [];

        for ($i = 0; $i < 25; $i++) {
            $article = [];
            $article['title'] = $this->faker->company();
            $article['sub_title'] = $this->faker->sentence(10, true);
            $article['slug'] = str_slug($article['title']);
            $article['cover'] = $this->faker->imageUrl($width = 960, $height = 720);
            $article['thumbnail'] = $this->faker->imageUrl($width = 320, $height = 240);
            $article['filename'] = $article['thumbnail'];
            $article['article'] = '<p>' . $this->faker->paragraphs(5, true) . '</p>';
            $article['published_on'] = "{$this->faker->year('now')}-{$this->faker->month('now')}-{$this->faker->dayOfMonth(28)}";
            $article['published'] = $this->faker->boolean();

            $list[] = $article;
        }

        return $list;
    }
}
