<?php

namespace App\Services\Marketing;

use App\Models\Marketing\MarketingSetting;


class MarketingSettingService {
  /**
   * @var MarketingSetting
   */
  private $marketingSetting;

  /**
   * MarketingSettingService constructor
   * 
   * @param MarketingSetting $marketingSetting
   */
  public function __construct(MarketingSetting $marketingSetting) {
    $this->marketingSetting = $marketingSetting;
  }

  /**
   * @return MarketingSetting|\stdClass
   */
  public function getSetting() {
    $setting = $this->marketingSetting->first();

    if(!$setting) {
      $setting = new \stdClass();
      $setting->notification_receiver = ADMIN_EMAIL;
      $setting->package_price = PACKAGE_PRICE;
      $setting->intro_video = INTRO_VIDEO;
    }

    return $setting;
  }
}