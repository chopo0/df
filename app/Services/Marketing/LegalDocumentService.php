<?php

namespace App\Services\Marketing;

use App\Models\Marketing\LegalDocument;
use App\Providers\Marketing\LegalDocumentProvider;


class LegalDocumentService
{
    /**
     * @var LegalDocument
     */
    private $legalDocument;

    /**
     * @var LegalDocumentProvider
     */
    private $provider;

    /**
     * LegalDocument constructor
     */
    public function __construct(
        LegalDocument $legalDocument,
        LegalDocumentProvider $provider
    ) {
        $this->legalDocument = $legalDocument;
        $this->provider = $provider;
    }

    public function getType($type)
    {
        switch ($type) {
            case PRIVACY_POLICY_SLUG:
            case PRIVACY_POLICY:
                return PRIVACY_POLICY_ABBREV;
            case TERMS_AND_CONDITIONS_SLUG:
            case TERMS_AND_CONDITIONS:
                return TERMS_AND_CONDITIONS_ABBREV;
            default:
                return null;
        }
    }

    public function toSlug($type)
    {
        switch ($type) {
            case PRIVACY_POLICY_ABBREV:
                return PRIVACY_POLICY_SLUG;
            case TERMS_AND_CONDITIONS_ABBREV:
                return TERMS_AND_CONDITIONS_SLUG;
            default:
                return null;
        }
    }

    public function getTitle($type)
    {
        switch ($type) {
            case PRIVACY_POLICY_SLUG:
            case PRIVACY_POLICY_ABBREV:
                return PRIVACY_POLICY;
            case TERMS_AND_CONDITIONS_SLUG:
            case TERMS_AND_CONDITIONS_ABBREV:
                return TERMS_AND_CONDITIONS;
            default:
                return null;
        }
    }

    /**
     * Gets document based on type; creates one if found none.
     * 
     * @param string $type
     * @return LegalDocument
     */
    public function getDocument($type)
    {
        $document = $this->legalDocument
            ->where('type', $type)
            ->orderBy('updated_at', 'desc')
            ->first();

        if (empty($document)) {
            $document = $this->create($type);
        }

        $document->title = $this->getTitle($type);

        return $document;
    }

    /**
     * Create a new LegalDocument
     * 
     * @param string $type
     * @return LegalDocument
     */
    public function create($type)
    {
        $document = new LegalDocument();
        $document->type = $type;
        $document->content = $this->provider->getDefaultContent($type);
        $document->save();

        return $document;
    }
}