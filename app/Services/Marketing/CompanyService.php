<?php

namespace App\Services\Marketing;

use GuzzleHttp\Client;
use function GuzzleHttp\json_decode;

class CompanyService
{
    private $client;

    /**
     * CompanyService constructor
     * 
     * @param Client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return array
     */
    public function getLogos()
    {
        $response = $this->client->get(COMPANY_LOGOS_API_URL);

        return json_decode($response->getBody()->getContents());
    }

    /**
     * Split logos into chunks of 4s
     * 
     * @return array
     */
    public function getLogosByFours()
    {
        $logos = $this->getLogos();
        return array_chunk($logos, 4);
    }
}