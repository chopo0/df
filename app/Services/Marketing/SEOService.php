<?php

namespace App\Services\Marketing;

use SEOMeta;
use OpenGraph;
use Carbon\Carbon;

class SEOService
{
    private $keywords = ['dry', 'forms', 'dryforms', MKTG_SHORT_NAME, 'restoration', 'contractors', 'document', 'equipment'];
    public function generateRegularMetaData($pageTitle = null)
    {
        $title = MKTG_SITE_NAME;
        $tagLine = MKTG_SITE_TAGLINE;
        $pageTitle = $pageTitle ?? $title;

        SEOMeta::setTitle($pageTitle);
        SEOMeta::setDescription($tagLine);
        SEOMeta::addKeyword($this->keywords);

        OpenGraph::setTitle($title);
        OpenGraph::setDescription($tagLine);
        OpenGraph::setUrl(FACEBOOK_LINK);
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addImage(APP_FAVICON);
        OpenGraph::addImage(APP_LOGO);
    }

    public function generateArticleMetaData($post)
    {
        $title = MKTG_SITE_NAME;
        $tagLine = MKTG_SITE_TAGLINE;

        $post->published_on = Carbon::createFromFormat('Y-m-d H:i:s.u', $post->updated_at . '.0');

        SEOMeta::setTitle($post->title);
        SEOMeta::setDescription($post->sub_title);
        SEOMeta::addMeta('article:title', $post->title, 'property');
        SEOMeta::addMeta('article:description', $post->sub_title, 'property');
        SEOMeta::addMeta('article:published_time', $post->updated_at->toW3CString(), 'property');
        SEOMeta::addKeyword($this->keywords);

        OpenGraph::setTitle($post->title);
        OpenGraph::setDescription($post->sub_title);
        OpenGraph::setUrl(url()->current());
        OpenGraph::addProperty('type', 'article');
        OpenGraph::addProperty('locale', 'en-us');

        OpenGraph::addImage(APP_FAVICON);
        OpenGraph::addImage(APP_LOGO);
        OpenGraph::addImage($post->cover);
        OpenGraph::addImage($post->thumbnail);
    }
}