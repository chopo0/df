<?php

namespace App\Services\Marketing;

use App\Models\Marketing\Article;


class BlogService
{
    /**
     * @var Article
     */
    private $article;

    /**
     * BlogService constructor
     */
    public function __construct(Article $article)
    {
        $this->article = $article;
    }

    /**
     * @param int $currentId
     * 
     * @return array
     */
    public function getRecentlyPublished($currentId = null)
    {
        $query = $this->article->orderBy('published_on', 'desc')->limit(3);

        if ($currentId) {
            $query = $query->where('id', '!=', $currentId);
        }

        return $query->where('published', true)->get();
    }
}